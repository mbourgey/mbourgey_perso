awk -v i=$2 ' BEGIN{
    OFS=";"
    at2=0
    at5=0
    at10=0
    at20=0
    atp=0
    ct2=0
    ct5=0
    ct10=0
    ct20=0
    ctp=0
    gt2=0
    gt5=0
    gt10=0
    gt20=0
    gtp=0
    ag2=0
    ag5=0
    ag10=0
    ag20=0
    agp=0
    cg2=0
    cg5=0
    cg10=0
    cg20=0
    cgp=0
    ac2=0
    ac5=0
    ac10=0
    ac20=0
    acp=0
    if (i == 10) {
        print "a->t/t->a(gp<2%)", "a->t/t->a(all)", "c->t/g->a(gp<2%)", "c->t/g->a(all)", "g->t/c->a(gp<2%)", "g->t/c->a(all)", "a->g/t->c(gp<2%)", "a->g/t->c(all)", "c->g/g->c(gp<2%)", "c->g/g->c(all)", "t->g/g->a(gp<2%)", "t->g/g->a(all)", "gp<2%/all", "a->t/t->a(gp[2%-5%[)", "a->t/t->a(all)", "c->t/g->a(gp[2%-5%[)", "c->t/g->a(all)", "g->t/c->a(gp[2%-5%[)", "g->t/c->a(all)", "a->g/t->c(gp[2%-5%[)", "a->g/t->c(all)", "c->g/g->c(gp[2%-5%[)", "c->g/g->c(all)", "t->g/g->a(gp[2%-5%[)", "t->g/g->a(all)", "gp[2%-5%[/all", "a->t/t->a(gp[5%-10%[)", "a->t/t->a(all)", "c->t/g->a(gp[5%-10%[)", "c->t/g->a(all)", "g->t/c->a(gp[5%-10%[)", "g->t/c->a(all)", "a->g/t->c(gp[5%-10%[)", "a->g/t->c(all)", "c->g/g->c(gp[5%-10%[)", "c->g/g->c(all)", "t->g/g->a(gp[5%-10%[)", "t->g/g->a(all)", "gp[5%-10%[/all", "a->t/t->a(gp[10%-20%[)", "a->t/t->a(all)", "c->t/g->a(gp[10%-20%[)", "c->t/g->a(all)", "g->t/c->a(gp[10%-20%[)", "g->t/c->a(all)", "a->g/t->c(gp[10%-20%[)", "a->g/t->c(all)", "c->g/g->c(gp[10%-20%[)", "c->g/g->c(all)", "t->g/g->a(gp[10%-20%[)", "t->g/g->a(all)", "gp[10%-20%[/all", "a->t/t->a(gp>=20%)", "a->t/t->a(all)", "c->t/g->a(gp>=20%)", "c->t/g->a(all)", "g->t/c->a(gp>=20%)", "g->t/c->a(all)", "a->g/t->c(gp>=20%)", "a->g/t->c(all)", "c->g/g->c(gp>=20%)", "c->g/g->c(all)", "t->g/g->a(gp>=20%)", "t->g/g->a(all)", "gp>=20%/all"
   }
} {
    split($i,x,":") 
    if (x[1] != "./.") {
        thr=x[6]/(x[6]+x[5]) 
        if (($4 == "T" && $5 == "A") || ($5 == "T" && $4 == "A")) {
            if (thr <= 0.02) {
                at2=at2+1
            } else if (thr <= 0.05) {
                at5=at5+1
            } else if (thr <= 0.1) {
                at10=at10+1
            } else if (thr <= 0.2) {
                at20=at20+1
            } else {
                atp=atp+1
            }
        } else if (($4 == "G" && $5 == "A") || ($5 == "T" && $4 == "C")) {
            if (thr <= 0.02) {
                ct2=ct2+1
            } else if (thr <= 0.05) {
                ct5=ct5+1
            } else if (thr <= 0.1) {
                ct10=ct10+1
            } else if (thr <= 0.2) {
                ct20=ct20+1
            } else {
                ctp=ctp+1
            }
        } else if (($4 == "C" && $5 == "A") || ($5 == "T" && $4 == "G")) {
            if (thr <= 0.02) {
                gt2=gt2+1
            } else if (thr <= 0.05) {
                gt5=gt5+1
            } else if (thr <= 0.1) {
                gt10=gt10+1
            } else if (thr <= 0.2) {
                gt20=gt20+1
            } else {
                gtp=gtp+1
            }
        } else if (($4 == "T" && $5 == "C") || ($5 == "G" && $4 == "A")) {
            if (thr <= 0.02) {
                ag2=ag2+1
            } else if (thr <= 0.05) {
                ag5=ag5+1
            } else if (thr <= 0.1) {
                ag10=ag10+1
            } else if (thr <= 0.2) {
                ag20=ag20+1
            } else {
                agp=agp+1
            }
        } else if (($4 == "G" && $5 == "C") || ($5 == "G" && $4 == "C")) {
            if (thr <= 0.02) {
                cg2=cg2+1
            } else if (thr <= 0.05) {
                cg5=cg5+1
            } else if (thr <= 0.1) {
                cg10=cg10+1
            } else if (thr <= 0.2) {
                cg20=cg20+1
            } else {
                cgp=cgp+1
            }
        } else if (($4 == "A" && $5 == "C") || ($5 == "G" && $4 == "T")) {
            if (thr <= 0.02) {
                tg2=tg2+1
            } else if (thr <= 0.05) {
                tg5=tg5+1
            } else if (thr <= 0.1) {
                tg10=tg10+1
            } else if (thr <= 0.2) {
                tg20=tg20+1
            } else {
                tgp=tgp+1
            }
        }
    }
} END {
    s2=at2+ct2+gt2+ag2+cg2+tg2
    s5=at5+ct5+gt5+ag5+cg5+tg5
    s10=at10+ct10+gt10+ag10+cg10+tg10
    s20=at20+ct20+gt20+ag20+cg20+tg20
    sp=atp+ctp+gtp+agp+cgp+tgp
    sall=s2+s5+s10+s20+sp
    print at2/s2, at2/sall, ct2/s2, ct2/sall, gt2/s2, gt2/sall, ag2/s2, ag2/sall, cg2/s2, cg2/sall, tg2/s2, tg2/sall, s2/sall, at5/s5, at5/sall, ct5/s5, ct5/sall, gt5/s5, gt5/sall, ag5/s5, ag5/sall, cg5/s5, cg5/sall, tg5/s5, tg5/sall, s5/sall,  at10/s10, at10/sall, ct10/s10, ct10/sall, gt10/s10, gt10/sall, ag10/s10, ag10/sall, cg10/s10, cg10/sall, tg10/s10, tg10/sall, s10/sall, at20/s20, at20/sall, ct20/s20, ct20/sall, gt20/s20, gt20/sall, ag20/s20, ag20/sall, cg20/s20, cg20/sall, tg20/s20, tg20/sall, s20/sall, atp/sp, atp/sall, ctp/sp, ctp/sall, gtp/sp, gtp/sall, agp/sp, agp/sall, cgp/sp, cgp/sall, tgp/sp, tgp/sall, sp/sall
} ' $1