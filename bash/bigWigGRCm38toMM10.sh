##created sorted bigbed comptible from original bigbed
mkdir mm10_compatible
for i in  */*sorted ; do na=$(echo $i | cut -d/ -f2) ; grep -v "GL" $i | grep -v "JH" | awk ' BEGIN {FS="\t";OFS="\t"} {if ($1 == "MT") {$1="M"} ; print "chr" $0} ' >  mm10_compatible/${na} ; done

##create chrom.size file
cd mm10_compatible

grep "@SQ" /cvmfs/soft.mugqic/CentOS6/genomes/species/Mus_musculus.mm10/genome/Mus_musculus.mm10.dict | awk ' {split($2,na,":"); split($3,le,":") ; print na[2] "\t" le[2] } ' > chrom.size.tsv

##create bigWig files
module load  mugqic/bedtools/2.25.0 mugqic/ucsc/v326
for i in *.sorted ; do out=$(echo $i | sed -e "s|bedGraph.sorted|bw|g" ) ; bedGraphToBigWig $i  chrom.size.tsv  bigWig/$out ; done
