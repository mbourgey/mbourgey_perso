#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

module load mugqic/python/2.7.8

###pipeline test run

#get last step number of each pipeline
dnaLast=$(python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/dnaseq/dnaseq.py -h | tail -1 |cut -d- -f1)
rnaLast=$(python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/rnaseq/rnaseq.py -h | tail -1 |cut -d- -f1)
chipLast=$(python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/chipseq/chipseq.py -h | tail -1 |cut -d- -f1)
rnaDNLast=$(python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/rnaseq_denovo_assembly/rnaseq_denovo_assembly.py -h | tail -1 |cut -d- -f1)

#initialize test folders
cd /lb/project/mugqic/projects/pipelineTest/

rm -rf weeklyTests

mkdir -p weeklyTests/dnaseq weeklyTests/rnaseq weeklyTests/chipseq weeklyTests/rnaseqDN

#create bash scripts
python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/dnaseq/dnaseq.py -c /home/mbourgey/work/repo/mugqic_pipelines/pipelines/dnaseq/dnaseq.base.ini  pipeline.testDNA.ini -s 2-${dnaLast} -o weeklyTests/dnaseq -r readsets.DNA.tsv -f > weeklyTestDna.sh

python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/rnaseq/rnaseq.py -c /home/mbourgey/work/repo/mugqic_pipelines/pipelines/rnaseq/rnaseq.base.ini pipeline.testRNA.ini -s 2-${rnaLast} -o weeklyTests/rnaseq -r readsets.RNA.tsv -f -d designRNA.tsv > weeklyTestRna.sh

python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/chipseq/chipseq.py -c /home/mbourgey/work/repo/mugqic_pipelines/pipelines/chipseq/chipseq.base.ini pipeline.testCHIP.ini -s 2-${chipLast} -o weeklyTests/chipseq -r readsets.ChIP.tsv -f -d designChip.tsv > weeklyTestChip.sh

python /home/mbourgey/work/repo/mugqic_pipelines/pipelines/rnaseq_denovo_assembly/rnaseq_denovo_assembly.py -c /home/mbourgey/work/repo/mugqic_pipelines/pipelines/rnaseq_denovo_assembly/rnaseq_denovo_assembly.base.ini pipeline.testRNADN.ini -s 2-${rnaDNLast} -o weeklyTests/rnaseqDN -r readsets.RNADN.tsv -f -d designRNA.tsv > weeklyTestRnaDN.sh

##launch tests
bash weeklyTestDna.sh
bash weeklyTestRna.sh
bash weeklyTestChip.sh
bash weeklyTestRnaDN.sh
