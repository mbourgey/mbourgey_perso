#!/bin/bash

DIR_TO_CHECK='/lb/scratch/jobspool/mbourgey/'
 
OLD_STAT_FILE='/lb/project/mugqic/tmpMB/abacusMail/old_stat.txt'
 
if [ -e $OLD_STAT_FILE ]
then
        OLD_STAT=`cat $OLD_STAT_FILE`
else
        OLD_STAT="nothing"
fi
 
NEW_STAT=`stat -t $DIR_TO_CHECK`
 
if [ "$OLD_STAT" != "$NEW_STAT" ]
then
        echo 'Directory has changed!'
        echo 'check your message in /lb/scratch/jobspool/mbourgey/ '
        # check the message in /lb/scratch/jobspool/mbourgey/.
        # update the OLD_STAT_FILE
        mail -s "Change in ${DIR_TO_CHECK}" "mathieu.bourgey@mcgill.ca" 
        echo $NEW_STAT > $OLD_STAT_FILE
fi
