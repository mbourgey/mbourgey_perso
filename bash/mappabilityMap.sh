#!/bin/sh
# 
##2011-11-01 - Mathieu Bourgey - mbourgey@genomequebec.com
##This pipeline
#1/ create reference p-e data set fastq given a specific design
#2/ gzip fastq files
#3/ align fastq.gz files with BWA
#4/ filtering, sorting and indexing bam
#5/ count the mean mapQ genome-wide : output bed
#6/ count the coverage of region with a mapQ >= X (x is given in paramter) : output bed

## Usage: mappingMap.sh  
# Parse arguments and print usage
ENDS_LENGTH="100"
MEAN_INSERT_SIZE="300"
SD_INSERT_SIZE="60"
WINDOWS_SHIFT="5"
REF_FASTA=""
OUTPUT_BASENAME=""
START_STEP="0"
LAST_STEP="0"
pidnum=$$

function usage {

            
            echo "Usage: `basename $0` [option] <value>"
            echo ""
            echo "mappingMap.sh - launches the pipeline to create a map of mappability"
            echo "Uses MOAB msub [required]"
            echo ""
            echo "Options:"
            echo "-i                    ini file"
            echo "-l                    ends length (default 100bp)"
            echo "-m                    mean insert size (default 300bp) "
            echo "-d                    standard deviation insert size (default 60bp) "
            echo "-o                    output basename"
            echo "-w                    windows shift (default 5bp)"
            echo "-r                    reference fasta file"
            echo "-q                    mapping quality lower threshold (default 20)"
            echo "-C                    max coverage threshold (default 40)"
            echo "-c                    min coverage threshold (default 10)"
            echo "-v                    version"
            echo "-s                    start step"
            echo "-e                    end step"
            echo "Steps:"
            echo "			1: Create reference Paired-end data set"
            echo "			2: align with BWA"
            echo "			3: sorting and indexing bam "
            echo "			4: get the coverage and mapping quality of each base from each adeqaute read (mpileup)"
            echo "			5: generate the mean mapping quality map, coverage maps and eclusion regions file (pileup2outMap.py)"
exit 0

}

##default arguments
MAX_COV="40"
MIN_COV="10"
MIN_MAPQ="20"
INI_FILE="$(dirname $0)/../ini/mappabilityMap.ini"
pidnum=$$
SCHEDULE_OPT=""
ALIGN_TYPE="mem"

##get arguments
while getopts "h:v:l:i:m:s:d:e:l:o:w:q:c:C:r:" OPT
do
    case "$OPT" in
        h)
            usage
            exit 0
            ;;
        v)
            echo "`basename $0` version 1.0"
            exit 0
            ;;
        l) 
           ENDS_LENGTH=$OPTARG
           ;;
	i) 
           INI_FILE=$OPTARG
           ;;
        m)
           MEAN_INSERT_SIZE=$OPTARG
           ;;
        d)
           SD_INSERT_SIZE=$OPTARG
           ;;
        o)
           OUTPUT_BASENAME=$OPTARG
           ;;
        w)
           WINDOWS_SHIFT=$OPTARG
           ;;
        r)
           REF_FASTA=$OPTARG
           ;;
        q)
           MIN_MAPQ=$OPTARG
           ;;
        C)
           MAX_COV=$OPTARG
           ;;
        c)
           MIN_COV=$OPTARG
           ;;
        s) 
           START_STEP=$OPTARG
           ;;
        e)
           LAST_STEP=$OPTARG
           ;;
        
    esac

done
workDir=$(dirname $OUTPUT_BASENAME)
OUTPUT_DIR=$workDir
cd $workDir 
JOB_OUT=$workDir/jobs_output/
CMD_LOG=$workDir/command_logs
if [ -f $INI_FILE ]; then
        echo "ini file found : $INI_FILE"
else 
	echo "ini file is missing : $INI_FILE"
	usage
        exit 1
fi
source $INI_FILE

        if [[ -z $ENDS_LENGTH ]] || [[ -z $MEAN_INSERT_SIZE ]] || [[ -z $SD_INSERT_SIZE ]] || [[ -z $REF_FASTA ]] || [[ -z $OUTPUT_BASENAME ]] 
then
     echo -e "Given argument end: $ENDS_LENGTH \nmean: $MEAN_INSERT_SIZE \nSD: $SD_INSERT_SIZE  \nref: $REF_FASTA  \nout: $OUTPUT_BASENAME \n"
     usage
     exit 1
fi


# Start function 0

function function_0 {
     usage
     exit 1
}

#Look for files

if [ ! -f $REF_FASTA ];then 
 echo "reference fasta file $REF_FASTA not found."; exit 1;
fi

if [ ! -d $OUTPUT_DIR/ ]; then
        mkdir $OUTPUT_DIR
	cd $workDir
fi

if [ ! -d $JOB_OUT ]; then
        mkdir $JOB_OUT
fi

if [ ! -d $CMD_LOG ]; then
        mkdir $CMD_LOG
fi


NUMTHREADS="12"
J_TEST=$(dirname $0)/$JOB_TEST_SCRIPT

# Start function 1

function function_1 {

echo "step 1 - Create reference Paired-end data set"


logfile="$CMD_LOG/getRefsimu.log"

if [ -f $logfile ]; then
        rm $logfile
fi

string="module load $MODULE_PYTHON && python $(dirname $0)/../python/getRefsimu.py -l $ENDS_LENGTH -i $MEAN_INSERT_SIZE -d $SD_INSERT_SIZE -r $REF_FASTA -m $WINDOWS_SHIFT -o $OUTPUT_BASENAME -e illumina"

echo $string > $logfile
echo $string
echo $string | msub $QUEUE -d $workDir -N getRefsimu_$pidnum -j oe -o $JOB_OUT/getRefPE_$pidnum -V -m ae -M mb.guillimin@gmail.com  -l walltime=35:00:00 $SCHEDULE_OPT

sleep 5
}

# Start function 2

function function_2 {

echo "step 2 - alignment"


logfile="$CMD_LOG/gzip.log"

if [ -f $logfile ]; then
        rm $logfile
fi

TESTD=$($J_TEST getRefsimu_$pidnum)

echo $TESTD

if [ $TESTD == 0 ]; then
	echo "step 2 wait for dependency"
	L_LIST1="walltime=35:00:00,nodes=1:ppn=12 -W x=depend:afterok:getRefsimu_$pidnum"
	L_LIST2="walltime=35:00:00,nodes=1:ppn=12 -W x=depend:afterok:getRefsimu_$pidnum"
else
	L_LIST1="walltime=35:00:00,nodes=1:ppn=12"
	L_LIST2="walltime=35:00:00,nodes=1:ppn=12"
fi

echo $L_LIST1
#ends aligning
if [ "$ALIGN_TYPE" == "aln" ]; then 
  string="module load $MODULE_BWA && bwa aln -t $NUMTHREADS -f $OUTPUT_BASENAME.pair1.sai $REF_FASTA $OUTPUT_BASENAME.pair1.fastq.gz"
  echo $string 
  echo $string >> $logfile
  echo $string | msub $QUEUE -d $workDir -N bwaP1_$pidnum  -j oe -o $JOB_OUT/bwaP1_$pidnum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST1  $SCHEDULE_OPT
  string="module load $MODULE_BWA && bwa aln -t $NUMTHREADS -f $OUTPUT_BASENAME.pair2.sai $REF_FASTA $OUTPUT_BASENAME.pair2.fastq.gz"
  echo $string 
  echo $string >> $logfile
  echo $string | msub $QUEUE -d $workDir -N bwaP2_$pidnum  -j oe -o $JOB_OUT/bwaP2_$pidnum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST2  $SCHEDULE_OPT


  #pairs aligning
  string="module load $MODULE_BWA $MODULE_SAM && bwa sampe $REF_FASTA $OUTPUT_BASENAME.pair1.sai $OUTPUT_BASENAME.pair2.sai $OUTPUT_BASENAME.pair1.fastq.gz $OUTPUT_BASENAME.pair1.fastq.gz | samtools view -bS -o $OUTPUT_BASENAME.bam -"
  echo $string 
  echo $string >> $logfile
  echo $string | msub $QUEUE -d $workDir -N bwa_$pidnum -j oe -o $JOB_OUT/bwaSampe_$pidnum -V -m ae -M mb.guillimin@gmail.com -l walltime=35:00:00,nodes=1:ppn=12 -W x=depend:afterok:bwaP1_$pidnum:bwaP2_$pidnum $SCHEDULE_OPT

elif [ "$ALIGN_TYPE" == "mem" ]; then

  MEM_THREADS=$(echo $NUMTHREADS | awk ' {print $1-1} ')
  string="module load $MODULE_BWA $MODULE_SAM && bwa mem -M -t $MEM_THREADS $REF_FASTA $OUTPUT_BASENAME.pair1.fastq.gz $OUTPUT_BASENAME.pair2.fastq.gz | samtools view -bS -o $OUTPUT_BASENAME.bam -"
  echo $string 
  echo $string >> $logfile
  echo $string | msub $QUEUE -d $workDir -N bwa_$pidnum  -j oe -o $JOB_OUT/bwaMem_$pidnum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST1  $SCHEDULE_OPT

fi


sleep 5


}

# Start function 3

function function_3 {

echo "step 3 - sorting and indexing bam"

logfile="$CMD_LOG/filterBam.log"

if [ -f $logfile ]; then
        rm $logfile
fi

TESTD=$($J_TEST bwa_$pidnum )

if [ $TESTD == 0 ]; then
	echo "step 3 wait for dependency"
	L_LIST="walltime=35:00:00,nodes=1:ppn=6 -W x=depend:afterok:bwa_$pidnum"
else
	L_LIST="walltime=35:00:00,nodes=1:ppn=6"
fi


#sort and indec bam
string="module load $MODULE_JAVA $MODULE_PICARD && java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=10 -Dsamjdk.use_async_io=true -Xmx27G -jar \${PICARD_HOME}/SortSam.jar VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true TMP_DIR=/gs/scratch/$USER INPUT=${OUTPUT_BASENAME}.bam OUTPUT=${OUTPUT_BASENAME}.sorted.bam SORT_ORDER=coordinate MAX_RECORDS_IN_RAM=3250000"
echo $string 
echo $string >> $logfile
echo $string | msub $QUEUE -d $workDir -N sortIndexBAM_$pidnum -j oe -o $JOB_OUT/samS_$pidnum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST  $SCHEDULE_OPT


sleep 5


}

# Start function 4
function function_4 {


echo "step 4 - get the coverage and mapping quality of each base from each adeqaute read (mpileup) "

logfile="$CMD_LOG/mpileup.log"

if [ -f $logfile ]; then
        rm $logfile
fi

TESTD=$($J_TEST sortIndexBAM_$pidnum)

if [ $TESTD == 0 ]; then
	echo "step 4 wait for dependency"
	L_LIST="walltime=35:00:00 -W x=depend:afterok:sortIndexBAM_$pidnum"
else
	L_LIST="walltime=35:00:00"
fi

grep ">" $REF_FASTA | grep -v "GL" | grep -v "random" | grep -v "Un" | cut -d\> -f2 | cut -d\  -f1  > ${workDir}/tmpchr.txt

for chr in  `cat  ${workDir}/tmpchr.txt `
do

string="module load $MODULE_SAM && samtools mpileup -A -B -C 0 -f $REF_FASTA -q 0 -Q 0 -s $OUTPUT_BASENAME.sorted.bam -r $chr > $OUTPUT_BASENAME.$chr.mpileup"
echo $string 
echo $string >> $logfile
echo $string | msub $QUEUE -d $workDir -N mpileup_$pidnum -j oe -o $JOB_OUT/mpileup_$pidnum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST  $SCHEDULE_OPT

done


string="rm -rf $OUTPUT_BASENAME.mpileup && for chr in  \`cat  ${workDir}/tmpchr.txt \` ; do cat $OUTPUT_BASENAME.\${chr}.mpileup >> $OUTPUT_BASENAME.mpileup ; done "
echo $string 
echo $string >> $logfile
echo $string | msub $QUEUE -d $workDir -N MergeMpileUp_$pidnum -j oe -o $JOB_OUT/merge.mpileup_$pidnum -V -m ae -M mb.guillimin@gmail.com -W x=depend:afterok:mpileup_$pidnum  $SCHEDULE_OPT

sleep 5

}


#Start function 5
function function_5 {

echo "step 5 - generate the mean mapping quality map, coverage maps and exclusion regions file (pileup2outMap.py) "

logfile="$CMD_LOG/output.log"

if [ -f $logfile ]; then
        rm $logfile
fi

TESTD=$($J_TEST MergeMpileUp_$pidnum)

if [ $TESTD == 0 ]; then
	echo "step 5 wait for dependency"
	L_LIST="walltime=35:00:00 -W x=depend:afterok:MergeMpileUp_$pidnum"
else
	L_LIST="walltime=35:00:00"
fi

string="module load $MODULE_EXONARATE $MODULE_PYTHON && fastalength $REF_FASTA | grep -v "GL" | grep -v "random" | grep -v "Un" > ${workDir}/tmpchrlength.txt && python $(dirname $0)/../python/pileup2outMap.py -f $OUTPUT_BASENAME.mpileup -m $MIN_MAPQ -d $MIN_COV -c $MAX_COV -o $OUTPUT_BASENAME -l ${workDir}/tmpchrlength.txt"
echo $string 
echo $string >> $logfile
echo $string | msub $QUEUE -d $workDir -N output_$pidnum -j oe -o $JOB_OUT/output_$pidnum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST  $SCHEDULE_OPT


wait
}

# Execute functions from $START_STEP to $LAST_STEP

for step in `seq $START_STEP $LAST_STEP`
do
function_$step
done
