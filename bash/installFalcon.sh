#works for abacus

m mugqic/python/2.7.8 mugqic_dev/gcc/4.8.3-gpfs

mkdir FALCON

cd FALCON

#virtualenv setup
FC=fc_env
virtualenv --no-site-packages   $FC

#virtualenv activate
. $FC/bin/activate
## should retrun fc_env

git clone git://github.com/PacificBiosciences/FALCON-integrate.git
cd FALCON-integrate
git clone git@github.com:PacificBiosciences/DALIGNER.git
git clone git@github.com:PacificBiosciences/DAZZ_DB.git
git clone git@github.com:PacificBiosciences/FALCON.git
git clone git@github.com:pb-cdunn/FALCON-examples.git
git clone git@github.com:pb-cdunn/FALCON-make.git
git clone git@github.com:cdunn2001/git-sym.git
cd git-sym/
git clone git@github.com:cdunn2001/git-sym-test.git
cd ..
git clone git@github.com:PacificBiosciences/pypeFLOW.git
git clone git@github.com:pb-cdunn/virtualenv.git

#build and install
cd pypeFLOW
python setup.py install
cd ..

cd FALCON
python setup.py install
cd ..

cd DAZZ_DB/
make
cp DBrm DBshow DBsplit DBstats fasta2DB $FC/bin/
cd ..

cd DALIGNER
make
cp daligner daligner_p DB2Falcon HPCdaligner LA4Falcon LAmerge LAsort  $FC/bin
cd ..
