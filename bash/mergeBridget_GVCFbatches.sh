#!/bin/bash
# Exit immediately on error
set -eu -o pipefail


##to modify for each cohort

variantL="--variant /project/6007512/C3G/projects/Bridget_bcklog_analysis_1663/dijon/b1/variants/allSamples.hc.g.vcf.bgz --variant /project/6007512/C3G/projects/Bridget_bcklog_analysis_1663/dijon/b2/variants/allSamples.hc.g.vcf.bgz --variant /project/6007512/C3G/projects/Bridget_bcklog_analysis_1663/dijon/b3/variants/allSamples.hc.g.vcf.bgz --variant /project/6007512/C3G/projects/Bridget_bcklog_analysis_1663/dijon/b4/variants/allSamples.hc.g.vcf.bgz --variant /project/6007512/C3G/projects/Bridget_bcklog_analysis_1663/dijon/b5/variants/allSamples.hc.g.vcf.bgz --variant /project/6004777/projects/Bridget_bcklog_analysis_1663/dijon/batch2/variants/allSamples.hc.g.vcf.bgz"

Cohort_ID="Dijon"

####

mkdir -p job_output

####merge Batch

exclusionL=""
MergeL=""
Dependency="--depend=afterok"

for i in `seq 1 22` X 
do
COMMAND="module load java/1.8.0_121 mugqic/GenomeAnalysisTK/3.7 && \
mkdir -p variants && \
java -Djava.io.tmpdir=\${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=1048576 -Xmx60G -jar \$GATK_JAR \
  --analysis_type CombineGVCFs  \
  --disable_auto_index_creation_and_locking_when_reading_rods \
  --reference_sequence /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  ${variantL} \
  --out variants/allSamples.${i}.hc.g.vcf.bgz \
  --intervals chr${i}"

exclusionL="${exclusionL} --excludeIntervals chr${i}"
MergeL="${MergeL} --variant variants/allSamples.${i}.hc.g.vcf.bgz"
JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
$COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $(pwd) -o job_output/ -J ${Cohort_ID}_mergeGVCF_batch_${i} --time=72:00:00 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
Dependency="${Dependency}:${JOB_ID}"
done

COMMAND="module load java/1.8.0_121 mugqic/GenomeAnalysisTK/3.7 && \
mkdir -p variants && \
java -Djava.io.tmpdir=\${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=1048576 -Xmx60G -jar \$GATK_JAR \
  --analysis_type CombineGVCFs  \
  --disable_auto_index_creation_and_locking_when_reading_rods \
  --reference_sequence /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  ${variantL} \
  --out variants/allSamples.other.hc.g.vcf.bgz \
  ${exclusionL}"

MergeL="${MergeL} --variant variants/allSamples.other.hc.g.vcf.bgz"
JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
$COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $(pwd) -o job_output/ -J ${Cohort_ID}_mergeGVCF_batch_other --time=72:00:00 --mem=64G -N 1 -n 16 | grep "[0-9]" | cut -d\  -f4)
Dependency="${Dependency}:${JOB_ID}"


####Merge intervals
COMMAND="module load java/1.8.0_121 mugqic/GenomeAnalysisTK/3.7 && \
mkdir -p variants && \
java -Djava.io.tmpdir=\${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=1048576 -Xmx60G -jar \$GATK_JAR \
  org.broadinstitute.gatk.tools.CatVariants  \
  --reference /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  ${MergeL} \
  --outputFile variants/allSamples.hc.g.vcf.bgz"
CAT_JOB_ID=$(echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
$COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $(pwd) -o job_output/ -J ${Cohort_ID}_catGVCF --time=120:00:00 --mem=64G -N 1 -n 16 ${Dependency} | grep "[0-9]" | cut -d\  -f4)

### Genotype GCVF
COMMAND="module load java/1.8.0_121 mugqic/GenomeAnalysisTK/3.7 && \
mkdir -p variants && \
java -Djava.io.tmpdir=\${SLURM_TMPDIR} -XX:ParallelGCThreads=1 -Dsamjdk.buffer_size=1048576 -Xmx60G -jar \$GATK_JAR \
  --analysis_type GenotypeGVCFs -nt 12 -A FisherStrand -A QualByDepth -A ChromosomeCounts \
  --disable_auto_index_creation_and_locking_when_reading_rods \
  --reference_sequence /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  --variant variants/allSamples.hc.g.vcf.bgz \
  --out variants/allSamples.hc.vcf.bgz"
echo "#! /bin/bash
echo '#######################################'
echo 'SLURM FAKE PROLOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
$COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
echo '#######################################'
echo 'SLURM FAKE EPILOGUE (MUGQIC)'
date 
scontrol show job \$SLURM_JOBID
sstat -j \$SLURM_JOBID.batch 
echo '#######################################'
exit \$MUGQIC_STATE" | \
sbatch --mail-type=END,FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D $(pwd) -o job_output/ -J GenotypeGVCFs --time=120:00:00 --mem=64G -N 1 -n 16 --depend=afterok:${CAT_JOB_ID}
