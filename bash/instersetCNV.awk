awk ' 
NR == 1 {
	chr=$1
	st=$2
	en=$3
	old=chr"_"st
	if ($4 == ".") {
		va=0
		cp=1
	} else {
		va=$7
		cp=1
	}
}
NR > 1 {
	if (old != $1"_"$2) {
		print chr "\t" st "\t" en "\t" va/cp
		chr=$1
		st=$2
		en=$3
		old=chr"_"st
		if ($4 == ".") {
			va=0
			cp=1
		} else {
			va=$7
			cp=1
		}
	} else {
		va=va+$7
		cp=cp+1
	}
}
END {
	if (cp > 0) {
		print chr "\t" st "\t" en "\t" va/cp
	}
} ' $1 