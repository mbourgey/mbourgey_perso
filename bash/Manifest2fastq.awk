#!/bin/bash
##ARG: 1 - header line number
##ARG: 2 - manifest input file
## Author: Mathieu Bourgey - mathieu.bourgey@mail.mcgill.ca

# Exit immediately on error

set -eu -o pipefail

##
awk -v head=$1 ' BEGIN {
    FS=","
    
} NR > head {
    if ($4 != "[N/A]" && $4 != "[D/I]"  && $4 != "[I/D]") { #for 660-Qwad chip
        split($17,seqF,"[")
        split(seqF[2],seqS,"]")
        split(seqS[1],baseSNP,"/")
        lSeq=length(seqS[2])+length(seqF[1])+1
        baseQ=""
        for(i=0; i<lSeq; i++ ) {
            baseQ = baseQ "I"
        }
        print "@" $2 "_" $10 "_" $11 "_" seqS[1] "_" baseSNP[1]
        print toupper(seqF[1]) toupper(baseSNP[1]) toupper(seqS[2])
        print "+" 
        print baseQ
    }
} ' $2 