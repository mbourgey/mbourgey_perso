#!/bin/bash


# IHEC ChIP-seq post-process script. To keep only data need for ihec
# Implemented by mathieu.bourgey@mcgill.ca - 16/10/2017 
# Usage IHEC_data_clean.sh <MARK_RESULT_FOLDER> <OUTPUT_FOLDER>
 

fol=$(pwd)
cd $1

#clean metrics
cd ihec_metrics/
ls | grep -v read_stats.txt | xargs -n 10 rm

#clean tracks
cd ../tracks/
for i in */bigWig/*bw ; do mv  $i . ; done
for i in `ls | grep -v bw` ; do rm -rf $i ; done

#Clean peak call
cd ../peak_call/
for i in  */*bb ; do mv $i . ; done
for i in `ls | grep -v bb` ; do rm -rf  $i ; done

#clean everything else
cd ..
rm -rf alignment  graphs ihec_alignment job_output metrics report tags trim

#move data
cd $fol
mkdir -p $2
mv $1 $2