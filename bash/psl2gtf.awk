#!/bin/bash

## psl2gtf.awk <INPUT>  <OUTPUT> <MINSIZE (percent of the query)>

FILE=$1
OUTPUT=$2
MINSIZE=$3

if [[ $FILE == "-h" || $FILE == "--help" || $# -ne 3 ]]
then
echo "psl2gtf.awk  convert blat psl to gtf file"
echo "Usage:"
echo "      psl2gtf.awk <INPUT>  <OUTPUT> <MINSIZE (percent of the query)>"
exit 0
fi 



awk  -v msize=$MINSIZE ' BEGIN {
    old="na"
    FS="\t"
    OFS="\t"
    ve=0
} NR > 5 {
    if (($1/$11) < msize) {
        if ($14 != old) {
            ve=0 
            old=$14
        }
        ve=ve+1
        ct=0
        if ($18 == 1) {
            ct=ct+1
            print $14 , "assembled_transcript" , "exon" , $16+1 , $17+1  , "." , $9 , "." , "gene_id \""$10"."ve"\"; transcript_id \""$10"."ve"\"; exon_number \""ct"\""
        } else {
            split($19,siEx,",")
            split($21,stEx,",")
            for (i = 1; i <= $18; i++) {
                ct=ct+1
                print $14 , "assembled_transcript" , "exon" , stEx[i]+1 , stEx[i]+1+siE[i] , "." , $9 , "." , "gene_id \""$10"."ve"\"; transcript_id \""$10"."ve"\"; exon_number \""ct"\""
            }
        }
    }
} ' $FILE | sort -k1,1 -k4,4 > $OUTPUT