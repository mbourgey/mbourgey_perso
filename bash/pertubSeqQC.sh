#!/bin/sh
# 
##2018-05-30 - Mathieu Bourgey - mathieu.bourgey@mcgill.ca

## Usage: mappingBFP.sh <cellranger_outs_path> <analysis_folder> <experience_Name>
# Parse arguments and print usage

OUT_PATH=$(readlink -e  $1)
ANALYSIS_PATH=$(readlink -e  $2)
EXP_NAME=$3

##
mkdir -p ${ANALYSIS_PATH}/${EXP_NAME}

#extract BFP reads
samtools view -h ${OUT_PATH}/possorted_genome_bam.bam BFP.backbone > ${ANALYSIS_PATH}/${EXP_NAME}/${EXP_NAME}.BFP.backbone.sam


#extract allele
cd ${ANALYSIS_PATH}/${EXP_NAME}/
grep GGATCCGGTGATGGGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\twt"}}}' > BFP_Genotypes.tsv
grep GGATCCTTGAGTGGGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\tsg607"}}}' >> BFP_Genotypes.tsv
grep GGATCCCGGTGATGGGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\tsg609"}}}' >> BFP_Genotypes.tsv
grep GGATCCGGGGGGGGGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\tsg623"}}}' >> BFP_Genotypes.tsv
grep GGATCCTGGGGGAAGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\tsg629"}}}' >> BFP_Genotypes.tsv
grep GGATCCGTGGGGTGGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\tsg631"}}}' >> BFP_Genotypes.tsv
grep GGATCCTTTCCTAGGACTTA ${EXP_NAME}.BFP.backbone.sam | awk ' {for(i=1;i<=NF;i++) {split($i,t,":") ; if (t[1] == "CB") {print t[3] "\tsg657"}}}' >> BFP_Genotypes.tsv


#sort and count alle per cell-barcode
sort -k1,1 BFP_Genotypes.tsv | uniq -c > BFP_Genotypes_count_sorted.tsv

#genotype
awk ' BEGIN {OFS="\t";wt=0;sg607=0;sg609=0;sg623=0;sg629=0;sg631=0;sg657=0;old=0;print "Barcode","wt","sg607","sg609","sg623","sg629","sg631","sg657"} { if (old != $2) { if (old != 0) {print old,wt,sg607,sg609,sg623,sg629,sg631,sg657} ; old=$2 ; wt=0;sg607=0;sg609=0;sg623=0;sg629=0;sg631=0;sg657=0} ; if ($3 == "wt") {wt+=$1} else if ($3 == "sg607" ){sg607+=$1} else if ($3 == "sg609" ){sg609+=$1} else if ($3 == "sg623" ){sg623+=$1} else if ($3 == "sg629" ){sg629+=$1} else if ($3 == "sg631" ){sg631+=$1} else if ($3 == "sg657" ){sg657+=$1} } END {print old,wt,sg607,sg609,sg623,sg629,sg631,sg657} ' BFP_Genotypes_count_sorted.tsv > BFP_Genotypes_unique.tsv

# cd ../..
# cp -r filtered_gene_bc_matrices orig_filtered_gene_bc_matrices
# 
# cd filtered_gene_bc_matrices/hg19_b37_BFP/
# # 
# # ##adddvector count to 10Xgen gene matrice
# #  ~/work/repo/mbourgey_perso/python/addvector2geneMatrix.py
# #  
# # mv genes_new.tsv genes.tsv
# # mv matrix_new.mtx matrix.mtx
#  
##seurat analysis 

Rscript ~/work/repo/mbourgey_perso/Rscript/PertubSeqQC.R ${OUT_PATH} ${EXP_NAME}

