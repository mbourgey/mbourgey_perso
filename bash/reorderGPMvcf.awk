awk ' BEGIN {
  FS= "\t"
  OFS= "\t"
}
NR == 1 {
  $8="1073225G:P:M;1624645G:P:M;1641227G:P:M;1654072G:P:M;259609G:P:M"
  print $0
}
NR > 1 {
    for (i=1;i<=5;i++) {
    y=38+(21*(i-1))
    x=split($y,info," , ")
    if (x > 1) {
      split(info[1],gFtmp,":")
      if (gFtmp[1] == "1/1") {
        gF=2
      } else if (gFtmp[1] == "0/1" || gFtmp[1] == "1/0") {
        gF=1
      } else {
        gF=0
      }
      split(info[2],mFtmp,":")
      if (mFtmp[1] == "1/1") {
        mF=2
      } else if (mFtmp[1] == "0/1" || mFtmp[1] == "1/0") {
        mF=1
      } else {
        mF=0
      }
    } else {
      gF=0
      mF=0
    }
    y=45+(21*(i-1))
    x=split($y,info," , ")
    if (x > 1) {
      split(info[2],pFtmp,":")
      if (pFtmp[1] == "1/1") {
        pF=2
      } else if (pFtmp[1] == "0/1" || pFtmp[1] == "1/0") {
        pF=1
      } else {
        pF=0
      }
    } else {
      pF=0
    }
    if (i == 1) {
      newF=gF":"pF":"mF
    } else {
      newF=newF";"gF":"pF":"mF
    }
  }
  $8=newF
  print $0
} ' $1 
  
