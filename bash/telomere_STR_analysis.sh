#!/bin/bash
# Exit immediately on error
set -eu -o pipefail


###USAGE required 10cpu - 45G
#telomer_expansion_analysis.sh <BAM_FILE> <REF_FASTA> <READ1_FASTQ> <READ2_FASTQ>

##set env
BAM_FILE=$1
INDEX=$(readlink -f ${BAM_FILE} | sed -e "s|.bam|.bai|g")
INDEX_LINK=${BAM_FILE}.bai
REF=$2
NAME=$(echo $BAM_FILE | cut -d/ -f3 | cut -d\. -f1)
F1=$(readlink -f $3)
F2=$(readlink -f $4)


###STR length analysis
##expensionHunter
# need bam.bai index file
ln -s ${INDEX} ${INDEX_LINK}
module load  mugqic_dev/ExpansionHunter/2.5.3
ExpansionHunter --bam ${BAM_FILE} \
                --ref-fasta ${REF} \
                --repeat-specs ${EXPANSION_DATA}/repeat-specs/grch37/ \
                --vcf STRexpansion/${NAME}.EH.vcf \
                --json STRexpansion/${NAME}.EH.json \
                --log STRexpansion/${NAME}.EH.log

module remove mugqic_dev/ExpansionHunter/2.5.3


### Telomere length analysis
## TelSeq
mkdir -p TelomereLength/TelSeq/${NAME}
module load mugqic/bamtools/2.4.1 mugqic_dev/Telseq/dev_Jan2018 mugqic/gcc/4.9.3
telseq -o TelomereLength/TelSeq/${NAME} \
        -r 150 \
        ${BAM_FILE}


module remove mugqic/bamtools/2.4.1 mugqic_dev/Telseq/dev_Jan2018 mugqic/gcc/4.9.3

## Computel
mkdir -p TelomereLength/Computel/
rm -rf TelomereLength/Computel/${NAME}
module load mugqic_dev/R_Bioconductor/3.2.3_3.2 mugqic_dev/Computel/1.2
computel.sh -proc 10 \
            -1 ${F1} \
            -2 ${F2} \
            -o TelomereLength/Computel/${NAME}

module remove mugqic_dev/R_Bioconductor/3.2.3_3.2 mugqic_dev/Computel/1.2

##qmotif
mkdir -p TelomereLength/qmotif/${NAME}
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic_dev/qmotif/1.2
java -Xmx45G  -jar ${QMOTIF_HOME}/qmotif-1.2.jar \
            -n 10 \
            --bam ${BAM_FILE} \
            --bai ${INDEX} \
            --log TelomereLength/qmotif/${NAME}/${NAME}.bam.qmotif.log \
            --loglevel DEBUG \
            -ini ${QMOTIF_HOME}/ini/telomere.GRCh37.ini \
            -o TelomereLength/qmotif/${NAME}/${NAME}.bam.qmotif.xml \
            -o TelomereLength/qmotif/${NAME}/${NAME}.telomere.bam

module remove mugqic/java/openjdk-jdk1.8.0_72 mugqic_dev/qmotif/1.2

##telomerecat
mkdir -p TelomereLength/telomerecat/${NAME}
module load mugqic_dev/python/2.7.13
telomerecat bam2length \
            -p 10 \
            -v 2 \
            --output TelomereLength/telomerecat/${NAME}/${NAME}.length.csv \
            ${BAM_FILE}

module remove mugqic_dev/python/2.7.13