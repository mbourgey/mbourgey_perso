awk ' 
NR == 1 {
	chr=$1
	st=$2
	en=$3
	old=chr"_"st
	if ($4 == ".") {
		va=$4
	} else {
		va=$7
	}
}
NR > 1 {
	if (old != $1"_"$2) {
		print chr "\t" st "\t" en "\t" va
		chr=$1
		st=$2
		en=$3
		old=chr"_"st
		if ($4 == ".") {
			va=$4
		} else {
			va=$7
		}
	} else {
		x=split(va,info,";")
		add=0
		for (i=1;i<=x;i++) {
			if (info[i] == $7) {
				add=1
			}
		}
		if (add == 0 ) {
			va=va";"$7
		}
	}
}
END {
	if (cp > 0) {
		print chr "\t" st "\t" en "\t" va
	}
} ' $1 