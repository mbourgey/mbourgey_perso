#!/bin/sh
# 
##2018-04-26 - Mathieu Bourgey - mathieu.bourgey@mcgill.ca
##log abacus cluster usage

da=$(date)

que=$(showq -i | grep "^[0-9]" | grep -v eligible | awk ' BEGIN {t=0}  { t+=$7 } END {print t}')
run=$(showq -r  | grep "^[0-9]" | grep -v active | awk ' BEGIN {t=0}  { t+=$10 } END {print t}')
blo=$(showq -b  | grep "^[0-9]" | grep -v blocked | awk ' BEGIN {t=0}  { t+=$5 } END {print t}')

echo -e "${da}\t${run}\t${que}\t${blo}\t2064"
