#!/bin/sh
## create a gene size in bp file (Name\tTotalSize) from a gtf file


#Usage gtf2geneID_symbol_transtable.awk gtf($1) output($2)

awk -v out=$2 ' BEGIN {
	FS="\t"
	na="pre"
	symb="NA"
}
{
	if ($3 == "CDS") {
		gsub (" ", "",$9)
		x=split($9,col,";")
		for (i=1 ; i<= x ; i++) {
			split(col[i],info,"\"")
			if ("gene_id" == info[1]) {
				geN=info[2]
			}
			if ("gene_name" == info[1]) {
				symbTmp=info[2]
			}
		}
		if (geN != na) {
			if (na != "pre") {
				print na "\t" symb > out
			}
			na=geN
			symb=symbTmp
		} else {
			if (symb != symbTmp) {
				print na " has different symbols"
			}
		}
	}
}
END {
	if (na != "pre") {
		print na "\t" symb > out
	}
}
' $1 