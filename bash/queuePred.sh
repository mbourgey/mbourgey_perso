#!/bin/sh 

##Usage queuePred CLUSTER_NAME

mkdir -p $HOME/queuePred
J12S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto12sw -l walltime=24:00:00 -l nodes=1:ppn=12 -N queuePred | grep "[0-9]")
J12S_PRED=$(showstart $J12S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob ${J12S}) 2> /dev/null
J10S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto10sw -l walltime=24:00:00 -l nodes=1:ppn=10 -N queuePred | grep "[0-9]")
J10S_PRED=$(showstart $J10S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob ${J10S}) 2> /dev/null
J8S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto8sw -l walltime=24:00:00 -l nodes=1:ppn=8 -N queuePred | grep "[0-9]")
J8S_PRED=$(showstart $J8S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J8S}) 2> /dev/null
J6S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto6sw -l walltime=24:00:00 -l nodes=1:ppn=6 -N queuePred | grep "[0-9]")
J6S_PRED=$(showstart $J6S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J6S}) 2> /dev/null
J4S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto4sw -l walltime=24:00:00 -l nodes=1:ppn=4  -N queuePred | grep "[0-9]")
J4S_PRED=$(showstart $J4S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J4S}) 2> /dev/null
J2S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto2sw -l walltime=24:00:00 -l nodes=1:ppn=2 -N queuePred | grep "[0-9]")
J2S_PRED=$(showstart $J2S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J2S}) 2> /dev/null
J1S=$(echo "sleep 3000" | msub -q sw -o $HOME/queuePred/toto1sw -l walltime=24:00:00 -l nodes=1:ppn=1  -N queuePred| grep "[0-9]")
J1S_PRED=$(showstart $J1S  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J1S}) 2> /dev/null

J12L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto12lm -l walltime=24:00:00 -l nodes=1:ppn=12 -N queuePred | grep "[0-9]")
J12L_PRED=$(showstart $J12L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob ${J12L}) 2> /dev/null
J10L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto10lm -l walltime=24:00:00 -l nodes=1:ppn=10 -N queuePred | grep "[0-9]")
J10L_PRED=$(showstart $J10L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob ${J10L}) 2> /dev/null
J8L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto8lm -l walltime=24:00:00 -l nodes=1:ppn=8 -N queuePred | grep "[0-9]")
J8L_PRED=$(showstart $J8L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J8L}) 2> /dev/null
J6L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto6lm -l walltime=24:00:00 -l nodes=1:ppn=6 -N queuePred | grep "[0-9]")
J6L_PRED=$(showstart $J6L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J6L}) 2> /dev/null
J4L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto4lm -l walltime=24:00:00 -l nodes=1:ppn=4  -N queuePred | grep "[0-9]")
J4L_PRED=$(showstart $J4L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J4L}) 2> /dev/null
J2L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto2lm -l walltime=24:00:00 -l nodes=1:ppn=2 -N queuePred | grep "[0-9]")
J2L_PRED=$(showstart $J2L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J2L}) 2> /dev/null
J1L=$(echo "sleep 3000" | msub -q lm -o $HOME/queuePred/toto1lm -l walltime=24:00:00 -l nodes=1:ppn=1  -N queuePred| grep "[0-9]")
J1L_PRED=$(showstart $J1L  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J1L}) 2> /dev/null


if [ $1 == "guillimin" ] 
then
J12H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto12hb -l walltime=24:00:00 -l nodes=1:ppn=12 -N queuePred | grep "[0-9]")
J12H_PRED=$(showstart $J12H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob ${J12H}) 2> /dev/null
J10H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto10hb -l walltime=24:00:00 -l nodes=1:ppn=10 -N queuePred | grep "[0-9]")
J10H_PRED=$(showstart $J10H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob ${J10H}) 2> /dev/null
J8H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto8hb -l walltime=24:00:00 -l nodes=1:ppn=8 -N queuePred | grep "[0-9]")
J8H_PRED=$(showstart $J8H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J8H}) 2> /dev/null
J6H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto6hb -l walltime=24:00:00 -l nodes=1:ppn=6 -N queuePred | grep "[0-9]")
J6H_PRED=$(showstart $J6H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J6H}) 2> /dev/null
J4H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto4hb -l walltime=24:00:00 -l nodes=1:ppn=4  -N queuePred | grep "[0-9]")
J4H_PRED=$(showstart $J4H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J4H}) 2> /dev/null
J2H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto2hb -l walltime=24:00:00 -l nodes=1:ppn=2 -N queuePred | grep "[0-9]")
J2H_PRED=$(showstart $J2H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J2H}) 2> /dev/null
J1H=$(echo "sleep 3000" | msub -q hb -o $HOME/queuePred/toto1hb -l walltime=24:00:00 -l nodes=1:ppn=1  -N queuePred| grep "[0-9]")
J1H_PRED=$(showstart $J1H  | grep "Estimated Rsv based start" | awk '{print $6}')
clean=$(canceljob  ${J1H}) 2> /dev/null

echo -e "-------------------------------------------------------------------------------\n"
echo -e "$1 cluster in queue prediction :"
echo -e "CPU#\tsw_prediction\t\t\tlm_prediction\t\t\thb_prediction"
echo -e "1\t${J1S_PRED}\t\t\t${J1L_PRED}\t\t\t${J1H_PRED}"
echo -e "2\t${J2S_PRED}\t\t\t${J2L_PRED}\t\t\t${J2H_PRED}"
echo -e "4\t${J4S_PRED}\t\t\t${J4L_PRED}\t\t\t${J4H_PRED}"
echo -e "6\t${J6S_PRED}\t\t\t${J6L_PRED}\t\t\t${J6H_PRED}"
echo -e "8\t${J8S_PRED}\t\t\t${J8L_PRED}\t\t\t${J8H_PRED}"
echo -e "10\t${J10S_PRED}\t\t\t${J10L_PRED}\t\t\t${J10H_PRED}"
echo -e "12\t${J12S_PRED}\t\t\t${J12L_PRED}\t\t\t${J12H_PRED}"
echo -e "\n ===> GOOD LUCK AND BE PATIENT !!\n"
echo -e "-------------------------------------------------------------------------------\n"

else
echo -e "-------------------------------------------------------------------------------\n"
echo -e "$1 Cluster in queue prediction :"
echo -e "CPU#\tsw_prediction\t\t\tlm_prediction"
echo -e "1\t${J1S_PRED}\t\t\t${J1L_PRED}"
echo -e "2\t${J2S_PRED}\t\t\t${J2L_PRED}"
echo -e "4\t${J4S_PRED}\t\t\t${J4L_PRED}"
echo -e "6\t${J6S_PRED}\t\t\t${J6L_PRED}"
echo -e "8\t${J8S_PRED}\t\t\t${J8L_PRED}"
echo -e "10\t${J10S_PRED}\t\t\t${J10L_PRED}"
echo -e "12\t${J12S_PRED}\t\t\t${J12L_PRED}"
echo -e "\n ===> GOOD LUCK AND BE PATIENT !!\n"
echo -e "-------------------------------------------------------------------------------\n"
fi



clean=$(rm -rf $HOME/queuePred)

