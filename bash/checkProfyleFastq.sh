#!/bin/bash

DIR_TO_CHECK='/lb/scratch/PROFYLE_FASTQ/14817_PROFYLE_Year2'
 
OLD_STAT_FILE='/lb/project/mugqic/profyle_fastq_check/old_stat.txt'
 
if [ -e $OLD_STAT_FILE ]
then
        OLD_STAT=`cat $OLD_STAT_FILE`
else
        OLD_STAT="nothing"
fi
 
NEW_STAT=`stat -t $DIR_TO_CHECK`
 
if [ "$OLD_STAT" != "$NEW_STAT" ]
then
        echo 'Directory has changed!'
        # do whatever you want to do with the directory.
        # update the OLD_STAT_FILE
        mail -s "Change in ${DIR_TO_CHECK}" "robert.eveleigh@mcgill.ca pascale.marquis2@mcgill.ca" 
        echo $NEW_STAT > $OLD_STAT_FILE
fi
