#!/bin/sh

#Usage annotateBedSV.sh bed_input($1) genePos($2) chromosomeLength($3) output($4) prefix($5)

#parms
BEDTOOL=module load mugqic/bedtools/2.17.0
INPUT_FILE=$1
GEN_POS=$2
CHR_LEN=$3
OUTPUT_FILE=$4
UTR_POS="$5.tmpUTRpos.txt"

#generate UTR position file
echo "Generate UTR position file ..."
#step 1 -get chromosome length
awk ' {
	if ($2 == "X") { 
		$2=23
	} 
	if ($2 == "Y") { 
		$2=24
	}
	print $2 "\t" $3
} ' $CHR_LEN | grep -v "GL" | grep -v "MT" > $5.tmpChrL.txt

#step 2 - generate raw UTRs
$BEDTOOL/flankBed -b 1000 -i $GEN_POS -g $5.tmpChrL.txt | grep -v "GL"  > $5.tmpUTR.deb

#step 3 - remove UTR region which overlapp a gene
$BEDTOOL/ubtractBed -a $5.tmpUTR.deb -b $GEN_POS > $UTR_POS

##gene annotation
echo "annotate using genes position ..."

$BEDTOOL/intersectBed -wao -a $INPUT_FILE -b $GEN_POS > $5.tmp.gene.txt
	
awk ' NR==1 {
	out=$1 "\t" $2 "\t" $3 "\t" $4 
	old=$4
	if ($5 != ".") {
		gen="Gene"
	}
	else {
		gen="."
	}
}
NR > 1 {
	if ($4 != old) {
		print out "," gen 
		out=$1 "\t" $2 "\t" $3 "\t" $4 
		old=$4
		if ($5 != ".") {
			gen=$5
		}
		else {
			gen="."
		}
	} 
} END{
	print out "," gen 
} ' $5.tmp.gene.txt > $5.Gene.txt

##utr annotation
echo "annotate using utr position ..."

$BEDTOOL/intersectBed -wao -a $5.Gene.txt -b $GEN_POS > $5.tmp.utr.txt
	
awk ' NR==1 {
	out=$1 "\t" $2 "\t" $3 "\t" $4 
	old=$4
	if ($5 != ".") {
		split($4,Info,",")
		if (Info[2] == ".") {
			utr="UTR"
		} else {
			utr="."
		}
	}
	else {
		utr="."
	}
}
NR > 1 {
	if ($4 != old) {
		print out "," utr 
		out=$1 "\t" $2 "\t" $3 "\t" $4 
		old=$4
		if ($5 != ".") {
		split($4,Info,",")
		if (Info[2] == ".") {
			utr="UTR"
		} else {
			utr="."
		}
		}
		else {
			utr="."
		}
	} 
} END{
	print out "," utr 
} ' $5.tmp.utr.txt > $OUTPUT_FILE

##cleaning
rm $5.tmpUTRpos.txt $5.tmpChrL.txt $5.tmpUTR.deb $5.tmp.gene.txt $5.Gene.txt $5.tmp.utr.txt

	