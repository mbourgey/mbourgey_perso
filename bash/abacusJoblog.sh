#!/usr/bin/env sh

echo  "step 1 - generate plot"
Rscript ~/work/repo/mbourgey_perso/Rscript/jobPrint.R
echo  "step 2 - catch file"
attach=$(ls -t /lb/project/mugqic/tmpMB/LogAbacusUsage/logPlot/* | head -1)
echo  "step 3 - send file"
echo "see graph" | mail -s "Abacus weekly usage log" -a ${attach}  -r mathieu.bourgey@mcgill.ca  BIOINF@LISTS.MCGILL.CA


echo "Done"
