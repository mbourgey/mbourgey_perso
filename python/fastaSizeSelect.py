#!/usr/bin/env python

### Mathieu Bourgey (2015/03/09)
### extract sequence while only keeping those with a length comprised between a max and a min value

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO



def getarg(argument):
	dirF="fasta"
	maxSize=False
	optli,arg = getopt.getopt(argument[1:],"i:o:s:m:f:h",['input','out','size','max','format','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-s","--size"):
			siz=int(value) 
		if option in ("-m","--max"):
			maxSize=int(value) 
		if option in ("-f","--format"):
			if (str(value) == "fastq" or str(value) == "fasta"):
				dirF=str(value)   
			else :
				print "unknow format: use fasta"
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, siz, maxSize, dirF



def usage():
	print "USAGE : fastaSizeSelect.py [option] "
	print "       -i :        input file"
	print "       -o :        output file"
	print "       -s :        lower cut-off size"
	print "       -m :        upper cut-off size (default no cut-off)"
	print "       -f :        format input file:" 
	print "                      fasta: for fasta file (default)"
	print "                      fastq: for fastq file"
	print "       -h :        this help \n"


def main():
	print "\n------------------------------------------------------------"
	print "fastaSizeSelect.py will generate fasta or fastq file while only keeping "
	print "sequences with a length comprised between a max and a min value" 
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, siz, msiz, dirF = getarg(sys.argv)
	if msiz == False :
		if dirF == "fasta" :
			records=(record for record in SeqIO.parse(inF, "fasta") if len(record.seq) >= siz)
			SeqIO.write(records, outF, "fasta")
		elif dirF == "fastq":
			records=(record for record in SeqIO.parse(inF, "fastq") if len(record.seq) >= siz)
			SeqIO.write(records, outF, "fastq")
	else:
		if dirF == "fasta" :
			records=(record for record in SeqIO.parse(inF, "fasta") if len(record.seq) >= siz and len(record.seq) <= msiz)
			SeqIO.write(records, outF, "fasta")
		elif dirF == "fastq":
			records=(record for record in SeqIO.parse(inF, "fastq") if len(record.seq) >= siz and len(record.seq) <= msiz)
			SeqIO.write(records, outF, "fastq")
	
	
main()

	    
	
	





