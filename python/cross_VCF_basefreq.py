#!/usr/bin/env python

### Mathieu Bourgey (2020/02/11) - mathieu.bourgey@mcgill.ca
### intersect vcf and basefreq file

import os
import sys
import string
import getopt
import re




def getarg(argument):
	vcf=""
	bf=""
	cov=10
	out=""
	na="Sample"
	optli,arg = getopt.getopt(argument[1:],"v:b:c:o:n:h",['vcf','basefreq','cov','out','name','help'])
	if len(optli) < 3 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-v","--vcf"):
			vcf=str(value)
		if option in ("-b","--basefreq"):
			bf=str(value)
		if option in ("-c","--cov"):
			cov=int(value)
		if option in ("-o","--out"):
			out=str(value)
		if option in ("-n","--name"):
			na=str(value)
                if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(vcf) :
		sys.exit("Error - VCF file not found:\n"+vcf)
        if not os.path.exists(bf) :
		sys.exit("Error - Basefreq file not found:\n"+bf)
        if out == "" :
		sys.exit("Error - No output file path given\n")
	return  vcf, bf, cov, out, na



def usage():
	print "USAGE : cross_VCF_basefreq.py [option] "
	print "       -v :        VCF file path"
	print "       -b :        Basefreq file path"
	print "       -c :        Coverage minimal threshold value (Default 10)"
        print "       -o :        Output tsv file path"
        print "       -n :        Sample name (Default Sample)"
	print "       -h :        this help \n"

def getVCFcall(fi):
        f=open(fi,'r')
        l=f.readline()
        snp={}
        while l != "":
            if l[0] != "#": ### skip header
                c=l.split()
                g=c[9].split(":")
                snp[c[0]+"_"+c[1]]=g[0]
            l=f.readline()
        f.close()
        return snp
	
	
def countCov(c): ##pass the list result of a basefreq line splited using ','
        ts=0
	b=c[2].split(" ")
	for i in range(0,5,1):
            v=b[i].split(":")
            ts+=int(v[1])
	return ts



def main():
	print "\n---------------------------------------------------------------------------------"
	print "cross_VCF_basefreq.py intersect a vcf and a basefreq file to report 0/0 or ./. "
	print "for position not in the vcf file"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mathieu.bourgey@mcgill.ca"
	print "----------------------------------------------------------------------------------\n"
	vcfP, bfP, cov, outP, na = getarg(sys.argv)
	snp=getVCFcall(vcfP) ## get vcf calls
	out=open(outP,'w')
	out.write("Position\t"+na+"\n")
	bf=open(bfP,'r')
	l=bf.readline() ## skip header
	l=bf.readline()
	while l != "" :
            c=l.split(",")
            pos=c[0]+"_"+c[1]
            outS=pos+"\t./.\n"
            pcov=countCov(c)
            if pcov >= cov :
                if snp.has_key(pos) :
                    outS=pos+"\t"+snp[pos]+"\n"
                else :
                    outS=pos+"\t0/0\n"
            out.write(outS)
            l=bf.readline()
        out.close()
        bf.close()

main()

	    
	
	





