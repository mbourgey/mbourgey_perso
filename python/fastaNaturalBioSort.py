#!/usr/bin/env python

### Mathieu Bourgey (2014/03/21)
### sort by natural order fasta sequences (Chro_numbers,X,Y,MT,NonChromosome sequences) or based on a given list

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO



def getarg(argument):
    inF=None
    outF=None
    nameList=None
    optli,arg = getopt.getopt(argument[1:],"i:o:l:h",['input','out','list','help'])
    if len(optli) < 1 :
            usage()
            sys.exit("Error : Missing argument(s)")
    for option, value in optli:
            if option in ("-i","--input"):
                    inF=str(value)
            if option in ("-o","--output"):
                    outF=str(value)
            if option in ("-l","--list"):
                    nameList=str(value)   
            if option in ("-h","--help"):
                    usage()
                    sys.exit()
    if not os.path.exists(inF) :
            sys.exit("Error - input file not found:\n"+inF)
    return  inF, outF, nameList



def usage():
    print "USAGE : fastaNaturalBioSort.py [option] "
    print "       -i :        input file"
    print "       -o :        output file"
    print "       -l :        pre-ordered list of sequence (optional)"
    print "       -h :        this help \n"


def main():
    print "\n------------------------------------------------------------"
    print "fastaNaturalBioSort.py take a fasta as input and output the sequences "
    print "using a natural Biological order (numbered chromosomes, X, Y, MT non-chromomses."
    print "A specific order could be use if the order is given in file" 
    print "This program was written by Mathieu BOURGEY"
    print "For more information, contact: mbourgey@genomequebec.com"
    print "------------------------------------------------------------\n"
    inF, outF, nameList = getarg(sys.argv)
    print "import input fasta..."
    record_dict = SeqIO.index(inF,'fasta')
    dl=[]
    for i in record_dict.keys():
        dl.append(i)
    print "... done"
    print "\n------------------------------------------------------------"
    print "ordering sequences..."
    dictName={}
    orderList=[]
    if nameList :
        print "Using pre-ordered list file" + nameList
        fi=open(nameList,'r')
        li=fi.redline()
        dictName={}
        orderList=[]
        while li != "" :
            co=li.split()
            dictName=[co[0]]=co[0]
            orderList.append(co[0])
            li=fi.redline()
        fi.close()
    else :
        print "Using natural biological order"
        numK=[]
        alloK=[]
        mitoK=[]
        nonK=[]
        for i in range(0,len(dl),1):
            ##test if name start by a variation of chr on remove it for ordering not for final name
            if dl[i][:3].upper() == "CHR":
                d=dl[i].upper().split("CHR")[1]
            else :
                d=dl[i].upper()
            if d.isdigit() :
                dictName[int(d)]=dl[i]
                numK.append(int(d))
                print "autosome found: " + dl[i]
            elif d == "X" or  d == "Y":
                alloK.append(d)
                dictName[d]=dl[i]
                print "allosome found: " + dl[i]
            elif d[0] == "M" :
                ##Mitochondria chromosome
                mitoK.append(d)
                print "Mitochondria sequence found: " + dl[i]
                dictName[d]=dl[i]
            else:
                print "Non-chromosome sequence found: " + dl[i]
                dictName[d]=dl[i]
                nonK.append(d)

        numK.sort()
        orderList=numK
        alloK.sort()
        orderList.extend(alloK)
        orderList.extend(mitoK)
        nonK.sort()
        orderList.extend(nonK)
    print "... done"
    print "\n------------------------------------------------------------"
    print "generate outputs in: "+outF+"..."
    out=open(outF,"w")
    for i in range(0,len(orderList),1):
        if record_dict.has_key(dictName[orderList[i]]) :
            SeqIO.write(record_dict[dictName[orderList[i]]],out,"fasta")
        else:
            print "Sequence not found in the initial fasta file: "+dictName[orderList[i]]
    print "... done"
    out.close()


main()






