#!/usr/bin/python

### Mathieu Bourgey (2015/09/11)

def main():
	b="HumanOmni2-5-8-v1-2-A.clean.new.tsv"
	a="/lb/project/mugqic/projects/IGN_Array/v1.1/humanomni25m-8v1-1_b.cleaned.sorted.csv"
	f1=open(a,'r')
	f2=open(b,'r')
	outInt=open("intersectOK.csv",'w')
	outIntN=open("intersectNotOK.csv",'w')
	out1=open("v1.1spec.csv",'w')
	out2=open("v1.2spec.csv",'w')
	endComp=0
	l1=f1.readline()
	l2=f2.readline()
	while endComp == 0 :
		c1=l1.split()[0].split(",")
		c2=l2.split()[0].split(",")
		if c1[2] == c2[2] :
			if l1 == l2 :
				outInt.write(l1)
			else :
				outIntN.write(",".join(c1)+"_"+",".join(c2)+"\n")
			l1=f1.readline()
			l2=f2.readline()
		else :
			if c1[0] == c2[0] :
				if c1[1] < c2[1] :
					out1.write(l1)
					l1=f1.readline()
				elif c1[1] > c2[1] :
					out2.write(l2)
					l2=f2.readline()
				else:
					print "same position but diffrente IDs: "+",".join(c1)+"_"+",".join(c2)
					if c1[3] == c2[3] and c1[4] == c2[4] and c1[5] == c2[5]:
						outInt.write(l1)
					l1=f1.readline()
					l2=f2.readline()
			elif c1[0] < c2[0]:
				out1.write(l1)
				l1=f1.readline()
			else :
				out2.write(l2)
				l2=f2.readline()
		if l1 == "" :
			endComp=endComp+1
		if l2 == "" :
			endComp=endComp+2
	while endComp == 1 :
		c2=l2.split()[0].split(",")
		out2.write(l2)
		l2=f2.readline()
		if l2 == "" :
			endComp=endComp+2
	while endComp == 2 :
		c1=l1.split()[0].split(",")
		out1.write(l1)
		l1=f1.readline()
		if l1 == "" :
			endComp=endComp+1
	f1.close()
	f2.close()
	outInt.close()
	outIntN.close()
	out1.close()
	out2.close()

main()