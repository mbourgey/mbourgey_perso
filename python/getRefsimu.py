#!/usr/bin/python

### Mathieu Bourgey (2014/08/06)
### simulate sequencing reads 

import os
import sys
import string
import getopt
import re
import random
import numpy
import gzip
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

class reads:
	def __init__(self,size1,size2):
		self.r1s=0
		self.r1e=0
		self.r1seq=""
		self.r1Qual="I"*size1
		self.rpID=""
		self.r2s=0
		self.r2e=0
		self.r2seq=""
		self.r2Qual="I"*size2


	
	def getIDpaired(self,ch):
		self.rpID="@"+str(ch)+"_"+str(self.r1s)+"_"+str(self.r1e)+"_"+str(self.r2s)+"_"+str(self.r2e)
	
	def getIDsingle(self,ch):
		self.rpID="@"+str(ch)+"_"+str(self.r1s)+"_"+str(self.r1e)

	def writerpaired(self,out1,out2) :
		tmp2Seq=Seq(self.r2seq, IUPAC.unambiguous_dna)
		self.r2seq= str(tmp2Seq.reverse_complement())
		prob=random.random()
		if prob < 0.5 :
			##reverse complement
			tmp1Seq=Seq(self.r1seq, IUPAC.unambiguous_dna)
			tmp2Seq=Seq(self.r2seq, IUPAC.unambiguous_dna)
			self.r2seq= str(tmp1Seq.reverse_complement())
			self.r1seq= str(tmp2Seq.reverse_complement())
		out1.write('%s\n%s\n+\n%s\n' % (self.rpID,self.r1seq,self.r1Qual))
		out2.write('%s\n%s\n+\n%s\n' % (self.rpID,self.r2seq,self.r2Qual))
	
	def writersingle(self,out1) :
		prob=random.random()
		if prob < 0.5 :
			##reverse complement
			tmp1Seq=Seq(self.r1seq, IUPAC.unambiguous_dna)
			self.r1seq= str(tmp1Seq.reverse_complement())
		out1.write('%s\n%s\n+\n%s\n' % (self.rpID,self.r1seq,self.r1Qual))

		
	def getReadPosFpaired(self,x,rl,rl2,ins,sd) :
		self.r1s=x
		self.r1e=int(self.r1s)+int(rl)
		self.r2s=int(self.r1e)+int(round(random.gauss(ins,sd)))
		self.r2e=int(self.r2s)+int(rl2)
		
	def getReadPosBpaired(self,x,le,rl,rl2,ins,sd) :
		self.r2e=le-x
		self.r2s=int(self.r2e)-int(rl2)
		self.r1e=int(self.r2s)-int(round(random.gauss(ins,sd)))
		self.r1s=int(self.r1e)-int(rl)
	
	def getReadPosFsingle(self,x,rl) :
		self.r1s=x
		self.r1e=int(self.r1s)+int(rl)
		
	def getReadPosBsingle(self,x,le,rl) :
		self.r1e=int(le)-int(x)
		self.r1s=int(self.r1e)-int(rl)


def getarg(argument):
	refF=""
	readL2=0
	move=1
	tarG=""
	libT="paired"
	error="null"
	fileD="null"
	optli,arg = getopt.getopt(argument[1:],"l:s:m:i:d:r:o:t:w:e:f:h",['len','sec','move','ins','dev','ref','out','target','library','error','file','help'])
	if len(optli) < 5 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-l","--len"):
			readL=int(value)
		if option in ("-s","--sec"):
			readL2=int(value)
		if option in ("-i","--ins"):
			insS=int(value)
		if option in ("-d","--dev"):
			insD=int(value)	
		if option in ("-r","--ref"):
			refF=str(value)	
		if option in ("-m","--move"):
			move=int(value)	
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-t","--target"):
                        tarG=str(value)
                if option in ("-w","--library"):
                        libT=str(value)
                if option in ("-e","--error"):
                        error=string.lower(str(value))
		if option in ("-f","--file"):
			fileD=str(value)       
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(refF) :
		sys.exit("Error - reference file not found:\n"+refF)
	if readL2 == 0:
		readL2=readL
	if libT != "paired" and libT != "single" :
                sys.exit("Error - UNKNOWN LIBRARY TYPE:\n"+libT)
	return  readL, readL2, move, insS, insD, refF, out, tarG, libT, error, fileD



def usage():
	print "USAGE : getRefsimu.py [option] "
	print "       -l :        read ends length (bp)"
	print "       -s :        optional read ends 2 length (bp)"
	print "       -m :        windows move (bp)"
	print "       -i :        insert size (bp)"
	print "       -d :        insert size standard deviation (bp)"
	print "       -r :        reference genome file "
	print "       -o :        output basename"
	print "       -t :        target region of reference genome file chr:start-end"
        print "       -w :        library type (single or paired) default paired"
	print "       -e :        error type (null or illumina or pacbio) default null"
	print "       -f :        fragment size distribution file; default null"
	print "                   if f argument is given then the m, i and d arguments will be ignored !"
	print "       -h :        this help \n"

def substitutionModel(seq ,pos):
	base=seq[pos]
	posSuiv=pos+1
	d={}
	d["A"]=["C","G","T"]
	d["C"]=["A","G","T"]
	d["G"]=["A","C","T"]
	d["T"]=["A","C","G"]
	d["N"]=["N","N","N"]
	d["R"]=["N","N","N"]
	d["Y"]=["N","N","N"]
	d["S"]=["N","N","N"]
	d["W"]=["N","N","N"]
	d["K"]=["N","N","N"]
	d["M"]=["N","N","N"]
	d["B"]=["N","N","N"]
	d["D"]=["N","N","N"]
	d["H"]=["N","N","N"]
	d["V"]=["N","N","N"]
	d["N"]=["N","N","N"]
	d["."]=["N","N","N"]
	d["-"]=["N","N","N"]
	prob=random.random()
	if prob < 0.33 :
		return seq[:pos]+d[string.upper(base)][0]+seq[posSuiv:]
	elif prob < 0.66 :
		return seq[:pos]+d[string.upper(base)][1]+seq[posSuiv:]
	else:
		return seq[:pos]+d[string.upper(base)][2]+seq[posSuiv:]
	
def indelModel(seqS,seqC,preP):
	dif=len(seqS)-len(seqC)
	indelpos=preP-dif
	prob=random.random()
	if prob < 0.5 :
		#deletion
		tmp=indelpos+1
		ts=seqC[:indelpos]+seqC[tmp:]
	else :
		ts=seqC[:indelpos]+seqC[indelpos]+seqC[indelpos:]
	return ts

def generateError(seq,errorM,read):
	if errorM == "null" :
		retSeq=seq
	elif errorM == "illumina" :
		###increasing with seq length 0.5 to 2 perct subsitution model
		retSeq=seq
		if read == 1 :
			errP=numpy.linspace(0.005,0.02,len(seq))
		else :
			errP=numpy.linspace(0.02,0.005,len(seq))
		for i in range(0,len(seq),1) :
			prob=random.random()
			if prob < errP[i]:
				retSeq=substitutionModel(seq,i)
	elif errorM == "pacbio" :
		###constant 2 perct subsitution model
		###13% indel on homopolymers ends
		ctH=0
		prevB="Z"
		prevP=-1
		dist=0
		#get sequeunce direction
		probDir=random.random()
		if probDir >= 0.5:
			tmpseq=seq
		else :
			tmpseq=''.join(reversed(seq))
		retSeq=tmpseq
		for i in range(0,len(tmpseq),1) :
			substi=True
			if prevB != tmpseq[i]:
				if  ctH == 0:
					prevB=tmpseq[i]
					prevP=i
				else :
					prob=random.random()
					if prob < 0.13 :
						retSeq=indelModel(tmpseq,retSeq,prevP)
						substi=False
					prevB=tmpseq[i]
					ctH=0
					prevP=i
			else :
				prevB=tmpseq[i]
				ctH=ctH+1
				prevP=i
			if substi:
				probS=random.random()
				if probS < 0.02:
					retSeq=substitutionModel(seq,i)
	elif errorM == "pacbiocor" :
		###constant 2 perct subsitution model
		###13% indel on homopolymers ends
		ctH=0
		prevB="Z"
		prevP=-1
		dist=0
		#get sequeunce direction
		probDir=random.random()
		if probDir >= 0.5:
			tmpseq=seq
		else :
			tmpseq=''.join(reversed(seq))
		retSeq=tmpseq
		for i in range(0,len(tmpseq),1) :
			substi=True
			if prevB != tmpseq[i]:
				if  ctH == 0:
					prevB=tmpseq[i]
					prevP=i
				else :
					prob=random.random()
					if prob < 0.01 :
						retSeq=indelModel(tmpseq,retSeq,prevP)
						substi=False
					prevB=tmpseq[i]
					ctH=0
					prevP=i
			else :
				prevB=tmpseq[i]
				ctH=ctH+1
				prevP=i
			if substi:
				probS=random.random()
				if probS < 0.02:
					retSeq=substitutionModel(seq,i)
	else :
		sys.exit("Error - UNKNOWN error model:\n"+errorM)
	return retSeq
		
#readLenList=getLength(fileD)
def getLength(x):
	fi=open(x,'r')
	lenList=[]
	li=fi.readline()
	while li != "":
		co=li.split()
		lenList.append(co[0])
		li=fi.readline()
	random.shuffle(lenList)
	return lenList

def main():
	print "\n---------------------------------------------------------------------------------"
	print "getRefsimu.py will generate fastq files of theoretical paired-end/single library covering "
	print "all the reference genome or a targeted region"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	readL, readL2, move, insSi, insD, refF, outB, tarG, libT, errM, fileD = getarg(sys.argv)
	if libT == "paired":
                insS=insSi-(readL+readL2)
		out1= gzip.open(outB+".pair1.fastq.gz",'wb')
		out2= gzip.open(outB+".pair2.fastq.gz",'wb')
		peRforw=reads(readL,readL2)
		peRback=reads(readL,readL2)
		if tarG != "" :
			info=tarG.split(":")
			chrT=info[0]
			coord=info[1].split("-")
			startT=int(coord[0])
			endT=int(coord[1])
			for seq_record in SeqIO.parse(refF, "fasta"):
				if chrT == seq_record.id :
					chroS=endT
					chroN=seq_record.id
					tarS=startT
					slide=0
					walkOnChr=True
					while walkOnChr :
						peRforw.getReadPosFpaired(tarS,readL,readL2,insS,insD)
						peRforw.getIDpaired(chroN)
						peRback.getReadPosBpaired(slide,chroS,readL,readL2,insS,insD)
						peRback.getIDpaired(chroN)
						if peRforw.r1s < peRback.r1s :
							peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
							peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
							peRforw.r1Qual="I"*len(peRforw.r1seq)
							peRforw.r2seq=str(seq_record.seq)[peRforw.r2s:peRforw.r2e]
							peRforw.r2seq=generateError(peRforw.r2seq,errM,2)
							peRforw.r2Qual="I"*len(peRforw.r2seq)
							peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
							peRback.r1seq=generateError(peRback.r1seq,errM,1)
							peRback.r1Qual="I"*len(peRback.r1seq)
							peRback.r2seq=str(seq_record.seq)[peRback.r2s:peRback.r2e]
							peRback.r2seq=generateError(peRback.r2seq,errM,2)
							peRback.r2Qual="I"*len(peRback.r2seq)
							peRforw.writerpaired(out1,out2)
							peRback.writerpaired(out1,out2)
							slide=slide+move
							tarS=tarS+move
						else :
							peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
							peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
							peRforw.r1Qual="I"*len(peRforw.r1seq)
							peRforw.r2seq=str(seq_record.seq)[peRforw.r2s:peRforw.r2e]
							peRforw.r2seq=generateError(peRforw.r2seq,errM,2)
							peRforw.r2Qual="I"*len(peRforw.r2seq)
							peRforw.writerpaired(out1,out2)
							walkOnChr=False
		else :
			for seq_record in SeqIO.parse(refF, "fasta"):
				chroS=len(seq_record)
				chroN=seq_record.id
				slide=0
				walkOnChr=True
				while walkOnChr :
					peRforw.getReadPosFpaired(slide,readL,readL2,insS,insD)
					peRforw.getIDpaired(chroN)
					peRback.getReadPosBpaired(slide,chroS,readL,readL2,insS,insD)
					peRback.getIDpaired(chroN)
					if peRforw.r1s < peRback.r1s :
						peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
						peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
						peRforw.r1Qual="I"*len(peRforw.r1seq)
						peRforw.r2seq=str(seq_record.seq)[peRforw.r2s:peRforw.r2e]
						peRforw.r2seq=generateError(peRforw.r2seq,errM,2)
						peRforw.r2Qual="I"*len(peRforw.r2seq)
						peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
						peRback.r1seq=generateError(peRback.r1seq,errM,1)
						peRback.r1Qual="I"*len(peRback.r1seq)
						peRback.r2seq=str(seq_record.seq)[peRback.r2s:peRback.r2e]
						peRback.r2seq=generateError(peRback.r2seq,errM,2)
						peRback.r2Qual="I"*len(peRback.r2seq)
						peRforw.writerpaired(out1,out2)
						peRback.writerpaired(out1,out2)
						slide=slide+move
					else :
						peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
						peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
						peRforw.r1Qual="I"*len(peRforw.r1seq)
						peRforw.r2seq=str(seq_record.seq)[peRforw.r2s:peRforw.r2e]
						peRforw.r2seq=generateError(peRforw.r2seq,errM,2)
						peRforw.r2Qual="I"*len(peRforw.r2seq)
						peRforw.writerpaired(out1,out2)
						walkOnChr=False
		out1.close()
		out2.close()
	else :
		out1= gzip.open(outB+".pair1.fastq.gz",'wb')
		peRforw=reads(readL,readL2)
		peRback=reads(readL,readL2)
		if tarG != "" :
			info=tarG.split(":")
			chrT=info[0]
			coord=info[1].split("-")
			startT=int(coord[0])
			endT=int(coord[1])
			for seq_record in SeqIO.parse(refF, "fasta"):
				if chrT == seq_record.id :
					chroS=endT
					chroN=seq_record.id
					tarS=startT
					slide=0
					if fileD != "null" :
						readLenList=getLength(fileD)
						move=float((endT-startT)/len(readLenList))
						indexF=range(0,len(readLenList)/2,1)
						indexB=range(len(readLenList)-1,(len(readLenList)/2)-1,-1)
						for i in range(0,len(indexB),1):
							if i < len(indexF) :
								peRforw.getReadPosFsingle(int(tarS),readLenList[indexF[i]])
								peRforw.getIDsingle(chroN)
								peRback.getReadPosBsingle(int(slide),chroS,readLenList[indexB[i]])
								peRback.getIDsingle(chroN)
								peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
								peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
								peRforw.r1Qual="I"*len(peRforw.r1seq)
								peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
								peRback.r1seq=generateError(peRback.r1seq,errM,1)
								peRback.r1Qual="I"*len(peRback.r1seq)
								peRforw.writersingle(out1)
								peRback.writersingle(out1)
							else :
								peRback.getReadPosBsingle(int(slide),chroS,readLenList[indexB[i]])
								peRback.getIDsingle(chroN)
								peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
								peRback.r1seq=generateError(peRback.r1seq,errM,1)
								peRback.r1Qual="I"*len(peRback.r1seq)
								peRback.writersingle(out1)
							slide=slide+move
							tarS=tarS+move
					else :
						walkOnChr=True
						while walkOnChr :
							peRforw.getReadPosFsingle(tarS,readL)
							peRforw.getIDsingle(chroN)
							peRback.getReadPosBsingle(slide,chroS,readL)
							peRback.getIDsingle(chroN)
							if peRforw.r1s < peRback.r1s :
								peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
								peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
								peRforw.r1Qual="I"*len(peRforw.r1seq)
								peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
								peRback.r1seq=generateError(peRback.r1seq,errM,1)
								peRback.r1Qual="I"*len(peRback.r1seq)
								peRforw.writersingle(out1)
								peRback.writersingle(out1)
								slide=slide+move
								tarS=tarS+move
							else :
								peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
								peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
								peRforw.r1Qual="I"*len(peRforw.r1seq)
								peRforw.writersingle(out1)
								walkOnChr=False
		else :
			for seq_record in SeqIO.parse(refF, "fasta"):
				chroS=len(seq_record)
				chroN=seq_record.id
				slide=0
				walkOnChr=True
				while walkOnChr :
					peRforw.getReadPosFsingle(slide,readL)
					peRforw.getIDsingle(chroN)
					peRback.getReadPosBsingle(slide,chroS,readL)
					peRback.getIDsingle(chroN)
					if peRforw.r1s < peRback.r1s :
						peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
						peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
						peRforw.r1Qual="I"*len(peRforw.r1seq)
						peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
						peRback.r1seq=generateError(peRback.r1seq,errM,1)
						peRback.r1Qual="I"*len(peRback.r1seq)
						peRforw.writersingle(out1)
						peRback.writersingle(out1)
						slide=slide+move
					else :
						peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
						peRforw.r1seq=generateError(peRforw.r1seq,errM,1)
						peRforw.r1Qual="I"*len(peRforw.r1seq)
						peRforw.writersingle(out1)
						walkOnChr=False
		out1.close()

main()

	    
	
	





