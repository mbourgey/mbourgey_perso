#!/usr/bin/python

### Mathieu Bourgey (2014/10/14)
### extract mutation signature

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO
import vcf

class chromSnpList :
	def __init__(self) :
		self.pos=[]
		self.nam=[]



def getarg(argument):
	dirF="fasta"
	fla=1
	optli,arg = getopt.getopt(argument[1:],"i:o:s:f:h",['input','out','snp','flanking','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-s","--snp"):
			regF=str(value)   
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, regF, fla



def usage():
	print "USAGE : getRefAllele.py [option] "
	print "       -i :        input bed file"
	print "       -o :        output file"
	print "       -s :        SNP vcf 4 file"
	print "       -h :        this help \n"

def main():
	print "\n------------------------------------------------------------"
	print "getRefAllel.py will generate table of the mutational signature "
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, vcfF, fla= getarg(sys.argv)
	snp={}
	f=open(inF,'r')
	l=f.readline()
	while l != "":
		c=l.split()
		snp[str(c[0])+"_"+str(c[1])]=c[3]
		l=f.readline()
	f.close()
	vcf_reader = vcf.Reader(open(vcfF,'r'))
	o=open(outF,'w')
	for record in vcf_reader:
		if record.is_snp and snp.has_key(str(record.CHROM)+"_"+str(record.POS)) :
			o.write(str(record.CHROM)+"\t"+str(record.POS)+"\t"+snp[str(record.CHROM)+"_"+str(record.POS)]+"\t"+str(record.REF)+"\t"+str(record.ALT[0])+"\n")
	o.close()	


main()

exit()





