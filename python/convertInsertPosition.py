#!/usr/bin/python

### Mathieu Bourgey (2011/03/21)
### convert scaffold insert position using a blat psl file as reference mapping
### output the poistion in bed format

import os
import sys
import string
import getopt
import re
import itertools



#define the insert point object
class insertPoint:
	def __init__(self,co):
		self.pos=[int(co[1])]
		self.com=[co[2]]
		
#define the blat resultobject
class blatRes:
	def __init__(self,co):
		self.Qstart=[int(co[11])+1]
		self.Qend=[int(co[12])]
		self.Tname=[co[13]]
		self.Tstart=[int(co[15])+1]
		self.Tend=[int(co[16])+1]
	
	def translate(self,pos):
		posT=0
		mat=""
		for i in range(0,len(self.Qstart),1):
			if pos >= self.Qstart[i] and pos <= self.Qend[i]:
				dist=pos-self.Qstart[i]
				posT=self.Tstart[i]+dist
				mat=self.Tname[i]
				break
		return mat,posT

##get argument
def getarg(argument):
	optli,arg = getopt.getopt(argument[1:],"i:o:b:h",['input','out','blat','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			out=str(value)   
		if option in ("-b","--blat"):
			blaF=str(value)   
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - insertion file not found:\n"+inF)
	if not os.path.exists(blaF) :
		sys.exit("Error - insertion file not found:\n"+blaF)
	return  inF, out, blaF



#usage
def usage():
	print "USAGE : convertInsertPosition.py [option] "
	print "       -i :        input insertion point file"
	print "       -o :        output bed file"
	print "       -b :        blat results file:" 
	print "       -h :        this help \n"

#get insertion point on scaffold
def getInsertion(x):
	fi=open(x,'r')
	dico={} #hash table
	li=fi.readline()
	while li != "":
		co=li.split()
		if dico.has_key(co[0]) :
			dico[co[0]].pos.append(int(co[1]))
			dico[co[0]].com.append(co[2])
		else:
			dico[co[0]]=insertPoint(co)
		li=fi.readline()
	fi.close()
	return dico

#get blat results and provide a hash table that allows translation
def getblatRes(x):
	fi=open(x,'r')
	for i in range(0,5,1):
		head=fi.readline()
	li=fi.readline()
	dico={} #hash table
	while li != "":
		co=li.split()
		if dico.has_key(co[9]) :
			dico[co[9]].Qstart.append(int(co[11])+1)
			dico[co[9]].Qend.append(int(co[12])+1)
			dico[co[9]].Tname.append(co[13])
			dico[co[9]].Tstart.append(int(co[15])+1)
			dico[co[9]].Tstart.append(int(co[16])+1)
		else:
			dico[co[9]]=blatRes(co)
		li=fi.readline()
	fi.close()
	return dico

def main():
	print "\n------------------------------------------------------------"
	print "convert scaffold insert position using a blat psl file as "
	print "reference mapping and output the positions in bed format"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, blaF = getarg(sys.argv)
	out=open(outF,'w')
	insP=getInsertion(inF)
	transTable=getblatRes(blaF)
	k=insP.keys()
	for i in k :
		if transTable.has_key(i):
			for j in range(0,len(insP[i].pos),1):
				matchN,posi=transTable[i].translate(insP[i].pos[j])
				if posi != 0 :
					out.write(matchN+"\t"+str(posi)+"\t"+str(posi+1)+"\t"+i+"_"+insP[i].com[j]+"\n")
				else :
					print "insertion " +i+":"+str(insP[i].pos[j])+" not found"
	out.close()



main()

	    
	
	





