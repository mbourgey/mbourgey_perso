#!/usr/bin/python

### Mathieu Bourgey (2015/05/21)
### extract reference allele from cleaned probs of chip manifest

import os
import sys
import string
import getopt
import re
import itertools
import threading
from Bio import SeqIO
from Bio.Alphabet import IUPAC

#init threadlocking systeme
threadLock = threading.Lock()
threads = []



class myThread (threading.Thread):
    def __init__(self, threadID, name, i, kh, ks):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.i = i
        self.kh = kh
        self.ks = ks
    def run(self):
        print "Starting " + self.name
        # Get lock to synchronize threads
        threadLock.acquire()
        build_kmer(self.i,self.kh,self.ks)
        # Free lock to release next thread
        threadLock.release()




def getarg(argument):
	thr=1
	min_prop=10
	ambigous=5
	homopol_max=10
	max_length=10000
	optli,arg = getopt.getopt(argument[1:],"i:o:k:m:M:t:a:b:h",['input','out','kmer','min','max','thread','ambiguity','homo','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-k","--kmer"):
			kmer_start=str(value)
		if option in ("-m","--min"):
			min_prop=int(value)
                if option in ("-M","--max"):
			max_length=int(value)
		if option in ("-a","--ambiguity"):
			mambigous=int(value)
		if option in ("-b","--homo"):
			homopol_max=int(value)
		if option in ("-t","--thread"):
			thr=int(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, kmer_start, min_prop, max_length, thr, ambigous, homopol_max



def usage():
	print "USAGE : kmer_extend.py [option] "
	print "       -i :        input fastq file"
	print "       -o :        output fasta file"
	print "       -k :        kmer serquence to start from (will be use to determine the kmer size"
	print "       -m :        minimal amount of the next base to continue the sequence extension (default 10)"
	print "       -M :        maximum length of the consensus sequence in bp (default 10000)"
	print "       -a :        maximum ambiguity in the consensus (default 5)"
	print "       -b :        maximum homopolymere size (default 10)" 
	print "       -t :        number of thread (default 1)"
	print "       -h :        this help \n"
def kmer_ini ():
	nextbase={}
	nextbase["A"]=0
	nextbase["C"]=0
	nextbase["G"]=0
	nextbase["T"]=0
	nextbase["N"]=0
	return nextbase


def build_kmer(seq_file,kmer_hash,kmer_size):
	f=open(seq_file,'r')
	l=f.readline()
	c=l.split
	while l != "":
		c=l.split()
		for i in range(0,len(c[0])-(kmer_size+1),1):
			try:
				kmer_hash[c[0][i:(i+kmer_size)]][c[0][i+kmer_size]]=kmer_hash[c[0][i:(i+kmer_size)]][c[0][i+kmer_size]]+1
			except:
				kmer_hash[c[0][i:(i+kmer_size)]]=kmer_ini()
				kmer_hash[c[0][i:(i+kmer_size)]][c[0][i+kmer_size]]=kmer_hash[c[0][i:(i+kmer_size)]][c[0][i+kmer_size]]+1
		l=f.readline()


  
def main():
	print "\n------------------------------------------------------------"
	print "kmer_extend.py will generate a the most common sequence in the "
	print "fastq starting from the given kmer"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mathieu.bourgey@mail.mcgill.ca"
	print "http://computationalgenomics.ca/"
	print "------------------------------------------------------------\n"
	inF, outF, kmer_start, min_prop, max_length, thr, ambigous, homopol_max = getarg(sys.argv)
	tmp_file_prefix="tmp_seq"
	out_tmp=[]
	kmer_hash_table={}
	kmer_length_size=len(kmer_start)
	#if kmer_length_size < homopol_max :
		#homopol_max=kmer_length_size
		#print "homopolymere length size to long setted to the kmer size" 
	print "Splitting sequences"
	for i in range(0,thr,1):
		out_tmp.append(open(tmp_file_prefix+"."+str(i)+".seq",'w'))
	cp=0
	cp2=0
	for seq_record in SeqIO.parse(inF, "fastq"):
		out_tmp[cp].write(str(seq_record.seq).upper()+"\n")
		out_tmp[cp].write(str(seq_record.seq.reverse_complement()).upper()+"\n")
		cp=cp+1
		cp2=cp2+1
		if cp == thr :
			cp=0
	print "writing "+str(cp2)+" sequences"
	print "Done"
	print "-------------------------------"
	print "starting generating kmer table"
	for i in range(0,thr,1):
		out_tmp[i].close()
		thread = myThread(i, "Thread-"+str(i),tmp_file_prefix+"."+str(i)+".seq", kmer_hash_table, kmer_length_size)
		thread.start()
		threads.append(thread)
	# Wait for all threads to complete
	for t in threads:
		t.join()
	print "kmer loaded"
	print "creating "+str(len(kmer_hash_table))+" kmers"
	print "done"
	print "-------------------------------"
	print "starting generating the consensus sequence:"
	consensus_build=True
	seed=kmer_start
	consensus=seed
	consensus_count=".-"*kmer_length_size
	consensus_ambiguity=0
	kmer_end=["A"*homopol_max,"C"*homopol_max,"G"*homopol_max,"T"*homopol_max]
	while consensus_build:
		print "------"
		next_base=""
		next_base_count=0
		next_base_other=""
		next_base_other_count=0
		if kmer_hash_table.has_key(seed):
			print "seed :"+seed
			previous_seed=seed
			if max(kmer_hash_table[seed].values()) >= min_prop :
				print kmer_hash_table[seed]
				for i in ["A","C","T","G"]:
					if kmer_hash_table[seed][i] >= 10 :
						consensus_ambiguity=consensus_ambiguity+1
						if kmer_hash_table[seed][i] > next_base_count :
							next_base_other=next_base
							next_base=i
							next_base_other_count=next_base_count
							next_base_count=kmer_hash_table[seed][i]
						elif kmer_hash_table[seed][i] > next_base_other_count:
							next_base_other=i
							next_base_other_count=kmer_hash_table[seed][i]
				if next_base_other != "" :
					consensus=consensus+"("+next_base+"/"+next_base_other.lower()+")"
					consensus_count=consensus_count+"("+str(next_base_count)+"/"+str(next_base_other_count)+")-"
					
				else :
					consensus=consensus+next_base
					consensus_count=consensus_count+str(next_base_count)+"-"
				tmp_seed=seed+next_base
				seed=tmp_seed[1:(kmer_length_size+1)]
				print consensus
				if seed[-homopol_max:] in kmer_end :
					consensus_build=False
					print "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
					print "Consensus stopped - presence of an homplymere"
			else:
				consensus_build=False
				print "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
				print "Consensus stopped - no extension feasible"
			del kmer_hash_table[previous_seed]
		else :
			consensus_build=False
			print "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			print "Consensus stopped - seed not found"
		if len(consensus) >= max_length :
			consensus_build=False
			print "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
			print "Consensus stopped - max consus size "
	print "done"
	out=open(outF,'w')
	out.write(">Concensus\n")
	out.write(consensus+"\n")
	out.write("+Concensus_count\n")
	out.write(consensus_count[0:-1]+"\n")
	out.close()
main()

exit()





