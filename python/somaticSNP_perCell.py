#!/usr/bin/env/ python

### Mathieu Bourgey (2019/01/22)

import os
import sys
import string
import getopt
import re


a=open("position.tsv",'r')
l=a.readline()
pos={}
posS=[]
while l != "" :
    if not re.search("KI", l) and not re.search("GL", l) and not re.search("Position", l) :
        c=l.split()
        pos[c[0]+"_"+c[1]]=[]
        posS.append(c[0]+"_"+c[1])
    l=a.readline()

a.close()

header="Chr\tPos\tCell_Freq"
cell=[]
for file in os.listdir("res_per_cell") :
    if file.endswith(".pos") :
        nam=file.split(".")
        cell.append(nam[0])
        header=header+"\t"+nam[0]
        a=open("res_per_cell/"+file,'r')
        l=a.readline()
        while l != "" :
            if not re.search("KI", l) and not re.search("GL", l) and not re.search("Position", l) :
                c=l.split()
                pos[c[0]+"_"+c[1]].append(nam[0])
            l=a.readline()
        a.close()

out=open("somatic_snp_percell.tsv",'w')
out.write(header+"\n")
for i in posS :
    p=i.split("_")
    outL="\t".join(p)
    print [len(pos[i]), len(cell), round(float(len(pos[i]))/float(len(cell)),3)]
    outL=outL+"\t"+str(round(float(len(pos[i]))/float(len(cell)),3))
    for j in cell:
        if j in pos[i] :
            outL=outL+"\t1"
        else :
            outL=outL+"\t0"
    out.write(outL+"\n")

out.close()

    
    
    
        

    
