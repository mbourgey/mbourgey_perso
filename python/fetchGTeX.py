#!/usr/bin/env python

### Mathieu Bourgey (2019/10/16) - mathieu.bourgey@mcgill.ca
### extract eQTL GTEX value for Elena Kuzmin


import os
import sys
import string
import getopt
import re
import operator

        


#Read input data and output GTeX results if it exists
a=open("/lb/project/mugqic/projects/park_morris_scDNA_TD46/scNDA_PDX1735/pairedVariants/SA920_PDX_sequenza/SA920_PDX.GTeX.LOH_CNA.tsv",'r')
out1=open("/lb/project/mugqic/projects/park_morris_scDNA_TD46/scNDA_PDX1735/pairedVariants/SA920_PDX_sequenza/SA920_PDX.GTeX.LOH_CNA.annotated_BrainCortex.tsv",'w')
out2=open("/lb/project/mugqic/projects/park_morris_scDNA_TD46/scNDA_PDX1735/pairedVariants/SA920_PDX_sequenza/SA920_PDX.GTeX.other.annotated_BrainCortex.tsv",'w')
#out3=open("/lb/project/mugqic/projects/park_morris_scDNA_TD46/scNDA_PDX1735/pairedVariants/SA920_PDX_sequenza/SA920_PDX.GTeX.LOH_CNA.annotated_Brain.tsv",'w')
l=a.readline()
l=a.readline()
lohCNApos={}
while l != "":
       c=l.split()
       lohCNApos[c[1]+"_"+c[2]]=c
       l=a.readline()
a.close()

a=open("/lb/project/mugqic/projects/park_morris_scDNA_TD46/scNDA_PDX1735/pairedVariants/SA920_PDX_sequenza/SA920_PDX.GTeX.noLOH_CNA.tsv",'r')
l=a.readline()
l=a.readline()
nolohCNApos={}
while l != "":
       c=l.split()
       nolohCNApos[c[1]+"_"+c[2]]=c
       l=a.readline()
a.close()

out1.write("SNP\tChr\tPos\tLOH_status\tCNA_status\tGene\tImpact\tImpact_details\tEsembl Id\tVariant Id\tP-value\tNES (normalized effect size)\tTissue\n")
out2.write("SNP\tChr\tPos\tLOH_status\tCNA_status\tGene\tImpact\tImpact_details\tEsembl Id\tVariant Id\tP-value\tNES (normalized effect size)\tTissue\n")
#out3.write("SNP\tChr\tPos\tLOH_status\tCNA_status\tGene\tImpact\tImpact_details\tGTeX\tVariant Id\tGencode Id\tP-value\tNES (normalized effect size)\tTissue\n")

#Get GTeX data Mammary
a=open("/lb/project/mugqic/projects/park_morris_scDNA_TD46/scNDA_PDX1735/pairedVariants/Brain_Cortex.signifpairs.GTex.tsv",'r')
l=a.readline()
l=a.readline()
while l != "":
        c=l.split()
        pos=c[0].split("_")
        id=pos[0]+"_"+pos[1]
        outSup=c[1]+"\t"+c[0]+"\t"+c[6]+"\t"+c[7]+"\tBrain_Tissue"
        if lohCNApos.has_key(id):
            out1.write("\t".join(lohCNApos[id]) + "\t" +outSup +"\n") 
        elif  nolohCNApos.has_key(id):
            out2.write("\t".join(nolohCNApos[id]) + "\t" +outSup +"\n") 
        else :
            out2.write("\t".join([".",pos[0],pos[1],".",".",".",".","."]) + "\t" +outSup +"\n")
        l=a.readline()
a.close()

out1.close()
out2.close()
#out3.close()
