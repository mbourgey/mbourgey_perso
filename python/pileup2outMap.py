#!/usr/bin/python

### Mathieu Bourgey (2011/11/22) - mbourgey@genomequebec.com
### generate wiggle of mean mapQ from mpileup file

import os
import sys
import string
import getopt
import re



class exclusionZ :
	def __init__(self) :
		self.ex=False
		self.ch="0"
		self.st=0
		self.en=0
		self.ec=[]
		
	def writer(self,out) :
		ecode=self.ec[0]
		if len(self.ec) > 1 :
			for i in range(1,len(self.ec),1):
				ecode=ecode+"_"+self.ec[i]
		out.write(self.ch+"\t"+str(self.st)+"\t"+str(self.en)+"\t"+ecode+"\n")


def getarg(argument):
	fil=""
	out=""
	minQ=20
	maxC=40
	minC=5
	optli,arg = getopt.getopt(argument[1:],"f:m:c:d:o:l:h",['file','minMQ','cov','minC','output','length','help'])
	#optli,arg = getopt.getopt(argument[1:],"f:o:h",['file','output','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			fil=str(value)
		if option in ("-m","--minMQ"):
			minQ=int(value)
		if option in ("-c","--cov"):
			maxC=int(value)
		if option in ("-d","--minC"):
			minC=int(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-l","--length"):
			lfi=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(fil) :
		usage()
		sys.exit("Error - mpileUp file not found:\n"+str(fil))
        if out == "" :
		usage()
		sys.exit("Error - mpileUp no output file location provided")
	return fil, minQ, maxC, minC, out, lfi



def usage():
	print "\n-------------------------------------------------------------------------------------------"
	print "pileup2outMap.py generate statistics, wiggle files and exclusion bed map according mapping"
	print "quality and coverage of each base (use information for a samtools mpileUp file)"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "--------------------------------------------------------------------------------------------\n"
	print "USAGE : pileup2outMap.py [option] "
	print "       -f :        mpileup file"
	print "       -o :        output file"
	print "Optional arguments: "
	print "       -m :        min mapQ (default 20)"
	print "       -d :        min coverage (default 10)"
	print "       -c :        max coverage (default 1000)"
	print "       -l :        chromsome length file (size TAB chrName)"
	print "       -h :        this help \n"


def ChroLength(x) :
	dico={}
	f=open(x,'r')
	l=f.readline()
	while l != "" :
		c=l.split()
		dico[c[1]]=int(c[0])
		l=f.readline()
	f.close()
	return dico

def setDist(d,e,v):
	if d.has_key(e) :
		d[e]=d[e]+v
	else :
		d[e]=v
	return d

def writeDist(d,o):
	keys=d.keys()
	mval=max(keys)+1
	for i in range(0,mval,1) :
		if d.has_key(i) :
			o.write(str(i)+"\t"+str(d[i])+"\n")
		else :
			o.write(str(i)+"\t0\n")


def main():
	#fiP, span, outP  = getarg(sys.argv)
	fiP, mq, mac, mic, outP, lfi  = getarg(sys.argv)
	print "mpileup file: "+fiP
	print "output file: "+outP
	print "min mapQ: "+str(mq)
	print "min coverage: "+str(mic)
	print "max coverage: "+str(mac)
	fi=open(fiP,'r')
	out4=open(outP+".exclusion.bed",'w')
	out4.write("#exclusion code: HC = to high coverage (>"+str(mac)+"); LC = to high coverage (<"+str(mic)+"); MQ = to low mean mapQ (<"+str(mq)+"); ND = no data at the position\n")
	##to do: exclusion bed file: chr start end reasons(HC, LC, MQ,ND)
	##to do file file: cov #pos covMQ #pos meanMQ #pos
	clen=ChroLength(lfi)
	covD={}
	meanMQD={}
	covMQD={}
	chrPrev="0"
	li=fi.readline()
	clt=1
	posPrev=0
	exc=exclusionZ()
	while li != "" :
		ci=li.split()
		chrCur=str(ci[0])
		posCur=int(ci[1])
		if chrPrev != chrCur :
			if chrPrev != "0":
				posN=clen[chrPrev]-posPrev
				if posN >= 0 :
					##exclusion
					if exc.ex :
						exc.en=clen[chrPrev]
						if not "ND" in exc.ec :
							exc.ec.append("ND")
					else :
						exc.ex=False
						exc.ch=chrPrev
						exc.st=posPrev+1
						exc.en=clen[chrPrev]
						exc.ec.append("ND")
					exc.writer(out4)
					exc=exclusionZ()
			chrPrev=chrCur
			posPrev=0
		if (posPrev+1) != posCur :
			##exclusion
			if exc.ex :
				exc.en=posCur-1
				if not "ND" in exc.ec :
					exc.ec.append("ND")
			else :
				exc.ex=True
				exc.ch=chrCur
				exc.st=posPrev+1
				exc.en=posCur-1
				exc.ec.append("ND")
			posPrev=posCur
		pos=str(ci[1])
		mapV=str(ci[6])
		l=0
		mapT=0
		covQ=0
		for i in mapV:
			l=l+1
			mapT=mapT+(ord(i)-33)
			if (ord(i)-33) >= mq:
				covQ=covQ+1
		mapM=str(mapT/l)
		#exclusion
		if mapM < mq :
			if exc.ex :
				exc.en=posCur
				if not "MQ" in exc.ec :
					exc.ec.append("MQ")
			else :
				exc.ex=True
				exc.ch=chrCur
				exc.st=posCur
				exc.en=posCur
				exc.ec.append("MQ")
		if covQ < mic :
			if exc.ex :
				exc.en=posCur
				if not "LC" in exc.ec :
					exc.ec.append("LC")
			else :
				exc.ex=True
				exc.ch=chrCur
				exc.st=posCur
				exc.en=posCur
				exc.ec.append("LC")
		if covQ > mac :
			if exc.ex :
				exc.en=posCur
				if not "HC" in exc.ec :
					exc.ec.append("HC")
			else :
				exc.ex=True
				exc.ch=chrCur
				exc.st=posCur
				exc.en=posCur
				exc.ec.append("HC")
		if (posCur != exc.en) and exc.ex and  chrPrev == chrCur :
			exc.writer(out4)
			exc=exclusionZ()
		li=fi.readline()
		chrPrev=chrCur
		posPrev=posCur
	fi.close()
	out4.close()
	#cov statistics output
	print "Output exclusion bed file :\n"+outP+".exclusion.bed"

	

main()






