#!/usr/bin/python

### Mathieu Bourgey (2019/11/19) - mathieu.bourgey@mcgill.ca
### get intersect from LOH and CNA region


import os
import sys
import string
import getopt
import re


a=open("tmp_intersect.2.tsv",'r')
l=a.readline()
loh={}
#dictionnary of list (inetrvals with different  CNA) of list (chr, start, end, CNA)
keyorder=[]
while l != "":
    c=l.split()
    if loh.has_key(c[0]+"_"+c[1]) :
        #cut the previous end of interval to include the new one
        loh[c[0]+"_"+c[1]][-1][2]=str(int(c[4])-1)
        if int(c[2]) <= int(c[5]) :  #CNA region finish before the end of interval => add region of ploidy 3
            loh[c[0]+"_"+c[1]].append([c[3],c[4],c[2],c[6]])
        else : # CNA region finish before the end of interval => add region of ploidy 3
            loh[c[0]+"_"+c[1]].append([c[3],c[4],c[5],c[6]])
            loh[c[0]+"_"+c[1]].append([c[3],str(int(c[5])+1),c[2],"3"])
    else :
        loh[c[0]+"_"+c[1]]=[]
        keyorder.append(c[0]+"_"+c[1])
        if c[3] == "." : #no intersect
            loh[c[0]+"_"+c[1]].append([c[0],c[1],c[2],"3"])
        else :
            if int(c[1]) < int(c[4]) : # add region of ploidy 3 before the intersection
                loh[c[0]+"_"+c[1]].append([c[3],c[1],str(int(c[4])-1),"3"])
                if int(c[2]) <= int(c[5]) :
                    loh[c[0]+"_"+c[1]].append([c[3],c[4],c[2],c[6]])
                else : # CNA region finish before the end of interval => add region of ploidy 3
                    loh[c[0]+"_"+c[1]].append([c[3],c[4],c[5],c[6]])
                    loh[c[0]+"_"+c[1]].append([c[3],str(int(c[5])+1),c[2],"3"])
            else :
                if int(c[2]) <= int(c[5]) :
                    loh[c[0]+"_"+c[1]].append([c[3],c[1],c[2],c[6]])
                else : # CNA region finish before the end of interval => add region of ploidy 3
                    loh[c[0]+"_"+c[1]].append([c[3],c[1],c[5],c[6]])
                    loh[c[0]+"_"+c[1]].append([c[3],str(int(c[5])+1),c[2],"3"])
    l=a.readline()

a.close()
out=open("SA920_PDX_segmentsLOH_CNA.tsv",'w')
for i in keyorder:
    for j in loh[i] :
        out.write("\t".join(j)+"\n")
out.close()
                                               
  
