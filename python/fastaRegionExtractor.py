#!/usr/bin/python

### Mathieu Bourgey (2014/03/21)
### extract sequence based on a given coordinate

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO



def getarg(argument):
	dirF="fasta"
	inF=""
	outF=""
	optli,arg = getopt.getopt(argument[1:],"i:o:r:f:h",['input','out','region','format','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-r","--region"):
			reg=str(value)   
		if option in ("-f","--format"):
			if (str(value) == "fastq" or str(value) == "fasta"):
				dirF=str(value)   
			else :
				print "unknow format: use fasta"
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, reg, dirF



def usage():
	print "USAGE : fastaRegionExtractor.py [option] "
	print "       -i :        input file"
	print "       -o :        output file"
	print "       -r :        region to capture (format chr:start-end)"
	print "       -f :        format input/output file:" 
	print "                      fasta: for fasta file (default)"
	print "                      fastq: for fastq file"
	print "       -h :        this help \n"


def main():
	print "\n------------------------------------------------------------"
	print "fastaRegionExtractor.py will generate fasta or fastq file while only keeping "
	print "sequences of the specified region" 
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, reg, dirF = getarg(sys.argv)
	info=reg.split(":")
	chrT=info[0]
	pos=info[1].split("-")
	if (int(pos[0]) >= 1) :
		for seq_record in SeqIO.parse(inF, dirF):
			if chrT == seq_record.id :
				if int(pos[1]) <= len(seq_record.seq):
					tragetR=seq_record[(int(pos[0])-1):int(pos[1])]
					tragetR.id=reg
					SeqIO.write(tragetR, outF, dirF)
				else:
					print "wrong region given (end)"
				break
	else:
		print "wrong region given (start)"
	
	
main()

	    
	
	





