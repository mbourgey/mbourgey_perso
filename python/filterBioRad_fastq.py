#!/usr/bin/env python

## filter and format bioRad Fastq files 
### Mathieu Bourgey (2017/10/23) - mathieu.bourgey@mcgill.ca


import os
import sys
import string
import getopt
import re
import itertools
from  ngs_plumbing import parsing 
import regex
import gzip
import editdistance

def getarg(argument):
        r1=""
        r2=""
        out=""
        bc=""
        lin=""
        li_num=200000
        optli,arg = getopt.getopt(argument[1:],"f:s:o::b:l:h",['first','second','out','barecode','linker','help'])
        if len(optli) == 0 :
                usage()
                sys.exit("Error : No argument given")
        for option, value in optli:
                if option in ("-f","--first"):
                        r1=str(value)
                if option in ("-s","--second"):
                        r2=str(value)
                if option in ("-l","--linker"):
                        lin=str(value)
                if option in ("-b","--barcode"):
                        bc=str(value)
                if option in ("-o","--out"):
                        out=str(value)
                if option in ("-h","--help"):
                        usage()
                        sys.exit()
        if not os.path.exists(r1) :
                sys.exit("Error - reads 1 file not found:\n"+str(r1))
        if not os.path.exists(r2) :
                sys.exit("Error - reads 2 file not found:\n"+str(r2))
        if not os.path.exists(bc) :
                sys.exit("Error - barcode file not found:\n"+str(bc))
        if not os.path.exists(lin) :
                sys.exit("Error - linker file not found:\n"+str(lin))
        return r1, r2, out, lin, bc
        
def usage():
        print "USAGE : filterBioRad_fastq.py [option] "
        print "       -f :        reads 1 fastq.gz file"
        print "       -s :        reads 2 fastq.gz file"
        print "       -l :        linker file"
        print "       -b :        barcode file"
        print "       -o :        output read2 fastq.gz file"
        print "       -h :        this help \n"


def getLinker(l):
  f=open(l,'r')
  lin=[]
  l=f.readline()
  while l != "" :
    c=l.split()
    lin.append(c[0])
    l=f.readline()
  f.close()
  return lin

def getCorrectedBarcode(b,bl):
  barcode="XXXXXX"
  if b in bl :
    barcode=b
  else :
    for br in bl :
      if editdistance.eval(br,b) <= 1 :
        barcode=br
        break
  return barcode
  

def main():
  print "\n---------------------------------------------------------------------------------"
  print "filterBioRad_fastq.py takes paired fastq.gz files and return the read 2 off valid"
  print "sequence while extracting the barcode and UMI information."
  print "This program was written by Mathieu Bourgey"
  print "For more information, contact: mathieu.bourgey@mcgill.ca"
  print "----------------------------------------------------------------------------------\n"
  reads1_file, reads2_file, out2_file, linker, barcode  = getarg(sys.argv)
  sequence_pairs = itertools.izip(parsing.open(reads1_file),parsing.open(reads2_file))
  linker_list=getLinker(linker)
  barcode_list=getLinker(barcode)
  rejected_l2=0
  rejected_l1=0
  rejected_bc3=0
  rejected_bc2=0
  rejected_bc1=0
  rejected_ACG_poly=0
  keep=0
  out=gzip.open(out2_file, 'wb')
  for pair in sequence_pairs:
    #find linker 2 perfect match
    l2_match=pair[0].sequence.find(linker_list[1])
    if l2_match == -1 :
      #not perfect match loking with 1ED
      if regex.findall("("+linker_list[1]+"){e<=1}",pair[0].sequence) != []:
        l2_match=pair[0].sequence.find(regex.findall("("+linker_list[1]+"){e<=1}",pair[0].sequence)[0])
    if l2_match != -1 and l2_match >= 27 :
        l1_match = l2_match - 21
        l1_ED=editdistance.eval(pair[0].sequence[l1_match:l1_match+15],linker_list[0])
        if l1_ED <= 1 :
          acg_match = l2_match + 21
          acg = pair[0].sequence[acg_match:acg_match+3]
          poly_match = l2_match + 32
          poly = pair[0].sequence[poly_match:poly_match+5]
          acg_poly_ED=editdistance.eval(acg+poly,"ACGGACTT")
          if acg_poly_ED <= 1 :
            bc1_match = l2_match - 27
            bc1 = pair[0].sequence[bc1_match:bc1_match+6]
            cbc1=getCorrectedBarcode(bc1,barcode_list)
            if cbc1 != "XXXXXX" :
              bc2_match = l2_match - 6
              bc2 = pair[0].sequence[bc2_match:bc2_match+6]
              cbc2=getCorrectedBarcode(bc2,barcode_list)
              if cbc2 != "XXXXXX" :
                bc3_match = l2_match + 15
                bc3 = pair[0].sequence[bc3_match:bc3_match+6]
                cbc3=getCorrectedBarcode(bc3,barcode_list)
                if cbc3 != "XXXXXX" :
                  keep+= 1
                  umi_match = l2_match + 24
                  umi = pair[0].sequence[umi_match:umi_match+8]
                  head=pair[1].header.split()
                  out.write(head[0] + "_CB:Z:" + cbc1 + cbc2 + cbc3 + "_CR:Z:" + bc1 + bc2 + bc3 + "_UR:Z:" + umi + " " + head[1] + "\n")
                  out.write(pair[1].sequence+"\n")
                  out.write("+\n")
                  out.write(pair[1].quality+"\n")
                else :
                  rejected_bc3+= 1
              else:
                rejected_bc2+= 1
            else :
              rejected_bc1+= 1
          else :
            rejected_ACG_poly+= 1
        else:
          rejected_l1+= 1
    else :
      rejected_l2+= 1
  out.close()
  rejected=rejected_l2+rejected_l1+rejected_bc3+rejected_bc2+rejected_bc1+rejected_ACG_poly
  print "Kept\tRejected_total\tRejected_Linker2\tRejected_Linker1\tRejected_Barcode3\tRejected_Barcode2\tRejected_Barcode1\tRejected_ACG_polyA"
  print  "\t".join([str(keep),str(rejected),str(rejected_l2),str(rejected_l1),str(rejected_bc3),str(rejected_bc2),str(rejected_bc1),str(rejected_ACG_poly)]) + "\n"

main()


