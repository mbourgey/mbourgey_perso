#!/usr/bin/env python

### Mathieu Bourgey (2014/10/27) - mbourgey@genomequebec.com
### get synchronize baseFreq output wiuth snp positions


import os
import sys
import string
import getopt
import re



def getarg(argument):
	f=""
	g=""
	s="S"
	out=""
	optli,arg = getopt.getopt(argument[1:],"f:o:p:h",['file','output','pos','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			f=str(value)
		if option in ("-p","--pos"):
			g=str(value)
#		if option in ("-t","--type"):
#			t=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(f) :
		sys.exit("Error - file not found:\n"+str(f))
	if not os.path.exists(g):
		sys.exit("Error - Positions not found:\n"+str(g))
	return f, g, out

def usage():
	print "\n---------------------------------------------------------------------------------"
	print "BaseF2RatioBAF_synchronize.py synchronize baseFreq and snp position input for RatioBaf"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : GetRecurenceSV.py [option] "
	print "       -f :        basefreq output file"
	print "       -p :        candidate position file"
##	print "       -t :        SV onco type A: all S: somatic (default S)"
	print "       -o :        output basename"
	print "       -h :        this help \n"

    
def main():
##	f1, ge, sa, out = getarg(sys.argv)
	f1 , ge, out =  getarg(sys.argv)
	d={}
	f=open(ge,'r')
	l=f.readline()
	while l != "" :
            c=l.split()
            d[c[0]+"_"+c[1]]=0
            l=f.readline()
        f.close()
	f=open(f1,'r')
	o=open(out,'w')
	li=f.readline()
	o.write(li)
	li=f.readline()
	while li != "" :
                co=li.split(",")
                if d.has_key(co[0]+"_"+co[1]) :
                    o.write(li)
                    del d[co[0]+"_"+co[1]]
                li=f.readline()
        f.close()
        o.close()


main()

