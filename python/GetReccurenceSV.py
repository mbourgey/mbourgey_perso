#!/usr/bin/python

### Mathieu Bourgey (2013/07/31) - mbourgey@genomequebec.com
### get by gene reccurence of SV events


import os
import sys
import string
import getopt
import re

class genSVcall :
	def __init__(self) :
		self.dup=[]
		self.dele=[]
		self.inv=[]
		self.tra=[]
		self.dupS=[]
		self.deleS=[]
		self.invS=[]
		self.traS=[]
		self.dupG=[]
		self.deleG=[]
		self.invG=[]
		self.traG=[]
		self.dupA=[]
		self.deleA=[]
		self.invA=[]
		self.traA=[]
		

def getarg(argument):
	f=""
	g=""
	s="S"
	out=""
	optli,arg = getopt.getopt(argument[1:],"f:o:g:s:h",['file','output','gene','sample','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			f=str(value)
		if option in ("-g","--gene"):
			g=str(value)
#		if option in ("-t","--type"):
#			t=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(f) :
		sys.exit("Error - file not found:\n"+str(f))
	if os.path.exists(g):
		return f, g, out
	else : 
		return f, ".", out

def usage():
	print "\n---------------------------------------------------------------------------------"
	print "RecurenceSV.py provided detailed recurence list based on SV filtered calls"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : GetRecurenceSV.py [option] "
	print "       -f :        filtered SV calls file"
	print "       -g :        optional: candidate gene file"
##	print "       -t :        SV onco type A: all S: somatic (default S)"
	print "       -o :        output basename"
	print "       -h :        this help \n"


def getRec(x,d):
##        head="Gene,ov-Del,ov-Dup,ov-Inv,ov-Tra,ov-Total,ov-Del-Sample,ov-Dup-Sample,ov-Inv-Sample,ov-Tra-Sample"
##        head=head +",som-Del,som-Dup,som-Inv,som-Tra,som-Total,som-Del-Sample,som-Dup-Sample,som-Inv-Sample,som-Tra-Sample"
##        head=head +",germ-Del,germ-Dup,germ-Inv,germ-Tra,germ-Total,germ-Del-Sample,germ-Dup-Sample,germ-Inv-Sample,germ-Tra-Sample"
##        head=head +",amb-Del,amb-Dup,amb-Inv,amb-Tra,amb-Total,amb-Del-Sample,amb-Dup-Sample,amb-Inv-Sample,amb-Tra-Sample"
        wt=False
        o="."
        if d.has_key(x):
                wt=True
                o=x
                os=""
                t=0
                if len(d[x].dele) >= 1 :
                        t=t+len(d[x].dele)
                        o=o+","+str(len(d[x].dele))
                        os=os+","+":".join(d[x].dele)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].dup) >= 1 :
                        t=t+len(d[x].dup)
                        o=o+","+str(len(d[x].dup))
                        os=os+","+":".join(d[x].dup)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].inv) >= 1 :
                        t=t+len(d[x].inv)
                        o=o+","+str(len(d[x].inv))
                        os=os+","+":".join(d[x].inv)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].tra) >= 1 :
                        t=t+len(d[x].tra)
                        o=o+","+str(len(d[x].tra))
                        os=os+","+":".join(d[x].tra)
                else :
                        o=o+",."
                        os=os+",."
                o=o+","+str(t)+os
                os=""
                t=0
                if len(d[x].deleS) >= 1 :
                        t=t+len(d[x].deleS)
                        o=o+","+str(len(d[x].deleS))
                        os=os+","+":".join(d[x].deleS)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].dupS) >= 1 :
                        t=t+len(d[x].dupS)
                        o=o+","+str(len(d[x].dupS))
                        os=os+","+":".join(d[x].dupS)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].invS) >= 1 :
                        t=t+len(d[x].invS)
                        o=o+","+str(len(d[x].invS))
                        os=os+","+":".join(d[x].invS)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].traS) >= 1 :
                        t=t+len(d[x].traS)
                        o=o+","+str(len(d[x].traS))
                        os=os+","+":".join(d[x].traS)
                else :
                        o=o+",."
                        os=os+",."
                o=o+","+str(t)+os
                os=""
                t=0
                if len(d[x].deleG) >= 1 :
                        t=t+len(d[x].deleG)
                        o=o+","+str(len(d[x].deleG))
                        os=os+","+":".join(d[x].deleG)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].dupG) >= 1 :
                        t=t+len(d[x].dupG)
                        o=o+","+str(len(d[x].dupG))
                        os=os+","+":".join(d[x].dupG)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].invG) >= 1 :
                        t=t+len(d[x].invG)
                        o=o+","+str(len(d[x].invG))
                        os=os+","+":".join(d[x].invG)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].traG) >= 1 :
                        t=t+len(d[x].traG)
                        o=o+","+str(len(d[x].traG))
                        os=os+","+":".join(d[x].traG)
                else :
                        o=o+",."
                        os=os+",."
                o=o+","+str(t)+os
                os=""
                t=0
                if len(d[x].deleA) >= 1 :
                        t=t+len(d[x].deleA)
                        o=o+","+str(len(d[x].deleA))
                        os=os+","+":".join(d[x].deleA)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].dupA) >= 1 :
                        t=t+len(d[x].dupA)
                        o=o+","+str(len(d[x].dupA))
                        os=os+","+":".join(d[x].dupA)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].invA) >= 1 :
                        t=t+len(d[x].inv)
                        o=o+","+str(len(d[x].invA))
                        os=os+","+":".join(d[x].invA)
                else :
                        o=o+",."
                        os=os+",."
                if len(d[x].traA) >= 1 :
                        t=t+len(d[x].traA)
                        o=o+","+str(len(d[x].traA))
                        os=os+","+":".join(d[x].traA)
                else :
                        o=o+",."
                        os=os+",."
                o=o+","+str(t)+os
        return wt, o

def getCandidate(x) :
	f=open(x,'r')
	l=f.readline()
	cand=[]
	while l != "" :
		c=l.split()
		cand.append(c[0])
		l=f.readline()
	f.close()
	return cand
        
        
def main():
##	f1, ge, sa, out = getarg(sys.argv)
	f1 , ge, out =  getarg(sys.argv)
	candUse=False
	if ge != "." :
		candUse=True
		candList=getCandidate(ge)
	fil=open(f1,'r')
	geneDict={}
	utrDict={}
	#sampleRec={}
	li=fil.readline()
	while li != "" :
                co=li.split()
                if len(co) == 15 :
                        if co[11] != "." :
                                genList=co[11].split(":")
                                for i in range(0,len(genList),1) :
                                        if not geneDict.has_key(genList[i]) :
                                                geneDict[genList[i]]=genSVcall()
                                        if co[7] == "DEL" :
                                                if co[1] not in geneDict[genList[i]].dele :
                                                        geneDict[genList[i]].dele.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].deleA :
                                                                geneDict[genList[i]].deleA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].deleG :
                                                                geneDict[genList[i]].deleG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].deleS :
                                                                geneDict[genList[i]].deleS.append(co[1])
                                        elif co[7] == "DUP" :
                                                if co[1] not in geneDict[genList[i]].dup :
                                                        geneDict[genList[i]].dup.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].dupA :
                                                                geneDict[genList[i]].dupA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].dupG :
                                                                geneDict[genList[i]].dupG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].dupS :
                                                                geneDict[genList[i]].dupS.append(co[1])
                                        elif co[7] == "TRA" :
                                                if co[1] not in geneDict[genList[i]].tra :
                                                        geneDict[genList[i]].tra.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].traA :
                                                                geneDict[genList[i]].traA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].traG :
                                                                geneDict[genList[i]].traG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].traS :
                                                                geneDict[genList[i]].traS.append(co[1]) 
                                        elif co[7] == "INV" :
                                                if co[1] not in geneDict[genList[i]].inv :
                                                        geneDict[genList[i]].inv.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].invA :
                                                                geneDict[genList[i]].invA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].invG :
                                                                geneDict[genList[i]].invG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].invS :
                                                                geneDict[genList[i]].invS.append(co[1])
                        elif co[12] != "." :
                                genList=co[12].split(":")
                                for i in range(0,len(genList),1) :
                                        if not geneDict.has_key(genList[i]) :
                                                geneDict[genList[i]]=genSVcall()
                                        if co[7] == "DEL" :
                                                if co[1] not in geneDict[genList[i]].dele :
                                                        geneDict[genList[i]].dele.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].deleA :
                                                                geneDict[genList[i]].deleA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].deleG :
                                                                geneDict[genList[i]].deleG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].deleS :
                                                                geneDict[genList[i]].deleS.append(co[1])
                                        elif co[7] == "DUP" :
                                                if co[1] not in geneDict[genList[i]].dup :
                                                        geneDict[genList[i]].dup.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].dupA :
                                                                geneDict[genList[i]].dupA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].dupG :
                                                                geneDict[genList[i]].dupG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].dupS :
                                                                geneDict[genList[i]].dupS.append(co[1])
                                        elif co[7] == "TRA" :
                                                if co[1] not in geneDict[genList[i]].tra :
                                                        geneDict[genList[i]].tra.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].traA :
                                                                geneDict[genList[i]].traA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].traG :
                                                                geneDict[genList[i]].traG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].traS :
                                                                geneDict[genList[i]].traS.append(co[1]) 
                                        elif co[7] == "INV" :
                                                if co[1] not in geneDict[genList[i]].inv :
                                                        geneDict[genList[i]].inv.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  geneDict[genList[i]].invA :
                                                                geneDict[genList[i]].invA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  geneDict[genList[i]].invG :
                                                                geneDict[genList[i]].invG.append(co[1])
                                                else :
                                                        if co[1] not in  geneDict[genList[i]].invS :
                                                                geneDict[genList[i]].invS.append(co[1])
                        elif co[13] != ".":
                                genList=co[13].split(":")
                                for i in range(0,len(genList),1) :
                                        if not utrDict.has_key(genList[i]) :
                                                utrDict[genList[i]]=genSVcall()
                                        if co[7] == "DEL" :
                                                if co[1] not in utrDict[genList[i]].dele :
                                                        utrDict[genList[i]].dele.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  utrDict[genList[i]].deleA :
                                                                utrDict[genList[i]].deleA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  utrDict[genList[i]].deleG :
                                                                utrDict[genList[i]].deleG.append(co[1])
                                                else :
                                                        if co[1] not in  utrDict[genList[i]].deleS :
                                                                utrDict[genList[i]].deleS.append(co[1])
                                        elif co[7] == "DUP" :
                                                if co[1] not in utrDict[genList[i]].dup :
                                                        utrDict[genList[i]].dup.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  utrDict[genList[i]].dupA :
                                                                utrDict[genList[i]].dupA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  utrDict[genList[i]].dupG :
                                                                utrDict[genList[i]].dupG.append(co[1])
                                                else :
                                                        if co[1] not in  utrDict[genList[i]].dupS :
                                                                utrDict[genList[i]].dupS.append(co[1])
                                        elif co[7] == "TRA" :
                                                if co[1] not in utrDict[genList[i]].tra :
                                                        utrDict[genList[i]].tra.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  utrDict[genList[i]].traA :
                                                                utrDict[genList[i]].traA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  utrDict[genList[i]].traG :
                                                                utrDict[genList[i]].traG.append(co[1])
                                                else :
                                                        if co[1] not in  utrDict[genList[i]].traS :
                                                                utrDict[genList[i]].traS.append(co[1]) 
                                        elif co[7] == "INV" :
                                                if co[1] not in utrDict[genList[i]].inv :
                                                        utrDict[genList[i]].inv.append(co[1])
                                                if co[9] == "A" :
                                                        if co[1] not in  utrDict[genList[i]].invA :
                                                                utrDict[genList[i]].invA.append(co[1])
                                                elif co[9] == "NT" :
                                                        if co[1] not in  utrDict[genList[i]].invG :
                                                                utrDict[genList[i]].invG.append(co[1])
                                                else :
                                                        if co[1] not in  utrDict[genList[i]].invS :
                                                                utrDict[genList[i]].invS.append(co[1])
                li=fil.readline()
        fil.close()
        geneAll=geneDict.keys()
        geneAll.extend(utrDict.keys())
        geneAllUnique=list(set(geneAll))
        head="Gene,ov-Del,ov-Dup,ov-Inv,ov-Tra,ov-Total,ov-Del-Sample,ov-Dup-Sample,ov-Inv-Sample,ov-Tra-Sample"
        head=head +",som-Del,som-Dup,som-Inv,som-Tra,som-Total,som-Del-Sample,som-Dup-Sample,som-Inv-Sample,som-Tra-Sample"
        head=head +",germ-Del,germ-Dup,germ-Inv,germ-Tra,germ-Total,germ-Del-Sample,germ-Dup-Sample,germ-Inv-Sample,germ-Tra-Sample"
        head=head +",amb-Del,amb-Dup,amb-Inv,amb-Tra,amb-Total,amb-Del-Sample,amb-Dup-Sample,amb-Inv-Sample,amb-Tra-Sample"
        outG=open(out+"GeneRec.csv",'w')
        outU=open(out+"UTRRec.csv",'w')
        if candUse:
		outGC=open(out+"_candidate_GeneRec.csv",'w')
		outUC=open(out+"_candidate_UTRRec.csv",'w')
		outGC.write(head+"\n")
		outUC.write(head+"\n")
        outG.write(head+"\n")
        outU.write(head+"\n")
        for i in geneAllUnique :
                writeTest, ovOut=getRec(i,geneDict)
                if writeTest :
                        outG.write(ovOut+"\n")
                        if candUse:
				if i in candList :
					outGC.write(ovOut+"\n")
                writeTest, ovOut=getRec(i,utrDict)
                if writeTest :
                        outU.write(ovOut+"\n")
                        if candUse:
				if i in candList :
					outUC.write(ovOut+"\n")
	outG.close()
	outU.close()
	if candUse:
		outGC.close()
		outUC.close()

main()

