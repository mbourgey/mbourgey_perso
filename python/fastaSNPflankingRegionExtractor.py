#!/usr/bin/python

### Mathieu Bourgey (2014/10/07)
### extract sequence based on a given SNP position

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO
import vcf

class chromSnpList :
	def __init__(self) :
		self.pos=[]
		self.nam=[]

def getarg(argument):
	dirF="fasta"
	fla=50
	optli,arg = getopt.getopt(argument[1:],"i:o:s:f:h",['input','out','snp','flanking','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-s","--snp"):
			regF=str(value)   
		if option in ("-f","--flanking"):
			fla=int(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, regF, fla



def usage():
	print "USAGE : fastaSNPflankingRegionExtractor.py [option] "
	print "       -i :        input file"
	print "       -o :        output file"
	print "       -s :        SNP vcf 4 file"
	print "       -f :        flanking size (default: 50)"
	print "       -h :        this help \n"


def main():
	print "\n------------------------------------------------------------"
	print "fastaSNPflankingRegionExtractor.py will generate fasta file while only keeping "
	print "flanmking sequences of the specified list of SNPs" 
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, vcfF, fla= getarg(sys.argv)
	snp={}
	records=[]
	print "loading SNPs ..."
	vcf_reader = vcf.Reader(open(vcfF,'r'))
	o=open(outF,'w')
	for record in vcf_reader:
		if not snp.has_key(record.CHROM) :
			snp[record.CHROM]=chromSnpList()
		snp[record.CHROM].pos.append(record.POS)
		snp[record.CHROM].nam.append(record.ID)
	print "... done"
	print "Extracting sequences ..."
	for seq_record in SeqIO.parse(inF, "fasta"):
		if snp.has_key(seq_record.id) :
			for i in range(0,len(snp[seq_record.id].pos),1) :
				targetR=seq_record[(int(snp[seq_record.id].pos[i])-fla):(int(snp[seq_record.id].pos[i])+fla)]
				targetR.id="_".join([seq_record.id,str(snp[seq_record.id].pos[i]),str(snp[seq_record.id].nam[i])])
				o.write("> "+str(targetR.id)+"\n")
				o.write(str(targetR.seq)+"\n")
	print "... done"
	o.close()
	
	
main()

	    
	
	





