### Mathieu Bourgey (2014/02/27)
### generate legoPlot from 96 mutation table 

import os
import sys
import string
import getopt
import re
from Bio import SeqIO
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


class legoPlot :
	def __init__(self):
		self.mapping=["TT","TC","TA","TG","CT","CC","CA","CG","AT","AC","AA","AG","GT","GC","GA","GG"]
		##### each section of the plot is a hash containing bar3d parameter except the dz (values) initialize at 0
		##### each section as the same context order in all arrays, the order is defined by the maping TT : T_T ; TC :T_C ; etc...
		#commun parameter
		self.zpos=np.zeros(16)
		self.dx=np.ones_like(self.zpos)-0.2
		self.dy=np.ones_like(self.zpos)-0.2
		self.section={}
		#C->T section
		self.section["CT"]={}
		self.section["CT"]["values"]=np.zeros(16)
		self.section["CT"]["xpos"]=np.array([3,2,1,0,3,2,1,0,3,2,1,0,3,2,1,0])
		self.section["CT"]["ypos"]=np.array([0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3])
		self.section["CT"]["col"]="yellow"
		#C->A section
		self.section["CA"]={}
		self.section["CA"]["values"]=np.zeros(16)
		self.section["CA"]["xpos"]=self.section["CT"]["xpos"].copy()+4
		self.section["CA"]["ypos"]=self.section["CT"]["ypos"].copy()
		self.section["CA"]["col"]="blue"
		#C->G section
		self.section["CG"]={}
		self.section["CG"]["values"]=np.zeros(16)
		self.section["CG"]["xpos"]=self.section["CA"]["xpos"].copy()+4
		self.section["CG"]["ypos"]=self.section["CA"]["ypos"].copy()
		self.section["CG"]["col"]="red"
		#A->G section
		self.section["AG"]={}
		self.section["AG"]["values"]=np.zeros(16)
		self.section["AG"]["xpos"]=self.section["CT"]["xpos"].copy()
		self.section["AG"]["ypos"]=self.section["CT"]["ypos"].copy()+4
		self.section["AG"]["col"]="green"
		#A->C section
		self.section["AC"]={}
		self.section["AC"]["values"]=np.zeros(16)
		self.section["AC"]["xpos"]=self.section["AG"]["xpos"].copy()+4
		self.section["AC"]["ypos"]=self.section["AG"]["ypos"].copy()
		self.section["AC"]["col"]="darkblue"
		#A->T section
		self.section["AT"]={}
		self.section["AT"]["values"]=np.zeros(16)
		self.section["AT"]["xpos"]=self.section["AC"]["xpos"].copy()+4
		self.section["AT"]["ypos"]=self.section["AC"]["ypos"].copy()
		self.section["AT"]["col"]="purple"
		
	def plot(self,outFile):
		#### generate the leogPlot and save the figure in the given file
		fig =plt.figure()
		ax=fig.add_subplot(111,projection='3d')
		ax.bar3d(self.section["CT"]["xpos"], self.section["CT"]["ypos"], self.zpos, self.dx, self.dy, self.section["CT"]["values"], color=self.section["CT"]["col"], zsort='max')
		ax.bar3d(self.section["CA"]["xpos"], self.section["CA"]["ypos"], self.zpos, self.dx, self.dy, self.section["CA"]["values"], color=self.section["CA"]["col"], zsort='max')
		ax.bar3d(self.section["CG"]["xpos"], self.section["CG"]["ypos"], self.zpos, self.dx, self.dy, self.section["CG"]["values"], color=self.section["CG"]["col"], zsort='max')
		ax.bar3d(self.section["AT"]["xpos"], self.section["AT"]["ypos"], self.zpos, self.dx, self.dy, self.section["AT"]["values"], color=self.section["AT"]["col"], zsort='max')
		ax.bar3d(self.section["AC"]["xpos"], self.section["AC"]["ypos"], self.zpos, self.dx, self.dy, self.section["AC"]["values"], color=self.section["AC"]["col"], zsort='max')
		ax.bar3d(self.section["AG"]["xpos"], self.section["AG"]["ypos"], self.zpos, self.dx, self.dy, self.section["AG"]["values"], color=self.section["AG"]["col"], zsort='max')
		#ax.axis('off')
		ax.view_init(azim=63)
		plt.savefig(outFile)
		
	def fill(self,hashV,rev=False):
		#### fill the lego values from a hashTable
		#### hash key expected: xz_AB where it correspond to a mutation xAz -> xBz
		#### mutation G-> [ACT] will be complemented :  C-> [TGA] etc... 
		#### because only C -> [ATG] and A -> [CTG] mutation are considered
		k=hashV.keys()
		for i in k:
			try:
				ct,mu=i.split("_")
				if self.section.has_key(mu):
					self.section[mu]["values"][self.mapping.index(ct)]=self.section[mu]["values"][self.mapping.index(ct)]+hashV[i]
				else :
					muC=str(Seq(mu).complement())
					if rev :
						ctC=str(Seq(ct).reverse_complement())
					else:
						ctC=ct
					if self.section.has_key(muC):
						self.section[muC]["values"][self.mapping.index(ctC)]=self.section[muC]["values"][self.mapping.index(ctC)]+hashV[i]
					else: 
						print "unrecognized DNA code in muation : "+i +" ... not considered"
						continue
			except ValueError:
				print "unrecognized muation : "+i +" ... not considered"
				continue
			print "incorporating the "+ ct[0] +mu[0] +ct[1] +" -> " + ct[0] +mu[1] +ct[1] + " mutation value in the plot"
			

def getarg(argument):
	muF=""
	revC=False
	outB=""
	argCount=0
	optli,arg = getopt.getopt(argument[1:],"r:o:t:h",['revcomp','output','table','help'])
	for option, value in optli:
		if option in ("-r","--revcomp"):
			try :
				revC=bool(value)
			except ValueError:
				sys.exit("Error - none boolean revcomp value:\n"+revC)
		if option in ("-o","--output"):
			outB=str(value)
			argCount=argCount+1
		if option in ("-t","--table"):
                        muF=str(value)
                        argCount=argCount+1
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if argCount < 2 :
		usage()
		sys.exit("#######\nError : Missing argument(s)")
	if not os.path.exists(muF) :
		usage()
		sys.exit("#######\nError - mutation table file not found:\n"+muF)
	if not os.path.lexists(os.path.dirname(outB)):
		usage()
		sys.exit("#######\nError - output directory not found:\n"+os.path.dirname(outB))
	return  muF, revC, outB



def usage():
	print "USAGE : legoPlot.py [option] "
	print "       -t :        string - mutation table file"
	print "       -r :        boolean (1: True; 0: False) - reverse complement "
	print "                   the context when G or T bref base are mutated (default False)"
	print "       -o :        string - output basename"
	print "       -h :        this help \n"



def main():
	print "\n--------------------------------------------------------------------------------------------"
	print "legoPlot.py will generate a lego plot for each set a value (line) in the mutation table file"
	print "The header of the mutation table file should should be name + mutation description:  "
	print "xz_AB where it corresponds to a mutation xAz -> xBz"
	print "----"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "---------------------------------------------------------------------------------------------\n"
	muF, revC, outB = getarg(sys.argv)
	f=open(muF,'r')
	l=f.readline()
	header=l.split()
	try:
		namePos=header.index("name")
		mutList=header
		del(mutList[namePos])
	except ValueError:
		sys.exit("#######\nError - the 'name' field not found in the mutation table header\n")
	indMutVal={}
	l=f.readline()
	while l != "" :
		c=l.split()
		nameI=c[namePos]
		del(c[namePos])
		if not indMutVal.has_key(nameI):
			#### create individual legoplot
			for i, x in enumerate(c):
				c[i] = float(x) 
			indMutVal[nameI]=legoPlot()
			indMutVal[nameI].fill(dict(zip(mutList,c)),revC)
			indMutVal[nameI].plot(outB+"_"+nameI+".png")
		else :
			sys.exit("#######\nError - multiple identical entry name:\n"+nameI)
		l=f.readline()
	f.close()

main()

