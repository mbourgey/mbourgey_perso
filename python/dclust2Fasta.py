#!/usr/bin/env python

### Mathieu Bourgey (2015/04/28) - mbourgey@genomequebec.com
### Dclust2Fasta


import os
import sys
import string
import getopt
import re
import operator

class clustSeq :
        def __init__(self) :
                self.ID=""
                self.barcodes={}
                self.size=0
                self.seq=""
        
        def addSeq(self,info):
                val=info.split(";")
                for i in range(1,len(val)-1,1):
                        bc=val[i].split("=")
                        if self.barcodes.has_key(bc[0]):
                                self.barcodes[bc[0]] += int(bc[1])
                        else :
                                self.barcodes[bc[0]] = int(bc[1])
                        self.size += int(bc[1])
        
        def writer(self,out):
                outItems=[self.ID]+["%s=%s" % (k, v) for k, v in self.barcodes.items()]+["size="+str(self.size)]
                out.write(";".join(outItems) + "\n")
                out.write(self.seq + "\n")



a=open("barcodes_302/assembled/obs/derep1_099_derep2_nonChimDenovoRef_sorted_097.dc",'r')
l=a.readline()
clust={}
unsortedDict={}
while l != "":
        c=l.split()
        start=c[0].split(";")
        clust[">"+start[0]]=clustSeq()
        clust[">"+start[0]].ID=">"+start[0]
        for i in range(0,len(c),1) :
                clust[">"+start[0]].addSeq(c[i])
        unsortedDict[">"+start[0]]=clust[">"+start[0]].size
        l=a.readline()
sortedDict=sorted(unsortedDict.items(), key=operator.itemgetter(1),reverse=True)
a.close()
a=open("barcodes_302/assembled/obs/derep1_099_derep2_nonChimDeNovoRef_sorted.fasta",'r')
l=a.readline()
ct=0
oldId=""
oldSeq=""
while l != "":
        if l[0] == ">" :
                if ct > 0 and clust.has_key(oldId) :
                        clust[oldId].seq=oldSeq
                c=l.split(";")
                oldId=c[0]
                ct += 1
        else :
                c=l.split()
                oldSeq = c[0]
        l=a.readline()
        
a.close()
out=open("barcodes_302/assembled/obs/derep1_099_derep2_nonChimDeNovoRef_sorted_097.fasta",'w')
for i in range(0,len(sortedDict),1):
        clust[sortedDict[i][0]].writer(out)
out.close()
