#!/usr/bin/env python

### Mathieu Bourgey (2014/03/21)
### extract sequence based on a given coordinate

import os
import sys
import string
import getopt
import re
import itertools
from Bio.Alphabet import IUPAC
from Bio import SeqIO
from Bio.Seq import Seq

class transcript :  ## general transcript class that combine all information
	def __init__(self) :
		self.chro="" ## fill in tmpExon ini then addExon object method
		self.start=[]  ## fill in tmpExon ini then addExon object method
		self.end=[]
		self.name=""

def getarg(argument):
	inF=""
	outF=""
	gtfF=""
	optli,arg = getopt.getopt(argument[1:],"f:o:g:t:h",['fasta','out','gtf','type','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-f","--fasta"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-g","--gtf"):
			gtfF=str(value)
		if option in ("-t","--type"):
                        typ=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - fasta file not found:\n"+inF)
	if not os.path.exists(gtfF) :
                sys.exit("Error - gtf file not found:\n"+gtfF)
	return  inF, outF, gtfF, typ



def usage():
	print "USAGE : gtf2fasta.py [option] "
	print "       -f :        fasta file"
	print "       -o :        output file"
	print "       -g :        gtf file"
	print "       -t :        gtf entry type to use" 
	print "       -h :        this help \n"


def importGtfFeatures(g, t):
	f=open(g,'r')
	l=f.readline()
	DG={}
	cf=0
	cc=0
	ct=0
	while l != "" :
		if  l[0] != "#":
			c=l.split()
			if c[2] == t:
				if not DG.has_key(c[0]) :
					DG[c[0]]={}
					cc+=1
				trName=c[c.index("transcript_id")+1].split("\"")[1]
				chro=c[0]
				st=c[3]
				end=c[4]
				if not DG[c[0]].has_key(trName) :
					DG[c[0]][trName]=transcript()
					ct+=1
				DG[c[0]][trName].chro=chro
				DG[c[0]][trName].start.append(int(st))  ## fill in tmpExon ini then addExon object method
				DG[c[0]][trName].end.append(int(end))
				DG[c[0]][trName].name=trName
				cf+=1
		l=f.readline()
	print str(cf) + " " + t + " features found"
	print "on " + str(ct) + " transcripts"
	print "located on " + str(cc) + " chromosomes"
	f.close()
	return DG




def main():
	print "\n------------------------------------------------------------"
	print "gtf2fasta.py will generate fasta file containing all the desired entries "
	print "of each gene while concataining them" 
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, gtfF, typ = getarg(sys.argv)
	print "Importing gtf regions..."
	dictGenome = importGtfFeatures(gtfF, typ)
	print "Done"
	o=open(outF,'w')
	for seq_record in SeqIO.parse(inF, "fasta"):
		if dictGenome.has_key(seq_record.id) :
			transList=dictGenome[seq_record.id].keys()
			for i in transList :
				o.write(">"+str(i)+"\n")
				concatenated = ""
				for j in range(0,len(dictGenome[seq_record.id][i].start),1):
					concatenated += str(seq_record.seq)[int(dictGenome[seq_record.id][i].start[j]-1):int(dictGenome[seq_record.id][i].end[j])]
				o.write(concatenated+"\n")
	o.close()


	
main()
