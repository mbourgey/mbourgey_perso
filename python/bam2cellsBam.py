#!/usr/bin/env python

## bam2cellsBam.py - extract the single cell reads for each cell in a separate bam file 
### Mathieu Bourgey (2021/02/19) - mathieu.bourgey@mcgill.ca


import os
import sys
import string
import getopt
import re
import pysam
import gzip

## extract 4 parameters (orginial bam - mandatory; good cells list - mandatory; output folder path -optional; corrected barcode bam tag)
def getarg(argument):
        bam_file=""
        cell_list=""
        output_folder="bam2cell"
        bc_tag="CB"
        optli,arg = getopt.getopt(argument[1:],"b:c:o:t:h",['bam','cells','output','tag','help'])
        if len(optli) == 0 :
                usage()
                sys.exit("Error : No argument given")
        for option, value in optli:
                if option in ("-b","--bam"):
                        bam_file=str(value)
                if option in ("-c","--cells"):
                        cell_list=str(value)
                if option in ("-o","--output"):
                        output_folder=str(value)
                if option in ("-t","--tag"):
                        bc_tag=str(value)
                if option in ("-h","--help"):
                        usage()
                        sys.exit()
        if not os.path.exists(bam_file) :
                sys.exit("Error - Bam file not found:\n"+str(bam_file))
        if not os.path.exists(cell_list) :
                sys.exit("Error - Cells list file not found:\n"+str(cell_list))
        if not os.path.exists(output_folder) :
                os.mkdir(output_folder)
                print("output folder missing - Creating directory:\n"+str(output_folder))
        return bam_file, cell_list, output_folder, bc_tag
        
def usage():
        print "USAGE : bam2cellsBam.py [option] "
        print "       -b :        all-cell bam file"
        print "       -c :        good cell list file"
        print "       -o :        output folder where to create the per-cell bam files (default bam2cell)"
        print "       -t :        corrected barcode tag (default 10XGenomics tag: CB)"
        print "       -h :        this help \n"



def main():
        print "\n---------------------------------------------------------------------------------"
        print "bam2cellsBam takes into imput a scRNA bam file and list of valid barcodes and output"
        print "and output generate individual cell bam files containing the corresponding reads"
        print "This program was written by Mathieu Bourgey"
        print "For more information, contact: mathieu.bourgey@mcgill.ca"
        print "----------------------------------------------------------------------------------\n"
        bf, cl, of, bt = getarg(sys.argv)
        print "Opening : " + bf
        bamfile=pysam.AlignmentFile(bf, "rb")
        ##extract valid barcode and open corresponding bam files
        print "Extracting barcodes from : " + cl
        outbam={}
        if cl.endswith(".gz") :
            f=gzip.open(cl,'rb')
        else :
            f=open(cl,'r')
        l=f.readline()
        while l != "":
            c=l.split()
            outbam[c[0]]=pysam.AlignmentFile(of +"/"+c[0]+".bam","wb", template=bamfile)
            l=f.readline()
        f.close()
        print "Extracting barcodes ... Done" 
        ##get over the input bam and write the read if the barcode is in the list
        print "Spliting the reads in cells bam files loacted : " + of
        print "Based on the tag : " + bt
        for x in bamfile:
            if x.has_tag(bt) :
                cbin=x.get_tag(bt)
                if outbam.has_key(cbin) :
                    outbam[cbin].write(x)
        print "Spliting ... Done"
        ### close files
        print "Closing files"
        bamfile.close()
        for i in  outbam.values():
            i.close()
        print "... Done"
        
main()
