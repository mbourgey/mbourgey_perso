#!/usr/bin/env python

## SNParrayVcf2Clean.py take into input a SNP array vcf file and a clean csv list of position 
### Mathieu Bourgey (2015/12/23) - mathieu.bourgey@mcgill.ca


import os
import sys
import string
import getopt
import re
from itertools import chain



def getarg(argument):
        vcf_file=""
        csv_file=""
        out_file=""
        optli,arg = getopt.getopt(argument[1:],"v:c:o:h",['vcf','csv','out','help'])
        if len(optli) == 0 :
                usage()
                sys.exit("Error : No argument given")
        for option, value in optli:
                if option in ("-v","--vcf"):
                        vcf_file=str(value)
                if option in ("-c","--csv"):
                        csv_file=str(value)
                if option in ("-o","--out"):
                        out_file=str(value)
                if option in ("-h","--help"):
                        usage()
                        sys.exit()
        if not os.path.exists(vcf_file) :
                sys.exit("Error - vcf file not found:\n"+str(vcf_file))
        if not os.path.exists(csv_file) :
                sys.exit("Error - csv file not found:\n"+str(csv_file))
        return vcf_file, csv_file, out_file
        
def usage():
        print "USAGE : extractMate.py [option] "
        print "       -v :        vcf snpArray filefile"
        print "       -c :        csv clean position file"
        print "       -o :        output vcf file"
        print "       -h :        this help \n"

def parse_csv(csv):
    d={}
    f=open(csv,'r')
    l=f.readline()
    while l != "" :
        c=l.split(",")
        d[c[2]]=[c[0],c[1],c[3]]
        l=f.readline()
    return d

def complement():
    d={}
    d["A"]="T"
    d["C"]="G"
    d["G"]="C"
    d["T"]="A"
    d["a"]="t"
    d["c"]="g"
    d["g"]="c"
    d["t"]="a"
    return d

def main():
        print "\n---------------------------------------------------------------------------------"
        print "SNParrayVcf2Clean.py take into input a SNP array vcf file and a clean csv list of position"
        print "and output a vcf file with only clean postion"
        print "This program was written by Mathieu Bourgey"
        print "For more information, contact: mathieu.bourgey@mcgill.ca"
        print "----------------------------------------------------------------------------------\n"
        vf, cf, of = getarg(sys.argv)
        clean_pos=parse_csv(cf)
        f=open(vf,'r')
        o=open(of,'w')
        l=f.readline()
        nc=0
        nf=0
        comP=complement()
        while l != "" :
            if l[0] == "#" :
                o.write(l)
            else :
                c=l.split()
                if c[9] != "./.":
                    try :
                        if clean_pos[c[2]][2] == c[3] or clean_pos[c[2]][2] == c[4] :
                            o.write(clean_pos[c[2]][0]+"\t"+clean_pos[c[2]][1]+"\t"+c[2]+"\t"+c[3]+"\t"+c[4]+"\t"+c[5]+"\t"+c[6]+"\t"+c[7]+"\t"+c[8]+"\t"+c[9]+"\n")
                        else:
                            #print l
                            #print clean_pos[c[2]]
                            nc=nc+1
                    except:
                        nf=nf+1
            l=f.readline()
        f.close()
        o.close()
        print "number of probe not found: " + str(nf)
        print "number of probe not clean: " + str(nc)
        
main()
