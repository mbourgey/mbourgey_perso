#!/usr/bin/env python

## random select a given number of line in a vcf 
### Mathieu Bourgey (2016/01/15) - mathieu.bourgey@mcgill.ca


import os
import sys
import string
import getopt
import re
import random
from itertools import chain



def getarg(argument):
        vcf_file=""
        out_file=""
        li_num=200000
        optli,arg = getopt.getopt(argument[1:],"v:l:o:h",['vcf','line','out','help'])
        if len(optli) == 0 :
                usage()
                sys.exit("Error : No argument given")
        for option, value in optli:
                if option in ("-v","--vcf"):
                        vcf_file=str(value)
                if option in ("-l","--line"):
                        li_num=int(value)
                if option in ("-o","--out"):
                        out_file=str(value)
                if option in ("-h","--help"):
                        usage()
                        sys.exit()
        if not os.path.exists(vcf_file) :
                sys.exit("Error - vcf file not found:\n"+str(vcf_file))
        return vcf_file, li_num, out_file
        
def usage():
        print "USAGE : sampleVcf.py [option] "
        print "       -v :        vcf snpArray filefile"
        print "       -l :        csv clean position file"
        print "       -o :        output vcf file"
        print "       -h :        this help \n"


def main():
        print "\n---------------------------------------------------------------------------------"
        print "sampleVcf.py take into input a vcf file and retourn a subset of given randomly selected line"
        print "This program was written by Mathieu Bourgey"
        print "For more information, contact: mathieu.bourgey@mcgill.ca"
        print "----------------------------------------------------------------------------------\n"
        vf, ln, of = getarg(sys.argv)
        f=open(vf,'r')
        ct=0
        l=f.readline()
        print "reading vcf file pass 1"
        while l != "" :
            if l[0] != "#" :
                ct=ct+1
            l=f.readline()
        f.close()
        if ct < ln :
            print "the number of line to select (" +str(ct)+") is higher than the number of line in the input file ("+str(ln)+")"
            sys.exit()
        print "compute subsample"
        random.seed(12345678910)
        rlines=random.sample(range(0,ct),ln)
        rlines.sort()
        print "reading vcf file pass 2 and generate ouputs"
        f=open(vf,'r')
        ct=0
        l=f.readline()
        o=open(of,'w')
        while l != "" :
            if len(rlines) > 0 :
                if l[0] == "#" :
                    o.write(l)
                else :
                    if ct == rlines[0]:
                        o.write(l)
                        del(rlines[0])
                    ct=ct+1
                l=f.readline()
            else :
                break
        f.close()
        o.close()
        print "Done"
        
main()
