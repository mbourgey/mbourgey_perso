import os
import sys
import string
import getopt
import re
import pysam

samfile = pysam.AlignmentFile(os.path.join("13S-YCD","HI.2340.005.Index_16.13S-YCD.bam"),'rb')

o=open(os.path.join("13S-YCD","HI.2340.005.Index_16.13S-YCD.rrna.stats.tsv"),'w')
ct={}
ct["total"]=0
ct["rRNA_mapped"]=0
for x in samfile:
 ct["total"]+=1
 if not x.is_unmapped :
  ct["rRNA_mapped"]+=1
  if not ct.has_key(samfile.getrname(x.reference_id)) :
   ct[samfile.getrname(x.reference_id)]=0
  ct[samfile.getrname(x.reference_id)]+=1

rname=["Mapped","rRNA_mapped"]
rcount=[str(ct["total"]),str(ct["rRNA_mapped"])]
for r in samfile.references:
 rname.append(str(r))
 if ct.has_key(r) :
  rcount.append(str(ct[r]))
 else:
  rcount.append("0")

o.write("\t".join(rname)+"\n")
o.write("\t".join(rcount)+"\n")
o.close()
samfile.close()

