#!/usr/bin/python

### Mathieu Bourgey (2011/03/21)
### extract sequence based on a given list

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO
import gzip



def getarg(argument):
	rlist=""
	fastq1=""
	fastq2="."
	gz=0
	optli,arg = getopt.getopt(argument[1:],"i:m:l:g:h",['input','mate','list','gzip','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			fastq1=str(value)
		if option in ("-m","--mate"):
			fastq2=str(value)
		if option in ("-l","--list"):
			rlist=str(value)
		if option in ("-g","--gzip"):
			gz=int(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(rlist) :
		sys.exit("Error - read name file not found:\n"+rlist)
	if not os.path.exists(fastq1) :
		sys.exit("Error - fastq file not found:\n"+fastq1)
	if fastq2 != ".":
		if not os.path.exists(fastq2) :
			sys.exit("Error - fastq file not found:\n"+fastq2)
	return  rlist, gz, fastq1, fastq2



def usage():
	print "USAGE : fastqPickUp.py [option] "
	print "       -i :        fastq input file"
	print "       -m :        mate fastq file (optional)"
	print "       -l :        read list file"
	print "       -g :        gziped input/output yes: 0 ; no: 1 (Default 0)"
	print "       -h :        this help \n"


def main():
	print "\n------------------------------------------------------------"
	print "fastqPickUp.py will extract the reads in a fastq file using a predefined list"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	rlist, gz, fastq1, fastq2 = getarg(sys.argv)
	if gz == 0 :
		seq_records= SeqIO.to_dict(SeqIO.parse(gzip.open(fastq1, 'rb'),'fastq'))
		o=gzip.open(re.sub('fastq.gz', 'FPU_fastq.gz',fastq1),'wb')
		if fastq2 != ".":
			seq_records2= SeqIO.to_dict(SeqIO.parse(gzip.open(fastq2, 'rb'),'fastq'))
			o2=gzip.open(re.sub('fastq.gz', 'FPU_fastq.gz',fastq2),'wb')
	else :
		seq_records=SeqIO.to_dict(SeqIO.parse(fastq1,'fastq'))
		o=open(re.sub('fastq', 'FPU_fastq',fastq1),'w')
		if fastq2 != ".":
			seq_records2=SeqIO.to_dict(SeqIO.parse(fastq2,'fastq'))
			o2=open(re.sub('fastq', 'FPU_fastq',fastq2),'w')
	print "Fastq loaded \n"
	f=open(rlist,'r')
	l=f.readline()
	if fastq2 != ".":
		while l != "" :
			c=l.split()
			if c[0] in seq_records :
				SeqIO.write(seq_records[c[0]],o,"fastq")
				del seq_records[c[0]]
				SeqIO.write(seq_records2[c[0]],o2,"fastq")
				del seq_records2[c[0]]
			l=f.readline()
		f.close()
		o.close()
		o2.close()
	else :
		while l != "" :
			c=l.split()
			if c[0] in seq_records :
				SeqIO.write(seq_records[c[0]],o,"fastq")
				del seq_records[c[0]]
			l=f.readline()
		f.close()
		o.close()
	
main()

	    
	
	





