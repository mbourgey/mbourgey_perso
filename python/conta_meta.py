import csv
import re
regex=re.compile('\d*,\d*,\d*')

a=open("contamination.csv",'rb')
b=csv.reader(a)
for l in b :
    if l[0] == "Project_approx" :
        headerC=l
    else :
        mps=l[2].split("-")
        if d.has_key(l[1]) :
            d[l[1]][l[3]+"_"+l[4]]={}
        else :
            d[l[1]]={}
            d[l[1]][l[3]+"_"+l[4]]={}
        d[l[1]][l[3]+"_"+l[4]]["projet"]=l[0]
        d[l[1]][l[3]+"_"+l[4]]["run"]=l[3]
        d[l[1]][l[3]+"_"+l[4]]["lane"]=l[4]
        d[l[1]][l[3]+"_"+l[4]]["library"]=mps[0]
        if len(mps) > 1 :
            d[l[1]][l[3]+"_"+l[4]]["hole"]=mps[1]
        else: 
            d[l[1]][l[3]+"_"+l[4]]["hole"]=''
        d[l[1]][l[3]+"_"+l[4]]["contaLane"]=l
        d[l[1]][l[3]+"_"+l[4]]['librayrLane']=['']*37
        d[l[1]][l[3]+"_"+l[4]]['readsetLane']=['']*48
        d[l[1]][l[3]+"_"+l[4]]['SampleLane']=['']*26
        d[l[1]][l[3]+"_"+l[4]]["readsNumTot"]='0'
        d[l[1]][l[3]+"_"+l[4]]["readsNum"]='0'
        


a.close()
a=open("readsetsHiseq.approved.csv",'rb')
b=csv.reader(a)
for l in b :
    if l[0] == "Name" :
        headerR=l
    else :
        if d.has_key(l[1]) :
            if d[l[1]].has_key(l[24]+"_"+l[26]) :
                d[l[1]][l[24]+"_"+l[26]]["readsNum"]=l[34]
                d[l[1]][l[24]+"_"+l[26]]["readsetLane"]=l
            elif d[l[1]].has_key("_") :
                d[l[1]]["_"]["readsNum"]=l[34]
                d[l[1]]["_"]["readsetLane"]=l
            else :
                for key in d[l[1]]:
                    if not d[l[1]][key].has_key("readsNum") :
                        d[l[1]][key]["readsNum"]=l[34]
                        d[l[1]][key]["readsetLane"]=l

a.close()

for k in d.keys():
    if len(d[k]) > 1 :
        redtot={}
        for k2 in d[k] :
            if redtot.has_key(d[k][k2]["projet"]):
                redtot[d[k][k2]["projet"]]+=int("".join(d[k][k2]['readsNum'].split(",")))
            else:
                 redtot[d[k][k2]["projet"]]=int("".join(d[k][k2]['readsNum'].split(",")))
        for k2 in d[k] :
            d[k][k2]["readsNumTot"]=redtot[d[k][k2]["projet"]]

a=open("libraries.approved.csv",'rb')
b=csv.reader(a)
for l in b :
    if l[0] == "Project" :
        headerL=l
    else :
        if d.has_key(l[1]) :
            if len(d[l[1]]) > 1 :
                for key in d[l[1]]:
                    rn=[m.group(0) for i in l for m in [regex.search(i)] if m]
                    if len(rn) >= 1 :
                        if (d[l[1]][key]["readsNum"] == rn[-1]) or (d[l[1]][key]["readsNumTot"] == int("".join(rn[-1].split(",")))):
                            d[l[1]][key]["librayrLane"]=l
            else :
                d[l[1]][d[l[1]].keys()[0]]["librayrLane"]=l

a.close()

a=open("samples.approved.csv",'rb')
b=csv.reader(a)
for l in b :
    if l[0] == "Project" :
        headerS=l
    else :
        if d.has_key(l[1]) :
            if len(d[l[1]]) > 1 :
                for key in d[l[1]]:
                    rn=[m.group(0) for i in l for m in [regex.search(i)] if m]
                    if len(rn) >= 1 :
                        if (d[l[1]][key]["readsNum"] == rn[-1]) or (d[l[1]][key]["readsNumTot"] == int("".join(rn[-1].split(",")))):
                            d[l[1]][key]["SampleLane"]=l
            else :
                d[l[1]][d[l[1]].keys()[0]]["SampleLane"]=l

a.close()

o=open("contamination_meta.csv",'wb')
out=csv.writer(o)
out.writerow(headerC+headerR+headerL+headerS)
for key in d:
    for key2 in d[key] :
        out.writerow(d[key][key2]["contaLane"]+d[key][key2]["readsetLane"]+d[key][key2]["librayrLane"]+d[key][key2]["SampleLane"])


o.close()

