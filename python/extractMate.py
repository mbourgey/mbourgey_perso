#!/usr/bin/env python

## extractMate take into imput a list of fastq and extract the corresponding mate 
### Mathieu Bourgey (2015/12/08) - mathieu.bourgey@mcgill.ca


import os
import sys
import string
import getopt
import re


def getarg(argument):
        task_file=""
        optli,arg = getopt.getopt(argument[1:],"t:h",['task','help'])
        if len(optli) == 0 :
                usage()
                sys.exit("Error : No argument given")
        for option, value in optli:
                if option in ("-t","--task"):
                        task_file=str(value)
                if option in ("-h","--help"):
                        usage()
                        sys.exit()
        if not os.path.exists(task_file) :
                sys.exit("Error - Task file not found:\n"+str(task_file))
        return task_file
        
def usage():
        print "USAGE : extractMate.py [option] "
        print "       -t :        task file"
        print "       -h :        this help \n"


def main():
        print "\n---------------------------------------------------------------------------------"
        print "extractMate take into imput a list of fastq and extract the corresponding mateaddLength.py"
        print "This program was written by Mathieu Bourgey"
        print "For more information, contact: mathieu.bourgey@mcgill.ca"
        print "----------------------------------------------------------------------------------\n"
        tf = getarg(sys.argv)
        
        
main()
