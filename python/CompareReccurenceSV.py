#!/usr/bin/python

### Mathieu Bourgey (2013/07/31) - mbourgey@genomequebec.com
### get by gene reccurence of SV events


import os
import sys
import string
import getopt
import re

class genSVcall :
	def __init__(self) :
		self.dup=[]
		self.dele=[]
		self.inv=[]
		self.tra=[]
		self.dupS=[]
		self.deleS=[]
		self.invS=[]
		self.traS=[]
		self.dupG=[]
		self.deleG=[]
		self.invG=[]
		self.traG=[]
		self.dupA=[]
		self.deleA=[]
		self.invA=[]
		self.traA=[]
		

def getarg(argument):
	f=""
	g=""
	s="S"
	out=""
	optli,arg = getopt.getopt(argument[1:],"f:o:g:s:h",['file','output','gene','sample','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			f=str(value)
		#if option in ("-g","--gene"):
			#g=str(value)
#		if option in ("-t","--type"):
#			t=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(f) :
		sys.exit("Error - file not found:\n"+str(f))
	#if os.path.exists(g):
		#return f, g, out
	#else : 
	return f, out

def usage():
	print "\n---------------------------------------------------------------------------------"
	print "CompareRecurenceSV.py provided detailed recurence list based on SV filtered calls"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : CompareRecurenceSV.py [option] "
	print "       -f :        merged SV reccurence calls file "
##	print "       -g :        optional: candidate gene file"
##	print "       -t :        SV onco type A: all S: somatic (default S)"
	print "       -o :        output basename"
	print "       -h :        this help \n"


     
        
def main():
##	f1, ge, sa, out = getarg(sys.argv)
	f1 , outP =  getarg(sys.argv)
	fil=open(f1,'r')
	outF = open(outP,'w')
	l1=fil.readline()
	l2=fil.readline()
	while l1 != "" :
		if l2 == "":
			c1=l1.split(",")
			outTmp=[]
			outTmp.extend(c1[0:6])
			outTmp.extend(c1[10:15])
			outTmp.extend(c1[19:24])
			outTmp.extend(c1[28:33])
			outTmp.extend(c1[37:38])
			outF.write(",".join(outTmp))
			l1=l2
		else :
			c1=l1.split(",")
			c2=l2.split(",")
			if (c1[0] != c2[0]) :
				outTmp=[]
				outTmp.extend(c1[0:6])
				outTmp.extend(c1[10:15])
				outTmp.extend(c1[19:24])
				outTmp.extend(c1[28:33])
				outTmp.extend(c1[37:38])
				out=",".join(outTmp)
				outF.write(out)
				l1=l2
				l2=fil.readline()
			else :
				col1=[[6,7,8,9],[15,16,17,18],[24,25,26,27],[33,34,35,36]]
				out=[c1[0]]
				for k in col1:
					allS1=[]
					allS2=[]
					col = k
					for i in col :
						if c1[i] != "."  and c2[i] != "." :
							lg1=c1[i].split(":")
							lg2=c2[i].split(":")
							allS1.extend(lg1)
							allS2.extend(lg2)
							share=0
							for j in lg1:
								if j in lg2 :
									share=share+1
							lg1.extend(lg2)
							lgBoth=list(set(lg1))
							out.append(str(len(lgBoth))+"("+str(share)+")")
						elif c1[i] != "." :
							lg1=c1[i].split(":")
							allS1.extend(lg1)
							out.append(str(len(lg1))+"(0)")
						elif c2[i] != "." :
							lg2=c2[i].split(":")
							out.append(str(len(lg2))+"(0)")
							allS2.extend(lg2)
						else :
							out.append(".")
					if len(allS1) > 0 and len(allS2) > 0 :
						share=0
						lg1=list(set(allS1))
						lg2=list(set(allS2))
						for j in lg1:
							if j in lg2 :
								share=share+1
						lg1.extend(lg2)
						lgBoth=list(set(lg1))
						out.append(str(len(lgBoth))+"("+str(share)+")")
					elif len(allS1) > 0 :
						lg1=list(set(allS1))
						out.append(str(len(lg1))+"(0)")
					elif len(allS2) > 0 :
						lg2=list(set(allS2))
						out.append(str(len(lg2))+"(0)")
					else :
						out.append(".")
				out.append("BRD_PI")
				outF.write(",".join(out)+"\n")
				l1=fil.readline()
				l2=fil.readline()
        fil.close()
	outF.close()

main()

