#!/usr/bin/env python

### Mathieu Bourgey (2018/08/09) 
### extract sequence from sam based on a whitelist of barcode
### for more information plese contact: mathieu.bourgey@mcgill.ca


import sys
from sys import stdout

##get hash table of BC
n="WL_barcode.txt"
f=open(n,'r')
l=f.readline()
cancer_dict={}
while l != "" :
  c=l.split()
  cancer_dict[c[0]]=""
  l=f.readline()
f.close()


### extract reads
print
b="possorted_bam.sam"
e="possorted_bam_filtered.sam"
f=open(b,'r')
oT=open(e,'w')
l=f.readline()
Tc=0
while l != "" :
    sys.stdout.write("\r............... Extracted reads: " + str(Tc) )
    c=l.split()
    if c[0][0] == "@" and len(c[0]) == 3 :
        ## Sam header
        oT.write(l)
    else :
        ## Sam body
        for i in range(0,len(c)):
            if c[i][0:4] == "CB:Z" :
                j=c[i].split(":")
                if cancer_dict.has_key(j[2]) :
                    ##barcode found
                    oT.write(l)
                    break
    l=f.readline()
    sys.stdout.flush()
f.close()
oT.close()


print "Filtered sam created\n"


