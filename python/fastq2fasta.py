#!/usr/bin/python

### Mathieu Bourgey (2011/03/21)
### simulate paire-end sequencing

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO



def getarg(argument):
	dirF="fasta"
	optli,arg = getopt.getopt(argument[1:],"i:o:d:h",['input','out','direction','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			out=str(value)   
		if option in ("-d","--direction"):
			if (str(value) == "fastq" or str(value) == "fasta"):
				dirF=str(value)   
			else :
				print "unknow direction: format from fastq to fasta"
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - fastq file not found:\n"+inF)
	return  inF, out, dirF



def usage():
	print "USAGE : fastq2fasta.py [option] "
	print "       -i :        input fastq file"
	print "       -o :        output fasta file"
	print "       -d :        formating direction:" 
	print "                      fasta: form fastq to fasta (default)"
	print "                      fastq: form fasta to fastq"
	print "       -h :        this help \n"


def main():
	print "\n------------------------------------------------------------"
	print "fastq2fasta.py will generate fasta files from a fastq file "
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, dirF = getarg(sys.argv)
	if dirF == "fasta" :
		SeqIO.write(SeqIO.parse(inF, "fastq"), outF, "fasta")
	elif dirF == "fastq":
		print "Warning adding fake phred quality of 40"
		records=[]
		for  record in SeqIO.parse(inF, "fasta"): 
			record.letter_annotations["phred_quality"]=[40] * len(record.seq)
			records.append(record)
		SeqIO.write(records, outF, "fastq")
			
	
	
main()

	    
	
	





