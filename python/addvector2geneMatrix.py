#!/usr/bin/env python

################################################################################
# Copyright (C) Mathieu Bourgey
# this script add CRISPR vector into the 10x matrix.mtx
################################################################################



g=open("genes.tsv",'r')
l=g.readline()
gene0=[]
gene1=[]

while l != "" :
    c=l.split()
    gene0.append(c[0])
    gene1.append(c[1])
    l=g.readline()

g.close()

b=open("barcodes.tsv",'r')
l=b.readline()
bc={}
bcr={}
ct=0
while l != "" :
    ct+=1
    c=l.split()
    bc[c[0]]=ct
    bcr[ct]=c[0]
    l=b.readline()

b.close()

v=open("../../../MB_analysis/BFP_Genotypes_unique.tsv",'r')
l=v.readline()
c=l.split()
ctg=len(gene0)
ctv={}
rep=[]
  
for i in range(1,len(c)):
    gene0.append("BFP_"+c[i])
    gene1.append("BFP_"+c[i])
    ctv["BFP_"+c[i]]=ctg+i
    rep.append(0)



l=v.readline()
bcvect={}
toL=0
while l != "" :
    vectval={}
    c=l.split()
    for i in range(1,len(c)):
        if int(c[i]) > 0 : 
            vectval[ctg+i]=c[i]
            toL+=1
            rep[i-1]=1
    bcvect[c[0]]=vectval
    l=v.readline()

v.close()

matout=open("matrix_new.mtx",'w')
m=open("matrix.mtx",'r')
l=m.readline()
matout.write(l)
l=m.readline()
matout.write(l)
l=m.readline()
c=l.split()
matout.write(str(int(c[0])+sum(rep))+" "+c[1]+" "+str(int(c[2])+toL)+"\n")
l=m.readline()
while l != "" :
    matout.write(l)
    c=l.split()
    old=c[1]
    l=m.readline()
    c=l.split()
    if bcr.has_key(int(old)) :
        if bcvect.has_key(bcr[int(old)]):
            if l == "" :
                k=bcvect[bcr[int(old)]].keys()
                k.sort()
                for i in k:
                    matout.write(str(i)+" "+old+" "+str(bcvect[bcr[int(old)]][i])+"\n")
            elif c[1] != old:
                k=bcvect[bcr[int(old)]].keys()
                k.sort()
                for i in k:
                    matout.write(str(i)+" "+old+" "+str(bcvect[bcr[int(old)]][i])+"\n")

    
matout.close()
m.close()

genout=open("genes_new.tsv",'w')
for i in range(0,len(gene0)):
    genout.write(gene0[i]+"\t"+gene1[i]+"\n")

genout.close()

