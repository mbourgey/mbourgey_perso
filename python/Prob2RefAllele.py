#!/usr/bin/python

### Mathieu Bourgey (2015/05/21) 
### Mathieu Bourgey update 2015/09/11
### extract reference allele from cleaned probs of chip manifest

import os
import sys
import string
import getopt
import re
import itertools
import threading
from Bio import SeqIO

#init threadlocking systeme
threadLock = threading.Lock()
threads = []


class myThread (threading.Thread):
    def __init__(self, threadID, name, probeSet, refSet, outTmp):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.ps = probeSet
        self.rs = refSet
        self.ot = outTmp
    def run(self):
        print "Starting " + self.name
        # Get lock to synchronize threads
        threadLock.acquire()
        probe_vs_reference(self.ps, self.rs, self.ot)
        # Free lock to release next thread
        threadLock.release()




def getarg(argument):
	thr=1
	optli,arg = getopt.getopt(argument[1:],"i:o:r:h",['input','out','ref','thread','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-r","--ref"):
			refF=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, refF



def usage():
	print "USAGE : Prob2RefAllele.py [option] "
	print "       -i :        input pseudo bed file of probs"
	print "                   PROBE_NAME\tCHROM\tPOSITION\tPROB_ALLELE_A/B"  
	print "       -o :        output file"
	print "       -r :        reference fasta sequence"
	print "       -h :        this help \n"


def complement():
	d={}
	d["A"]="T"
	d["C"]="G"
	d["G"]="C"
	d["T"]="A"
	d["a"]="t"
	d["c"]="g"
	d["g"]="c"
	d["t"]="a"
	return d

  
def main():
	print "\n------------------------------------------------------------"
	print "Prob2RefAllel.py will extract reference allele from cleaned "
	print "probs of chip manifest"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mathieu.bourgey@mcgill.ca"
	print "------------------------------------------------------------\n"
	inF, outF, refF = getarg(sys.argv)
	snp={}
	f=open(inF,'r')
	l=f.readline()
	print "importing good probes ..."
	ct=0
	while l != "":
		c=l.split()
		alleles=c[3].split("/")
		if not snp.has_key(str(c[1])):
			snp[str(c[1])]={}
		if snp[str(c[1])].has_key(int(c[2])):
			print "position: "+str(c[1])+"-"+str(c[2])+" duplicated ;" +",".join(snp[str(c[1])][int(c[2])])+";" +",".join(c)
		snp[str(c[1])][int(c[2])]=[c[1],c[2],c[0],"",alleles[0],alleles[1]]
		l=f.readline()
		ct=ct+1
	f.close()
	snpChr=snp.keys()
	snpChr.sort()
	print snpChr
	print "Done - found "+str(ct)+" SNPs"
	print "\n------------------------------------------------------------"
	print "importing reference genome..."
	refSeq={}
	for seq_record in SeqIO.parse(refF, "fasta"):
		if snp.has_key(str(seq_record.id)):
			refSeq[str(seq_record.id)]=seq_record.seq
			print str(seq_record.id)
	seqName=refSeq.keys()
	seqName.sort()
	print "Done"
	print "\n------------------------------------------------------------"
	print "Checking probes vs ref compatibility..."
	mapName={}
	autosomes=[]
	allosomes=[]
	mitoK=[]
	nonK=[]
	ct=0
	for i in seqName :
		if i[:3].upper() == "CHR":
			d=i.upper().split("CHR")[1]
		else :
			d=i.upper()
		if d.isdigit() :
			autosomes.append(int(d))
		elif d == "X" or  d == "Y":
			allosomes.append(d)
		elif d[0] == "M" :
			mitoK.append(d)
		else:
			nonK.append(d)
		mapName[str(d)]=i
	o=open(outF,'w')
	comp=complement()
	if autosomes and len(autosomes) > 0:
		autosomes.sort()
		print autosomes
		for i in autosomes:
			snpPos=snp[mapName[str(i)]].keys()
			snpPos.sort()
			for j in snpPos :
				reference=refSeq[mapName[str(i)]][j-1]
				if reference == snp[mapName[str(i)]][j][4] or reference == snp[mapName[str(i)]][j][5] :
					snp[mapName[str(i)]][j][3]=reference
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				elif reference == comp[snp[mapName[str(i)]][j][4]] or reference == comp[snp[mapName[str(i)]][j][5]] :
					snp[mapName[str(i)]][j][3]=reference
					snp[mapName[str(i)]][j][4]=comp[snp[mapName[str(i)]][j][4]]
					snp[mapName[str(i)]][j][5]=comp[snp[mapName[str(i)]][j][5]]
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				else:
					print reference +" :"+",".join(snp[mapName[str(i)]][j])
					ct=ct+1
	if allosomes and len(allosomes) > 0:
		allosomes.sort()
		print allosomes
		for i in allosomes:
			snpPos=snp[mapName[str(i)]].keys()
			snpPos.sort()
			for j in snpPos :
				reference=refSeq[mapName[str(i)]][j-1]
				if reference == snp[mapName[str(i)]][j][4] or reference == snp[mapName[str(i)]][j][5] :
					snp[mapName[str(i)]][j][3]=reference
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				elif reference == comp[snp[mapName[str(i)]][j][4]] or reference == comp[snp[mapName[str(i)]][j][5]] :
					snp[mapName[str(i)]][j][3]=reference
					snp[mapName[str(i)]][j][4]=comp[snp[mapName[str(i)]][j][4]]
					snp[mapName[str(i)]][j][5]=comp[snp[mapName[str(i)]][j][5]]
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				else:
					print reference +" :"+",".join(snp[mapName[str(i)]][j])
					ct=ct+1
	if mitoK and len(mitoK) > 0:
		mitoK.sort()
		print mitoK
		for i in mitoK:
			snpPos=snp[mapName[str(i)]].keys()
			snpPos.sort()
			for j in snpPos :
				reference=refSeq[mapName[str(i)]][j-1]
				if reference == snp[mapName[str(i)]][j][4] or reference == snp[mapName[str(i)]][j][5] :
					snp[mapName[str(i)]][j][3]=reference
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				elif reference == comp[snp[mapName[str(i)]][j][4]] or reference == comp[snp[mapName[str(i)]][j][5]] :
					snp[mapName[str(i)]][j][3]=reference
					snp[mapName[str(i)]][j][4]=comp[snp[mapName[str(i)]][j][4]]
					snp[mapName[str(i)]][j][5]=comp[snp[mapName[str(i)]][j][5]]
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				else:
					print reference +" :"+",".join(snp[mapName[str(i)]][j])
					ct=ct+1
	if nonK and len(nonK) > 0 :
		nonK.sort()
		print nonK
		for i in nonK:
			snpPos=snp[mapName[str(i)]].keys()
			snpPos.sort()
			for j in snpPos :
				reference=refSeq[mapName[str(i)]][j-1]
				if reference == snp[mapName[str(i)]][j][4] or reference == snp[mapName[str(i)]][j][5] :
					snp[mapName[str(i)]][j][3]=reference
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				elif reference == comp[snp[mapName[str(i)]][j][4]] or reference == comp[snp[mapName[str(i)]][j][5]] :
					snp[mapName[str(i)]][j][3]=reference
					snp[mapName[str(i)]][j][4]=comp[snp[mapName[str(i)]][j][4]]
					snp[mapName[str(i)]][j][5]=comp[snp[mapName[str(i)]][j][5]]
					o.write(",".join(snp[mapName[str(i)]][j])+"\n")
					ct=ct+1
				else:
					print reference +" :"+",".join(snp[mapName[str(i)]][j])
					ct=ct+1
	print "Done - found "+str(ct)+" SNPs in ref"
	o.close()



main()

exit()





