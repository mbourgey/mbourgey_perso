#!/usr/bin/python

### Mathieu Bourgey (2011/03/21)
### simulate paire-end sequencing

import os
import sys
import string
import getopt
import re
import random
import gzip
from Bio import SeqIO

class peReads:
	def __init__(self,size1,size2):
		self.r1s=0
		self.r1e=0
		self.r1seq=""
		self.r1Qual="I"*size1
		self.rpID=""
		self.r2s=0
		self.r2e=0
		self.r2seq=""
		self.r2Qual="I"*size2


	
	def getID(self,ch):
		self.rpID="@"+str(ch)+"_"+str(self.r1s)+"_"+str(self.r1e)+"_"+str(self.r2s)+"_"+str(self.r2e)

	def writer(self,out1,out2) :
		out1.write('%s\n%s\n+\n%s\n' % (self.rpID,self.r1seq,self.r1Qual))
		out2.write('%s\n%s\n+\n%s\n' % (self.rpID,self.r2seq,self.r2Qual))
		
	def getReadPosF(self,x,rl,rl2,ins,sd) :
		self.r1s=x
		self.r1e=self.r1s+rl
		self.r2s=self.r1e+int(round(random.gauss(ins,sd)))
		self.r2e=self.r2s+rl2
		
	def getReadPosB(self,x,le,rl,rl2,ins,sd) :
		self.r2e=le-x
		self.r2s=self.r2e-rl2
		self.r1e=self.r2s-int(round(random.gauss(ins,sd)))
		self.r1s=self.r1e-rl
	


def getarg(argument):
	refF=""
	readL2=0
	move=1
	optli,arg = getopt.getopt(argument[1:],"l:s:m:i:d:r:o:h",['len','sec','move','ins','dev','ref','out','help'])
	if len(optli) < 5 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-l","--len"):
			readL=int(value)
		if option in ("-s","--sec"):
			readL2=int(value)
		if option in ("-i","--ins"):
			insS=int(value)
		if option in ("-d","--dev"):
			insD=int(value)	
		if option in ("-r","--ref"):
			refF=str(value)	
		if option in ("-m","--move"):
			move=int(value)	
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(refF) :
		sys.exit("Error - reference file not found:\n"+refR)
	if readL2 == 0:
		readL2=readL
	return  readL, readL2, move, insS, insD, refF, out



def usage():
	print "USAGE : getRefPE.py [option] "
	print "       -l :        read ends length (bp)"
	print "       -s :        optional read ends 2 length (bp)"
	print "       -m :        windows move (bp)"
	print "       -i :        insert size (bp)"
	print "       -d :        insert size standard deviation (bp)"
	print "       -r :        reference genome file "
	print "       -o :        output basename"
	print "       -h :        this help \n"


def main():
	print "\n---------------------------------------------------------------------------------"
	print "getRefPE.py will generate fastq files of theoretical paired end library covering "
	print "all the reference genome "
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	readL, readL2, move, insS, insD, refF, outB = getarg(sys.argv)
	out1= gzip.open(outB+".pair1.fastq.gz",'wb')
	out2= gzip.open(outB+".pair2.fastq.gz",'wb')
	peRforw=peReads(readL,readL2)
	peRback=peReads(readL,readL2)
	for seq_record in SeqIO.parse(refF, "fasta"):
		chroS=len(seq_record)
		chroN=seq_record.id
		slide=0
		walkOnChr=True
		while walkOnChr :
			peRforw.getReadPosF(slide,readL,readL2,insS,insD)
			peRforw.getID(chroN)
			peRback.getReadPosB(slide,chroS,readL,readL2,insS,insD)
			peRback.getID(chroN)
			if peRforw.r1s < peRback.r1s :
				peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
				peRforw.r2seq=str(seq_record.seq)[peRforw.r2s:peRforw.r2e]
				peRback.r1seq=str(seq_record.seq)[peRback.r1s:peRback.r1e]
				peRback.r2seq=str(seq_record.seq)[peRback.r2s:peRback.r2e]
				peRforw.writer(out1,out2)
				peRback.writer(out1,out2)
				slide=slide+move
			else :
				peRforw.r1seq=str(seq_record.seq)[peRforw.r1s:peRforw.r1e]
				peRforw.r2seq=str(seq_record.seq)[peRforw.r2s:peRforw.r2e]
				peRforw.writer(out1,out2)
				walkOnChr=False
			
	out1.close()
	out2.close()

main()

	    
	
	





