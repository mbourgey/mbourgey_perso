#!/usr/bin/python

### Mathieu Bourgey (2014/08/01)
##reverse-complement fasta or fastq

import os
import sys
import string
import getopt
import re
import itertools
import gzip
from Bio import SeqIO




def getarg(argument):
	typeS="fasta"
	gz=0
	optli,arg = getopt.getopt(argument[1:],"i:o:t:g:h",['input','out','type','gzip','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			out=str(value)   
		if option in ("-t","--type"):
			if (str(value) == "fastq" or str(value) == "fasta"):
				typeS=str(value)   
			else :
				print "unknow type: default fasta"
		if option in ("-g","--gzip"):
			gz=int(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - file not found:\n"+inF)
	return  inF, out, typeS, gz



def usage():
	print "USAGE : fast_revComp.py [option] "
	print "       -i :        input fastq file"
	print "       -o :        output fasta file"
	print "       -t :        format type :" 
	print "                      fasta (default)"
	print "                      fastq"
	print "       -g :        gziped input/output yes: 0 ; no: 1 (Default 0)"
	print "       -h :        this help \n"


def main():
	print "\n------------------------------------------------------------"
	print "fast_revComp.py will generate reverse complement file from a fastq/a file "
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, typeS, gz = getarg(sys.argv)
	records=[]
	if gz == 0 :
		o=gzip.open(outF, 'wb')
		i=gzip.open(inF, 'rb')
	else :
		o=open(outF, 'w')
		i=open(inF, 'r')
	for  record in SeqIO.parse(i, typeS): 
		records.append(record.reverse_complement())
	SeqIO.write(records, o, typeS)
	i.close()
	o.close()
	
	
	
main()

	    
	
	





