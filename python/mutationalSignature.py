#!/usr/bin/python

### Mathieu Bourgey (2014/10/14)
### extract mutation signature

import os
import sys
import string
import getopt
import re
import itertools
from Bio import SeqIO
import vcf

class chromSnpList :
	def __init__(self) :
		self.pos=[]
		self.nam=[]



def getarg(argument):
	dirF="fasta"
	fla=1
	optli,arg = getopt.getopt(argument[1:],"i:o:s:f:h",['input','out','snp','flanking','help'])
	if len(optli) < 1 :
		usage()
		sys.exit("Error : Missing argument(s)")
	for option, value in optli:
		if option in ("-i","--input"):
			inF=str(value)
		if option in ("-o","--output"):
			outF=str(value)
		if option in ("-s","--snp"):
			regF=str(value)   
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(inF) :
		sys.exit("Error - input file not found:\n"+inF)
	return  inF, outF, regF, fla



def usage():
	print "USAGE : mutationalSignature.py [option] "
	print "       -i :        input file"
	print "       -o :        output file"
	print "       -s :        SNP vcf 4 file"
	print "       -h :        this help \n"

def mutProfil():
	##first level (#6): mutation CA: C->A or G->T
	##second level (#16) :context AA: AxA
	dico={}
	dico["total"]=0
	dico["CA"]={}
	dico["CA"]["AA"]=0
	dico["CA"]["AC"]=0
	dico["CA"]["AG"]=0
	dico["CA"]["AT"]=0
	dico["CA"]["CA"]=0
	dico["CA"]["CC"]=0
	dico["CA"]["CG"]=0
	dico["CA"]["CT"]=0
	dico["CA"]["GA"]=0
	dico["CA"]["GC"]=0
	dico["CA"]["GG"]=0
	dico["CA"]["GT"]=0
	dico["CA"]["TA"]=0
	dico["CA"]["TC"]=0
	dico["CA"]["TG"]=0
	dico["CA"]["TT"]=0
	dico["CG"]={}
	dico["CG"]["AA"]=0
	dico["CG"]["AC"]=0
	dico["CG"]["AG"]=0
	dico["CG"]["AT"]=0
	dico["CG"]["CA"]=0
	dico["CG"]["CC"]=0
	dico["CG"]["CG"]=0
	dico["CG"]["CT"]=0
	dico["CG"]["GA"]=0
	dico["CG"]["GC"]=0
	dico["CG"]["GG"]=0
	dico["CG"]["GT"]=0
	dico["CG"]["TA"]=0
	dico["CG"]["TC"]=0
	dico["CG"]["TG"]=0
	dico["CG"]["TT"]=0
	dico["CT"]={}
	dico["CT"]["AA"]=0
	dico["CT"]["AC"]=0
	dico["CT"]["AG"]=0
	dico["CT"]["AT"]=0
	dico["CT"]["CA"]=0
	dico["CT"]["CC"]=0
	dico["CT"]["CG"]=0
	dico["CT"]["CT"]=0
	dico["CT"]["GA"]=0
	dico["CT"]["GC"]=0
	dico["CT"]["GG"]=0
	dico["CT"]["GT"]=0
	dico["CT"]["TA"]=0
	dico["CT"]["TC"]=0
	dico["CT"]["TG"]=0
	dico["CT"]["TT"]=0
	dico["TA"]={}
	dico["TA"]["AA"]=0
	dico["TA"]["AC"]=0
	dico["TA"]["AG"]=0
	dico["TA"]["AT"]=0
	dico["TA"]["CA"]=0
	dico["TA"]["CC"]=0
	dico["TA"]["CG"]=0
	dico["TA"]["CT"]=0
	dico["TA"]["GA"]=0
	dico["TA"]["GC"]=0
	dico["TA"]["GG"]=0
	dico["TA"]["GT"]=0
	dico["TA"]["TA"]=0
	dico["TA"]["TC"]=0
	dico["TA"]["TG"]=0
	dico["TA"]["TT"]=0
	dico["TC"]={}
	dico["TC"]["AA"]=0
	dico["TC"]["AC"]=0
	dico["TC"]["AG"]=0
	dico["TC"]["AT"]=0
	dico["TC"]["CA"]=0
	dico["TC"]["CC"]=0
	dico["TC"]["CG"]=0
	dico["TC"]["CT"]=0
	dico["TC"]["GA"]=0
	dico["TC"]["GC"]=0
	dico["TC"]["GG"]=0
	dico["TC"]["GT"]=0
	dico["TC"]["TA"]=0
	dico["TC"]["TC"]=0
	dico["TC"]["TG"]=0
	dico["TC"]["TT"]=0
	dico["TG"]={}
	dico["TG"]["AA"]=0
	dico["TG"]["AC"]=0
	dico["TG"]["AG"]=0
	dico["TG"]["AT"]=0
	dico["TG"]["CA"]=0
	dico["TG"]["CC"]=0
	dico["TG"]["CG"]=0
	dico["TG"]["CT"]=0
	dico["TG"]["GA"]=0
	dico["TG"]["GC"]=0
	dico["TG"]["GG"]=0
	dico["TG"]["GT"]=0
	dico["TG"]["TA"]=0
	dico["TG"]["TC"]=0
	dico["TG"]["TG"]=0
	dico["TG"]["TT"]=0
	return dico

def revComp():
	dico={}
	dico["AA"]="TT"
	dico["AC"]="GT"
	dico["AG"]="CT"
	dico["AT"]="AT"
	dico["CA"]="TG"
	dico["CC"]="GG"
	dico["CG"]="CG"
	dico["CT"]="AG"
	dico["GA"]="TC"
	dico["GC"]="GC"
	dico["GG"]="CC"
	dico["GT"]="AC"
	dico["TA"]="TA"
	dico["TC"]="GA"
	dico["TG"]="CA"
	dico["TT"]="AA"
	return dico

def comp():
	dico={}
	dico["AA"]="TT"
	dico["AC"]="TG"
	dico["AG"]="TC"
	dico["AT"]="TA"
	dico["CA"]="GT"
	dico["CC"]="GG"
	dico["CG"]="GC"
	dico["CT"]="GA"
	dico["GA"]="CT"
	dico["GC"]="CG"
	dico["GG"]="CC"
	dico["GT"]="CA"
	dico["TA"]="AT"
	dico["TC"]="AG"
	dico["TG"]="AC"
	dico["TT"]="AA"
	return dico


def main():
	print "\n------------------------------------------------------------"
	print "mutationSignature.py will generate table of the mutational signature "
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "------------------------------------------------------------\n"
	inF, outF, vcfF, fla= getarg(sys.argv)
	snp={}
	records=[]
	mp=mutProfil()
	rc=revComp()
	com=comp()
	print "loading SNPs ..."
	vcf_reader = vcf.Reader(open(vcfF,'r'))
	o=open(outF,'w')
	for record in vcf_reader:
		if record.is_snp and len(record.ALT) == 1:
			if not snp.has_key(record.CHROM) :
				snp[record.CHROM]=chromSnpList()
			snp[record.CHROM].pos.append(record.POS)
			snp[record.CHROM].nam.append(str(record.REF)+str(record.ALT[0]))
	print "... done"
	print "Counting mutation type ..."
	for seq_record in SeqIO.parse(inF, "fasta"):
		if snp.has_key(seq_record.id) :
			for i in range(0,len(snp[seq_record.id].pos),1) : 
				#print snp[seq_record.id].nam[i]
				targetR=seq_record[(int(snp[seq_record.id].pos[i])-fla):(int(snp[seq_record.id].pos[i])+fla+1)]
				#print str(targetR.seq)
				if snp[seq_record.id].nam[i][0] != str(targetR.seq)[1].upper() :
					#print "Complement SNP ..."
					snp[seq_record.id].nam[i]=com[snp[seq_record.id].nam[i]]
					#print snp[seq_record.id].nam[i]
					#print str(targetR.seq)
					#print "done"
					if snp[seq_record.id].nam[i][0] != str(targetR.seq)[1].upper() :
						print "Skipping SNP: "+str(seq_record.id)+":"+str(snp[seq_record.id].pos[i])
						continue
				if mp.has_key(snp[seq_record.id].nam[i]) :
					mp["total"]=mp["total"]+1
					mp[snp[seq_record.id].nam[i]][(str(targetR.seq)[0]+str(targetR.seq)[2]).upper()]=mp[snp[seq_record.id].nam[i]][(str(targetR.seq)[0]+str(targetR.seq)[2]).upper()]+1
				else :
					mp["total"]=mp["total"]+1
					mp[com[snp[seq_record.id].nam[i]]][rc[(str(targetR.seq)[0]+str(targetR.seq)[2]).upper()]]=mp[com[snp[seq_record.id].nam[i]]][rc[(str(targetR.seq)[0]+str(targetR.seq)[2]).upper()]]+1
	print "... done"
	mutKeys=["CA","CG","CT","TA","TC","TG"]
	contextKeys=["AA","AC","AG","AT","CA","CC","CG","CT","GA","GC","GG","GT","TA","TC","TG","TT"]
	header="total"
	mutsig=str(mp["total"])
	for i in mutKeys :
		for j in contextKeys:
			header=header+"\t"+i+"_"+j
			mutsig=mutsig+"\t"+str(mp[i][j])
	o.write(header+"\n")
	o.write(mutsig+"\n")
	o.close()
	
	
main()

	    
	
	





