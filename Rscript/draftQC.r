library(ggplot2)
options(scipen=999)

ARG=commandArgs(trailingOnly = T)
###ARG: input_file output_file project_name

a=read.table(ARG[1],header=F)

##############################################
#####input file should follow that format:
#0002	A00266_0357	1145152122	53.74
#0005	A00266_0357	572499194	26.87
#0007	A00266_0357	504914654	23.67
#0008	A00266_0356	849270092	40.01
#0009	A00266_0356	794556520	37.45
#0010	A00266_0356	657710750	30.85
#0012	A00266_0356	724204428	34.27
#0014	A00266_0356	889662102	41.88
#############################################


colnames(a)=c("Sample","Run","PFreads","DraftCov")
fcn=as.vector(unique(a[2])[,1])
h1=paste("Tolal PFReads per run",ARG[3],sep="  ")
h2=paste("Total Coverage per run",ARG[3],sep="  ")
v1=NULL
v2=NULL
for (i in fcn) {
    tpf=colSums(a[a[,2] == i,c(3,4)])
    h1=paste(h1,paste(i,"=",as.character(tpf[1])),sep="\n")
    h2=paste(h2,paste(i,"=",as.character(tpf[2])),sep="\n")
    v1=c(v1,tpf[1])
    v2=c(v2,tpf[2])
}
names(v1)=fcn
names(v2)=fcn


pdf(ARG[2])
ggplot(a, aes(x=as.character(Run), y=PFreads)) +  geom_boxplot() + geom_jitter(shape=16, position=position_jitter(0.2)) + labs(title=paste("PFReads per run",ARG[3],sep="  "),x="Flowcell", y = "PFReads") +  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1), plot.title = element_text(size=15)) 
ggplot(a, aes(x=as.character(Run), y=DraftCov)) +  geom_boxplot() + geom_jitter(shape=16, position=position_jitter(0.2)) + labs(title=paste("Coverage per run",ARG[3],sep="  "),x="Flowcell", y = "Draft mean coverage") + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1), plot.title = element_text(size=15))
ggplot(a, aes(x=DraftCov, y=PFreads))  +  geom_point(aes(colour = factor(Run)))  + labs(title="PFReads vs Coverage",x="Coverage", y = "PFReads")

dev.off()
