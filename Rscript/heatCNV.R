#Usage --args folderPath pattern cov output_img
args <- commandArgs(trailingOnly = TRUE)
tertiary=function(x){
te=NULL
for (i in 1:(dim(x)[1]-1)) {
	preD=NULL
	for (j in (i+1):dim(x)[1]) {
		dist=10
		const=1
		prvD=-1
		for (k in 1:dim(x)[2]) {
			if (x[i,k] == x[j,k] ) {
				if ( prvD == 0 & x[i,k] != 0) const=const+1 
				dist=dist+0
				prvD=0
			} else if (x[i,k] != 0 & x[j,k] != 0) {
				#if ( prvD == 1)  const=const+2
				dist=dist+2
				prvD=1
			} else {
				if ( prvD == 2) 
				prvD=2
				dist=dist+5
			}
		}
                const=1
		preD=c(preD,dist/const)
	}
	te=c(te,preD)
}
attr(te, "Size") <- dim(x)[1]
attr(te, "Labels") <- NULL
attr(te, "Diag") <- F
attr(te, "Upper") <- F
attr(te, "method") <- "tertiary"
attr(te, "p") <- NULL
attr(te, "call") <- match.call()
class(te) <- "dist"
return(te)
}

li=list.files(args[1],pattern=args[2])
if (args[3] != "none") {
    tmpcovCK=read.table(args[3])
}
#print(tmpcovCK[1:3,])
covCK=NULL
for (i in 1:length(li)) {
    print(li[i])
    tmp=strsplit(li[i],".",fixed=T)
    tatmp=read.table(paste(args[1],li[i],sep="/"))
   if (args[3] != "none") {
       covCK=rbind(covCK,as.numeric(tmpcovCK[tmpcovCK[,1] == tmp[[1]][1],2:3]))
    if (i == 1) {
        ta=tatmp[,c(1,2,4)]
        nam=as.vector(tmpcovCK[tmpcovCK[,1] == tmp[[1]][1],1])
    } else {
        ta=cbind(ta,tatmp[,4])
        nam=c(nam,as.vector(tmpcovCK[tmpcovCK[,1] == tmp[[1]][1],1]))
    }
   } else {
	if (i == 1) {
		ta=tatmp[,c(1,2,4)]
		nam=tmp[[1]][1]
	} else {
		ta=cbind(ta,tatmp[,4])
		nam=c(nam,tmp[[1]][1])
	}
   }
}
print(nam)
ta_autosome=ta[ta[,1] != "Y" & ta[,1] != "MT",]
binN=dim(ta_autosome)[1]
chr=c(as.character(1:22),"X")
chrN=0
chrNmid=NULL
st=0
for (i in 1: length(chr)) {
    tmpc=length(ta_autosome[ta_autosome[,1]==chr[i],1])
    chrN=c(chrN,(st+tmpc)/binN)
    chrNmid=c(chrNmid,(st+(tmpc/2))/binN)
    st=st+tmpc
}
ta_a_mat=t(matrix(as.numeric(unlist(ta_autosome[,3:dim(ta_autosome)[2]])),ncol=length(li),byrow=F))
delR=colSums(ta_a_mat == -1) * -1
dupR=colSums(ta_a_mat == 1)
recTableTmp=cbind(ta_autosome[,1:2],ta_autosome[,2]+999999,delR,dupR)
recTable=recTableTmp[recTableTmp[,4] != 0 |  recTableTmp[,5] != 0,]
colnames(recTable)=c("Chr","Start","End","Deletion","Duplication")
write.table(recTable,paste(args[4],"ReccurenceTable.tsv",sep="_"),col.names=T,quote=F,sep="\t")
#ta_a_mat[ta_a_mat == 2]=0
#colnames(ta_a_mat)=as.character(ta_autosome[,1])
#if clustering on CNV
#tertiary(ta_a_mat)
hcr <- hclust(tertiary(ta_a_mat),method="ward")
#if clustering on pheno
# if (args[3] != "none") {
#     rownames(covCK)=nam
#     #hcr <- hclust(dist(covCK[,3]))
# }
ddr <- as.dendrogram(hcr)
ta_a_mat[ta_a_mat == 2]=0
rownames(ta_a_mat)=nam
ta_a_mat_order=ta_a_mat[hcr$order,]
if (args[3] != "none") {
    covCK_order=covCK[hcr$order,]
}
#jpeg(args[4],2200,2000)
jpeg(args[4],width=3000,height=2200)
if (args[3] != "none") {
    layout(rbind(c(1:3),c(4,5,6),c(7,8,9)), widths = c(1,3,0.6), heights = c(5,2,2))
} else {
    layout(rbind(c(1:2),c(3:4),c(5:6)), widths = c(0.5,3), heights = c(5,2,2))
}
par(mar=c(1,1,5,0))
plot(ddr, horiz = TRUE, axes = FALSE, yaxs = "i", leaflab ="none")
par(las=1)
if (args[3] != "none") {
    par(mar=c(1,10,5,0))
} else {
    par(mar=c(1,10,5,1))
}
image(t(ta_a_mat_order), breaks=c(-1.2,-0.1,0.1,1.2), col=c('blue','white','red'),axes=F)
axis(3,at=chrN,label=rep("",length(chrN)))
axis(3,at=chrNmid,label=as.character(chr),tick=F,cex.axis=2)
axis(2,at=seq(0,1,len=length(nam)),label=nam[hcr$order],cex.axis=2)
if (args[3] != "none") {
	par(mar=c(1,0,5,1))
	image(t(covCK_order), breaks=c(-1,0.1,1.1,2.1,3.1,4.1,5.1,6.1,7.1), col=c('white','yellow','orange','red','brown','blue' ,'green','grey'),axes=F)
	#axis(1,at=chrNmid,label=as.character(chr),tick=F,cex.axis=2)
	axis(3,at=c(0,1),label=c("GradeFFPE","GradeFroz"),tick=F,cex.axis=2)
	plot(-1,axes=F,xlim=c(0,1),ylim=c(0,1))
	par(mar=c(3,10,0,0))
	par(xaxs="i")
	plot(dupR,col='red',type="l",axes=F,ylim=c(0,100),ylab="Duplication recurrence",xlab="")
	axis(2,seq(0,100,by=5),cex.axis=2)
	axis(1,at=chrN*binN,label=rep("",length(chrN)))
	axis(1,at=chrNmid*binN,label=as.character(chr),tick=F,cex.axis=2)
	plot(-1,axes=F,xlim=c(0,1),ylim=c(0,1))
	plot(-1,axes=F,xlim=c(0,1),ylim=c(0,1))
	par(mar=c(0.2,10,0,0))
	par(xaxs="i")
	plot(delR,col='blue',type="l",axes=F,ylim=c(-100,0),ylab="Deletion recurrence",xlab="")
	axis(2,at=seq(-100,0,by=5),label=as.character(seq(-100,0,by=5)* -1),cex.axis=2)
	plot(-1,axes=F,xlim=c(0,1),ylim=c(0,1))
} else {
	plot(-1,axes=F,xlim=c(0,1),ylim=c(0,1))
	par(mar=c(3,10,0,0))
	par(xaxs="i")
	plot(dupR,col='red',type="l",axes=F,ylim=c(0,100),ylab="Duplication recurrence",xlab="")
	axis(2,seq(0,100,by=5),cex.axis=2)
	axis(1,at=chrN*binN,label=rep("",length(chrN)))
	axis(1,at=chrNmid*binN,label=as.character(chr),tick=F,cex.axis=2)
	plot(-1,axes=F,xlim=c(0,1),ylim=c(0,1))
	par(mar=c(0.2,10,0,0))
	par(xaxs="i")
	plot(delR,col='blue',type="l",axes=F,ylim=c(-100,0),ylab="Deletion recurrence",xlab="")
	axis(2,at=seq(-100,0,by=5),label=as.character(seq(-100,0,by=5)* -1),cex.axis=2)
}

dev.off()
