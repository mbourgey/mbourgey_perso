featF=""
minCell=10
minFeature=10
minUMI=100
maxUMI=30000
minGene=30
maxGene=6000
DoubletRate=0.075
maxMT=20

minU=c(0.625,0.625,0.625,0.625,0.75,0.625,0.625,0.75,0.75)
maxU=c(1.75,1.25,1.25,1.25,2.5,1.25,1.25,2,1.5)
minG=c(0.875,0.875,0.875,0.875,1,0.875,0.875,1,1)
maxG=c(2,2,1.75,1.5,2,2,1.5,1.75,1.5)
ct=1

reference=readRDS("/lb/project/dlanglais/CITE-seq_IRF1_May_2021/analysisMB_June2021/Seurat_Multimodal_reference/pbmc_multimodal.rds")
ge2=read.table("/lb/project/dlanglais/CITE-seq_IRF1_May_2021/analysisMB_June2021/gene.list")

for  (nam in c("564","568","569","570","080","082","139","HIP002","HIP015")) {
print(paste("####################\nAnalyzing:",nam))
te=readRDS(paste("extDataRDS/",nam,"_PBMC_cell.counts.matrices.rds",sep=""))
CT2 <- CreateSeuratObject(counts = te$exon, assay="RNA", min.cells = minCell, min.features = minFeature, project = nam)

CT2[["percent.mt"]] <- PercentageFeatureSet(CT2, pattern = "^MT-")
CD14CT2=CT2[["RNA"]]@data[rownames(CT2[["RNA"]]@data) == "CD14",]
if (length(CD14CT2) != length(colnames(CT2[["RNA"]]@data))) {
    CD14CT2=rep(0,length(colnames(CT2[["RNA"]]@data)))
}
CD79ACT2=CT2[["RNA"]]@data[rownames(CT2[["RNA"]]@data) == "CD79A",]
if (length(CD79ACT2) != length(colnames(CT2[["RNA"]]@data))) {
    CD79ACT2=rep(0,length(colnames(CT2[["RNA"]]@data)))
}
TRBC1CT2=CT2[["RNA"]]@data[rownames(CT2[["RNA"]]@data) == "CD3G",]
if (length(TRBC1CT2) != length(colnames(CT2[["RNA"]]@data))) {
    TRBC1CT2=rep(0,length(colnames(CT2[["RNA"]]@data)))
}
HBA2CT2=CT2[["RNA"]]@data[rownames(CT2[["RNA"]]@data) == "HBA2",]
if (length(HBA2CT2) != length(colnames(CT2[["RNA"]]@data))) {
    HBA2CT2=rep(0,length(colnames(CT2[["RNA"]]@data)))
}
LILRA4CT2=CT2[["RNA"]]@data[rownames(CT2[["RNA"]]@data) == "LILRA4",]
if (length(LILRA4CT2) != length(colnames(CT2[["RNA"]]@data))) {
    LILRA4CT2=rep(0,length(colnames(CT2[["RNA"]]@data)))
}
UMICT2=as.vector(CT2[["nCount_RNA"]][,1])
GENECT2=as.vector(CT2[["nFeature_RNA"]][,1])

pdf(paste(nam,"cellTypeMarker_hist.pdf",sep="."))
hist(CD14CT2,main="CD14 distribution",,breaks=seq(0,max(CD14CT2)+1))
hist(CD79ACT2,main="CD79A distribution",breaks=seq(0,max(CD79ACT2)+1))
hist(TRBC1CT2,main="TRBC1 distribution",breaks=seq(0,max(TRBC1CT2)+1))
hist(HBA2CT2,main="HBA2 distribution",breaks=seq(0,max(HBA2CT2)+1))
hist(LILRA4CT2,main="LILRA4 distribution",breaks=seq(0,max(LILRA4CT2)+1))
dev.off()

i=0

##############################

CTCT2=rep("Doublet",length(UMICT2))
CTCT2pool=rep("Other",length(UMICT2))

    CTCT2[as.vector(LILRA4CT2) <= i & as.vector(TRBC1CT2) <= i & as.vector(CD79ACT2) <= i & as.vector(CD14CT2) > i]="CD14_Myel"
    CTCT2[as.vector(LILRA4CT2) <= i & as.vector(TRBC1CT2) <= i & as.vector(CD79ACT2) > i & as.vector(CD14CT2) <= i]="CD79A_Bcell"
    CTCT2[as.vector(LILRA4CT2) <= i & as.vector(TRBC1CT2) > i & as.vector(CD79ACT2) <= i & as.vector(CD14CT2) <= i]="TRBC1_Tcell"
    #CTCT2[as.vector(LILRA4CT2) <= i & as.vector(TRBC1CT2) <= i & as.vector(CD79ACT2) <= i & as.vector(CD14CT2) <= i]="HBA2_eryth"
    CTCT2[as.vector(LILRA4CT2) > i & as.vector(TRBC1CT2) <= i & as.vector(CD79ACT2) <= i & as.vector(CD14CT2) <= i]="LILRA4_pDC"
    CTCT2[as.vector(LILRA4CT2) <= i & as.vector(TRBC1CT2) <= i & as.vector(CD79ACT2) <= i & as.vector(CD14CT2) <= i]="Unknown"
    CTCT2pool[CTCT2 == "CD14_Myel" | CTCT2 == "CD79A_Bcell" | CTCT2 == "TRBC1_Tcell" | CTCT2 == "LILRA4_pDC"]="B_T_Myel_pDC_pool"
cellTUMICT2=data.frame(UMICT2, GENECT2, CTCT2, CTCT2pool)

CTCT2DF=data.frame(CTCT2)
rownames(CTCT2DF)=colnames(x = CT2[["RNA"]]@data)
colnames(CTCT2DF)="cellType"



CT2= AddMetaData(object = CT2, metadata = CTCT2DF, col.name = "cellType")


meanCT2p=mean(cellTUMICT2[cellTUMICT2[,4] == "B_T_Myel_pDC_pool",1])
sdCT2p=sd(cellTUMICT2[cellTUMICT2[,4] == "B_T_Myel_pDC_pool",1])


meangCT2p=mean(cellTUMICT2[cellTUMICT2[,4] == "B_T_Myel_pDC_pool",2])
sdgCT2p=sd(cellTUMICT2[cellTUMICT2[,4] == "B_T_Myel_pDC_pool",2])

###plot
pdf(paste(nam, "UMIdist_cellTypeMarker.v2.pdf",sep="_"))

print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2pool))  +
geom_density(size=1) + xlim(0,30000) + ggtitle("UMI count distribution per CellType Marker pooled"))

print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +
geom_density(size=1) + xlim(0,30000) + ggtitle("UMI count distribution per CellType Marker"))


print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2pool))  +
geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(1*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(2.5*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker pooled\nGreen=mean;blue=-1SD;red=+2.5SD"))


print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +  geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(1.5*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(2.5*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker\nGreen=mean;blue=-1.5SD;red=+2.5SD"))


print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +  geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(1.25*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(2.25*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker\nGreen=mean;blue=-1.25SD;red=+2.25SD"))


print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +  geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(1*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(2*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker\nGreen=mean;blue=-1SD;red=+2SD"))

print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +  geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(0.75*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(1.75*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker\nGreen=mean;blue=-0.75SD;red=+1.75SD"))

print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +  geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(0.5*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(1.5*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker\nGreen=mean;blue=-0.5SD;red=+1.5SD"))

print(ggplot(cellTUMICT2, aes(UMICT2, color=CTCT2))  +  geom_density(size=1) + xlim(0,30000) + geom_vline(xintercept=meanCT2p, color="green") + geom_vline(xintercept=meanCT2p-(0.25*sdCT2p), color="blue") + geom_vline(xintercept=meanCT2p+(1.25*sdCT2p), color="red") + ggtitle("UMI count distribution per CellType Marker\nGreen=mean;blue=-0.25SD;red=+1.25SD"))

dev.off()



pdf(paste(nam, "GENEdist_cellTypeMarker.v2.pdf",sep="_"))

print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2pool))  +
geom_density(size=1) + xlim(0,5000)  + ggtitle("Gene count distribution per CellType Marker pooled"))

print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +
geom_density(size=1) + xlim(0,5000) +  ggtitle("Gene count distribution per CellType Marker"))


print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2pool))  +
geom_density(size=1) + xlim(0,5000) +  geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(1.5*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(2.5*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker pooled with Filter\nGreen=mean;blue=-1.5SD;red=+2.5SD"))


print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +  
geom_density(size=1) + xlim(0,5000) + geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(1.5*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(2.5*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker\nGreen=mean;blue=-1.5SD;red=+2.5SD"))

print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +  
geom_density(size=1) + xlim(0,5000) + geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(1.25*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(2.25*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker\nGreen=mean;blue=-1.25SD;red=+2.25SD"))
    
print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +  
geom_density(size=1) + xlim(0,5000) + geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(1*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(2*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker\nGreen=mean;blue=-1SD;red=+2SD"))

    
print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +  
geom_density(size=1) + xlim(0,5000) + geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(0.75*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(1.75*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker\nGreen=mean;blue=-0.75SD;red=+1.75SD"))

    
print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +  
geom_density(size=1) + xlim(0,5000) + geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(0.5*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(1.5*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker\nGreen=mean;blue=-0.5SD;red=+1.5SD"))
    
print(ggplot(cellTUMICT2, aes(GENECT2, color=CTCT2))  +  
geom_density(size=1) + xlim(0,5000) + geom_vline(xintercept=meangCT2p, color="green") + geom_vline(xintercept=meangCT2p-(0.25*sdgCT2p), color="blue") + geom_vline(xintercept=meangCT2p+(1.25*sdgCT2p), color="red") + ggtitle("Gene count distribution per CellType Marker\nGreen=mean;blue=-0.25SD;red=+1.25SD"))

dev.off()





## Normalize data
##30threads
plan("multiprocess", workers = 30)
##20Go of RAM per Thread
options(future.globals.maxSize = 20 * 1000 * 1024^2)

CT2.RN=rownames(CT2[["RNA"]])
CT2 <- NormalizeData(CT2, assay = "RNA")
CT2 <- ScaleData(CT2,vars.to.regress =  c("nFeature_RNA", "nCount_RNA", "percent.mt"), features=CT2.RN, assay = "RNA")
CT2 <- RunPCA(object = CT2, assay = "RNA", features=CT2.RN, verbose = FALSE, reduction.name = 'pca', npcs = 50 )

##QC plot

pdf(paste(nam,"VlnPlot_Gene_UMI_Mitho.pdf",sep="_"))
 
print(VlnPlot(CT2, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3) + ggtitle("Control2") )

dev.off()


pdf(paste(nam,"GenePlot_UMI_mitho.pdf",sep="_")) 

plot3 <- FeatureScatter(CT2, feature1 = "nCount_RNA", feature2 = "percent.mt") + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
plot4 <- FeatureScatter(CT2, feature1 = "nCount_RNA", feature2 = "nFeature_RNA") + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
print(CombinePlots(plots = list(plot3, plot4)) + ggtitle(nam))


dev.off()



CT2 <- subset(CT2, subset = nFeature_RNA > meangCT2p-(minG[ct]*sdgCT2p) & nFeature_RNA < meangCT2p+(maxG[ct]*sdgCT2p) & nCount_RNA > meanCT2p-(minU[ct]*sdCT2p) & nCount_RNA < meanCT2p+(maxU[ct]*sdCT2p) & percent.mt <= 20 )


##run analysis
CT2 <-  FindVariableFeatures(CT2)
CT2 <- SCTransform(CT2, vars.to.regress =  c("nFeature_RNA", "nCount_RNA", "percent.mt"), verbose = FALSE, assay= "RNA")
CT2 <- RunPCA(CT2, npcs = 50 , verbose = FALSE)


pdf(paste(nam,"PCs_plots.pdf",sep="."),paper='USr')
print(ElbowPlot(CT2, ndims = 50 ))
print(DimHeatmap(CT2, dims = 1:10, cells = 500, balanced = TRUE))
print(DimHeatmap(CT2, dims = 11:20, cells = 500, balanced = TRUE))
print(DimHeatmap(CT2, dims = 21:30, cells = 500, balanced = TRUE))
print(DimHeatmap(CT2, dims = 31:40, cells = 500, balanced = TRUE))
print(DimHeatmap(CT2, dims = 41:50, cells = 500, balanced = TRUE))
dev.off()


CT2 <- RunUMAP(CT2, dims = 1:25, verbose = FALSE)
CT2 <- FindNeighbors(CT2, reduction = "pca", dims = 1:25)
CT2 <- FindClusters(CT2, resolution = 0.5)
# CT2Markers=FindAllMarkers(CT2,logfc.threshold = 0,test.use="MAST")
# write.table(CT2Markers,paste(na,"RawCluster_markers.sdF.tsv",sep="_"),quote=F,sep="\t",col.names=T,row.names=F)

pdf(paste(nam,"RawClusters_UMAP.pdf",sep="."))
print(DimPlot(CT2, reduction = "umap",label=T))
print(DimPlot(CT2, reduction = "umap", group.by = "cellType"))
print(FeaturePlot(CT2, features = "nFeature_RNA"))
print(FeaturePlot(CT2, features = "nCount_RNA"))
print(FeaturePlot(CT2, features = "percent.mt"))
dev.off()

###Doublet finder
##-----
TCT2 = table(CT2@meta.data$cellType)
CT2.doubletRate=round(TCT2[names(TCT2) == "Doublet"]/sum(TCT2[names(TCT2) != "Unknown"]),3)

sweep.res.CT2 <- paramSweep_v3(CT2, PCs = 1:25, sct=T)
sweep.stats_CT2 <- summarizeSweep(sweep.res.CT2, GT = FALSE)
bcmvn_CT2 <- find.pK(sweep.stats_CT2)


homotypic.prop <- modelHomotypic(CT2@active.ident)           ## ex: annotations <- CT2@meta.data$ClusteringResults
nExp_poi_CT2 <- round(CT2.doubletRate*length(CT2@active.ident))  ## using estimated doublet formation rate - tailor for your dataset
nExp_poi.adj_CT2 <- round(nExp_poi_CT2*(1-homotypic.prop))

#Graph to get pK estimate
pdf(paste(nam,"meanBC_DF.sdF.pdf",sep="."))
print(plot(x=bcmvn_CT2$pK,y=bcmvn_CT2$MeanBC,main=paste("pK value exploration\noptimal pK is",as.character(bcmvn_CT2$pK[which.max(bcmvn_CT2$MeanBC)])),xlab="pK",ylab="MeanBC",type="l",col='blue'))
print(abline(v=bcmvn_CT2$pK[which.max(bcmvn_CT2$MeanBC)],col='red',lty=3))
dev.off()




# Run DoubletFinder with varying classification stringencies ----------------------------------------------------------------
CT2 <- doubletFinder_v3(CT2, PCs = 1:25, pN = 0.25, pK = as.numeric(as.vector(bcmvn_CT2$pK[which.max(bcmvn_CT2$MeanBC)])), nExp = nExp_poi_CT2, reuse.pANN = FALSE,sct=T)
CT2 <- doubletFinder_v3(CT2, PCs = 1:25, pN = 0.25, pK = as.numeric(as.vector(bcmvn_CT2$pK[which.max(bcmvn_CT2$MeanBC)])), nExp = nExp_poi.adj_CT2, reuse.pANN = colnames(CT2@meta.data)[grep("pANN",colnames(CT2@meta.data))],sct=T)
CT2@meta.data[,"DF_hi.lo"] <- CT2@meta.data[,grep("DF.classifications",colnames(CT2@meta.data))[1]]
CT2@meta.data$DF_hi.lo[which(CT2@meta.data$DF_hi.lo == "Doublet" & CT2@meta.data[,grep("DF.classifications",colnames(CT2@meta.data))[2]] == "Singlet")] <- "Doublet_lo"

# Plot results --------------------------------------------------------------------------------------------------------------
CT2@meta.data$DF_hi.lo[which(CT2@meta.data$DF_hi.lo == "Doublet")] <- "Doublet_hi"

pdf(paste(nam,"DFinder_UMAP.pdf",sep="."))
print(DimPlot(CT2, reduction = "umap",label=T))
print(DimPlot(CT2, reduction = "umap", group.by = "cellType"))
print(DimPlot(CT2, reduction = "umap", group.by="DF_hi.lo",order=c("Doublet_hi","Doublet_lo","Singlet"), cols=c("lightgrey","gold","red")))
dev.off()

CT2[["mean_UMI"]] = meanCT2p
CT2[["SD_UMI"]] = sdCT2p
CT2[["mean_GENE"]] = meangCT2p
CT2[["SD_GENE"]] = sdgCT2p
CT2[["Estimated_Doublet_Rate"]] = CT2.doubletRate


CT2 <- subset(CT2, cells = rownames(CTCT2DF)[CTCT2DF[,1] != "Doublet" ])
CT2 <- subset(CT2, cells = rownames(CT2[["DF_hi.lo"]])[CT2[["DF_hi.lo"]] == "Singlet"] )


##reference annotation

CT2.anchors <- FindTransferAnchors(reference = reference, query = CT2, normalization.method = "SCT", reference.reduction = "spca", dims = 1:50)
CT2 <- MapQuery(anchorset =CT2.anchors, query = CT2, reference = reference, refdata = list(celltype.l1 = "celltype.l1", celltype.l2 = "celltype.l2", predicted_ADT = "ADT"), reference.reduction = "spca", reduction.model = "wnn.umap")

##Final individual plots
#load PBMC reference genes


pdf(paste(nam,"final_UMAP_general.pdf",sep="."))
print(DimPlot(CT2, reduction = "umap",label=T))
print(DimPlot(CT2, reduction = "umap", group.by = "cellType"))
print(DimPlot(CT2, reduction = "umap", split.by = "cellType"))
print(DimPlot(CT2, reduction = "umap", group.by = "predicted.celltype.l1", label = TRUE, label.size = 3, repel = TRUE) + NoLegend())
print(DimPlot(CT2, reduction = "umap", group.by = "predicted.celltype.l2", label = TRUE, label.size = 3 ,repel = TRUE) + NoLegend())
dev.off()


CT2.features.plot=as.vector(ge2[ge2[,1] %in% rownames(CT2[["RNA"]]@data),1])

pdf(paste(nam,"final_UMAP_GeneRef.pdf",sep="."))
fp=FeaturePlot(CT2, reduction = "umap", features = CT2.features.plot, combine=F)
for(i in 1:length(fp)) {
     print(fp[[i]])
}
dev.off()


saveRDS(CT2,paste(nam,"QC.rds",sep=""))
ct=ct+1
}




