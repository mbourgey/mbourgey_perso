## Automate the use of  DNAC for calling CNV in WGS
##this script have been write  by Mathieu Bourgey - mbourgey@genomequebec.com
##last modified - 2013/05/27
##include rescue calls
rm(list=ls())

#####################################
##Usage
usage=function(errM) {
	cat("\nUSAGE : DNACRD.1.5.R [option] <Value>\n")
	cat("       -f        : binned read depth count file\n")
	cat("       -o        : output file \n")
	cat("       -c        : GC content and mappability file \n")
	cat("       -g        : apply GC correction (0: true 1: False ; default true) \n")
	cat("       -m        : apply mappability correction (0: true 1: False ; default true) \n")
	cat("       -s        : apply ratio smoothing step (0: true 1: False ; default true) \n")
	cat("       -b        : bin size default 30kb \n")
	cat("       -d        : minimum consecutive bin support default 5 \n")
	cat("       -a        : approach (0: Individual 1: Somatic 2: Somatic+Germiline ; default Somatic+Germiline) \n")
	cat("       -t        : threads (default 10) \n")
	cat("       -z        : mappability bin thershold (default 1 - 1% max \n")
#	cat("       -v        : bed file of CNV calls for comparison\n")
#	cat("       -e        : exclusion file of telomeric sequences\n")
	cat("       -h        : this help\n\n")
	stop(errM)
}


##################################
## get args
ARG=commandArgs(trailingOnly = T)
if (length(ARG) == 0) {
	usage("no argument")
}
## defult arg values
rF=""
out=""
gcF=""
corG=TRUE
corM=TRUE
smoR=TRUE
threads=10
binSize=30000
binMin=5
mapMax=1
metR="somG"
otherM=""
doComp=FALSE
## get arg variables
cat("-----------------------------------------------------\n")
cat("reading arguments\n")
for (i in 1:length(ARG)) {
	if (ARG[i] == "-f") {
		rF=ARG[i+1]
		cat(paste("bin file:",rF,"\n",sep=" "))
	}  else if (ARG[i] == "-o") {
		out=ARG[i+1]
		cat(paste("output file:",out,"\n",sep=" "))
	}  else if (ARG[i] == "-c") {
		gcF=ARG[i+1]
		cat(paste("GC and Unmapp bin file:",gcF,"\n",sep=" "))
	}  else if (ARG[i] == "-g") {
		corGCarg=as.numeric(ARG[i+1])
		if (corGCarg == 0) {
			corG=TRUE
			cat("Apply GC correction\n")
		} else if (corGCarg == 1) {
			corG=FALSE
			cat("Do not apply GC correction\n")
		} else {
			usage("unknown -g argument")
		}
	}  else if (ARG[i] == "-t") {
		threads=as.numeric(ARG[i+1]) 
		cat(paste("number of threads =",as.character(threads),"\n",sep=""))
	} else if (ARG[i] == "-m") {
		corMarg=as.numeric(ARG[i+1])
		if (corMarg == 0) {
			corM=TRUE
			cat("Apply mappability correction\n")
		} else if (corMarg == 1) {
			corM=FALSE
			cat("Do not apply mappability correction\n")
		} else {
			usage("unknown -m argument")
		}
	}  else if (ARG[i] == "-s") {
		smoarg=as.numeric(ARG[i+1])
		if (smoarg == 0) {
			smoR=TRUE
			cat("Apply smoothing on ratios\n")
		} else if (smoarg == 1) {
			smoR=FALSE
			cat("Do not apply smoothing on ratios\n")
		} else {
			usage("unknown -s argument")
		}
	}  else if (ARG[i] == "-b") {
		binSize=as.numeric(ARG[i+1])
		cat(paste("Bin size =",as.character(binSize),"\n",sep=""))
	}  else if (ARG[i] == "-d") {
		binMin=as.numeric(ARG[i+1])
		cat(paste("Minimum consecutive bin =",as.character(binMin),"\n",sep=""))
	}  else if (ARG[i] == "-a") {
		apparg=as.numeric(ARG[i+1])
		if (apparg == 0) {
			metR="ind"
			cat("CNV detection: Individual\n")
		} else if (apparg == 1) {
			metR="som"
			cat("CNV detection: Somatic\n")
		}  else if (apparg == 2) {
			metR="somG"
			cat("CNV detection: Somatic+Germline\n")
		} else {
			usage("unknown -s argument")
		}
		ChrN=ARG[i+1]
	}  else if (ARG[i] == "-v") {
		otherM=ARG[i+1]
		cat(paste("comparison file:",otherM,"\n"))
#	}  else if (ARG[i] == "-e") {
#		exclF=ARG[i+1]
	}   else if (ARG[i] == "-z") {
		mapMax=as.numeric(ARG[i+1])
		cat(paste("Bin size =",as.character(binSize),"\n",sep=""))
	} else if (ARG[i] == "-h") {
		usage(" ")
	}
}
## check arg consitency
if (!(file.exists(rF))) {
	usage("Error : binned read depth count file not found")
}
if (file.exists(otherM)) {
	cat("Do comparison\n")
	doComp=TRUE
	otherR=read.table(otherM,sep="\t")
}
if (out == "") {
	usage("Error : Output basename not specified")
}

if (corM || corG) {
	if (gcF == "") {
		usage("Error : GC and Mappabilty file not specified")
	}
}


##listfunction
lte=function(x) return(length(x))

## main
library(DNAcopy)
library(parallel)
cat("Reading files\n")
tmpdataF=read.table(rF,sep="\t",header=T)
##data cleanning
if (corM || corG) {
	tmpCor=read.table(gcF,sep="\t",header=T)
	tmpdataF2=cbind(tmpdataF[,1:5],tmpCor[,4:5])
	if (corM) {
		cat("Mappability correction\n")
		tmpdataF3=tmpdataF2[tmpdataF2[,7] < mapMax,]
	} else {
		tmpdataF3=tmpdataF2
	}
	if (corG) {
		cat("GC correction\n")
		mdall1=median(tmpdataF3[,4])
		mdall2=median(tmpdataF3[,5])
		tmpdataF4=tmpdataF3
		for (i in 1:101) {
			tmp1=tmpdataF4[tmpdataF3[,6] >= (i-1) & tmpdataF3[,6] < i,4]
			tmp2=tmpdataF4[tmpdataF3[,6] >= (i-1) & tmpdataF3[,6] < i,5]
			if (length(tmp1) > 0 & median(tmp1) > 0 &  length(tmp2) > 0 & median(tmp2) > 0) {
				tmpdataF4[tmpdataF3[,6] >= (i-1) & tmpdataF3[,6] < i,4]=tmpdataF4[tmpdataF3[,6] >= (i-1) & tmpdataF3[,6] < i,4]*(mdall1/median(tmp1))
				tmpdataF4[tmpdataF3[,6] >= (i-1) & tmpdataF3[,6] < i,5]=tmpdataF4[tmpdataF3[,6] >= (i-1) & tmpdataF3[,6] < i,5]*(mdall2/median(tmp2))
			}
		}
	} else {
		tmpdataF4=tmpdataF2
	}
} else {
	tmpdataF4=cbind(tmpdataF[,1:5])
}
tmpdataF5=tmpdataF4[-1*(c(grep("GL",tmpdataF4[,1]),grep("M",tmpdataF4[,1]))),]
dataF=tmpdataF5[tmpdataF5[,4] > 0 & tmpdataF5[,5] > 0,]
##Normaization
cat("Normalisation\n")
Tcount=as.numeric(as.vector(dataF[,4]))
meT=mean(Tcount)
Ncount=as.numeric(as.vector(dataF[,5]))*(sum(Tcount)/sum(as.numeric(as.vector(dataF[,5]))))
meN=mean(Ncount)
##get genomic information
Chr=as.vector(dataF[,1])
chrN=round(as.numeric(gsub("Y","24",gsub("X","23",Chr))))
chrNt=table(chrN)
chrNtn=names(table(Chr))
chrNtnD=floor(length(chrNtn)/threads)
chrNtnS=list()
cp=0
for (i in 1:chrNtnD) {
	chrNtnS[[i]]=chrNtn[(cp+1):(cp+threads)]
	cp=cp+threads
}
if (length(chrNtnS) != length(chrNtn)) {
	mth=TRUE
	if (cp < length(chrNtn)) chrNtnS[[chrNtnD+1]]=chrNtn[(cp+1):length(chrNtn)]
} else {
	mth=FALSE
}
chrNcs=cumsum(chrNt)
chrNmid=as.vector(chrNcs-(chrNt/2))
Pos=dataF[,2]
###get clean logRatio
cat("Generate ratio\n")
logR=cbind(log2(Tcount/Ncount),log2(Tcount/meT),log2(Ncount/meN))
# if (metR == "somG") {
# 	###output Normal Tumor bed file to allow the use of ASCAT on WGS data 
# 	outASCAT=cbind(dataF[,1:3],logR[,2:3])
# 	colnames(outASCAT)=c("Chr","Start","End","logR_tumor","logR_normal")
# 	write.table(outASCAT,file=paste(out,"ToAscat.tsv",sep="_"),sep="\t",quote=F,col.names=T,row.names=F)
# }
### get sample specific CNV detection thresholds -- Hampell outlier detection
med1=median(logR[,1])
devM1=mad(logR[,1])
ri1=abs(logR[,1]-med1)
th1L=c(min(logR[ri1 <=  3*devM1,1]),max(logR[ri1 <=  3*devM1,1]))
##tmp
#print(th1L)
med2=median(logR[,2])
devM2=mad(logR[,2])
ri2=abs(logR[,2]-med2)
th2L=c(min(logR[ri2 <=  3*devM2,2]),max(logR[ri2 <=  3*devM2,2]))
med3=median(logR[,3])
devM3=mad(logR[,3])
ri3=abs(logR[,3]-med3)
th3L=c(min(logR[ri3 <=  3*devM3,3]),max(logR[ri3 <=  3*devM3,3]))
### CNV callings and output graph
if (metR == "ind") {
	print(th2L)
	## smoothing and segmentation function
	runSeg1 = function(l,ch,po,sm){
		CNA.object=CNA(l,ch,po, data.type="logratio",sampleid="I")
		if (sm) {
			#cat("smoothing\n")
			smoothed.CNA.object=smooth.CNA(CNA.object)
		} else {
			smoothed.CNA.object=CNA.object
		}
		#cat("segmentation\n")
		segment1=segment(smoothed.CNA.object, verbose=1)
		return(segment1$output)
	}
	## multithreding of S and S step
	if (mth) {
		cat("mutlithreading smoothing and segmentation\n")
		segment1=NULL
		for (i in 1:length(chrNtnS)) {
			for (j in 1:length(chrNtnS[[i]])) {
				chI=Chr[Chr == chrNtnS[[i]][j]]
				poI=Pos[Chr == chrNtnS[[i]][j]]
				lI=logR[Chr == chrNtnS[[i]][j],2]
				cmd=expression(runSeg1(lI,chI,poI,smoR))
				sink("/dev/null")
				mcparallel(cmd)
				sink()
			}
			tmpres=mccollect()
			cat(paste("Chr " ,paste(chrNtnS[[i]],collapse=" ")," : threads collected\n",sep=""))
			for (j in 1:length(chrNtnS[[i]])) {
				segment1=rbind(segment1,tmpres[[j]])
			}
		}
	} else {
		segment1=runSeg1(logR[,2],Chr,Pos,smoR)
	}
	###rawcalls
	covDifC2=data.frame(as.character(as.vector(segment1[segment1[,1] == "I",1])),as.vector(as.vector(segment1[segment1[,1] == "I",2])),as.numeric(as.vector(segment1[segment1[,1] == "I",3])),as.numeric(as.vector(segment1[segment1[,1] == "I",4])),as.numeric(as.vector(segment1[segment1[,1] == "I",5])),as.numeric(as.vector(segment1[segment1[,1] == "I",6])))
	colnames(covDifC2)=c("ID","chrom","start","end","probNum","seg.mean")
	covDifC2T2=covDifC2[(covDifC2[,5] >= binMin),]
	preCallL=covDifC2T2[ (covDifC2T2[,5] >= (3*binMin)) & (covDifC2T2[,6] < (th2L[1]) |  covDifC2T2[,6] > (th2L[2]) ),]
	covDifC2T2k=NULL
	covDifC2T2s=NULL
	for ( i in chrNtn ) {
		tmp=covDifC2T2[covDifC2T2[,2] == i,]
		if (dim(tmp)[1] >= 4) {
			covDifC2T2s=rbind(covDifC2T2s,tmp)
		} else {
			covDifC2T2k=rbind(covDifC2T2k,tmp)
		}
	}
	### calls refinement and extension
	C2.CNA.object=CNA(covDifC2T2s[,6],covDifC2T2s[,2],covDifC2T2s[,3], data.type="logratio",sampleid="I")
	C2.segment1=segment(C2.CNA.object, verbose=1,alpha=0.2,nperm=100)
	colnames(C2.segment1$output)=c("ID","chrom","start","end","probNum","seg.mean")
	C2.segment1.all=rbind(C2.segment1$output,covDifC2T2k)
	C2.segment1.all=C2.segment1.all[order(C2.segment1.all[,2],C2.segment1.all[,3]),]
	TcallsL=C2.segment1.all[(C2.segment1.all[,6] < th2L[1] |  C2.segment1.all[,6] > th2L[2]),]
	### raw call rescue
	for (i in 1:dim(preCallL)[1]) {
		if (length(preCallL[as.vector(TcallsL[,2]) == as.vector(preCallL[i,2]) & ((as.vector(TcallsL[,3]) <= as.vector(preCallL[i,3]) & as.vector(TcallsL[,4]) >= as.vector(preCallL[i,3])) | (as.vector(TcallsL[,3]) >= as.vector(preCallL[i,3]) & as.vector(TcallsL[,3]) <= as.vector(preCallL[i,4]))),1]) < 1) {
			TcallsL=rbind(TcallsL,preCallL[i,])
		}
	}
	### calls annotation
	if (dim(na.omit(TcallsL))[1] != 0) {
		typ=NULL
		onco=NULL
		for ( i in 1:dim(TcallsL)[1]) {
			onco=c(onco,"I")
			if (TcallsL[i,6] > 0) {
				typ=c(typ,3)
			} else {
				typ=c(typ,1)
			}
			TcallsL[i,4]=covDifC2T2[as.vector(covDifC2T2[,2]) == as.vector(TcallsL[i,2]) & (as.vector(covDifC2T2[,3]) == as.vector(TcallsL[i,4]) | as.vector(covDifC2T2[,4]) == as.vector(TcallsL[i,4])),4]+ binSize
		}
		GLfinal=cbind(TcallsL[,c(2,3,4,6)],typ,onco)
	} else {
		GLfinal=NULL
	}
	Lcalls=GLfinal
	##output genomic ratio
	jpeg(paste(out,"GenomicRatios.jpeg",sep="_"),1200,400)
	plot(logR[,2],main="Individual ratio",axes=F,xlab="",ylab="logR")
	axis(1,at=c(0,chrNcs),labels=F)
	axis(1,at=chrNmid,labels=names(chrNt),tick=F)
	axis(2)
	abline(h=mean(logR[,2]))
	dev.off()
} else if (metR == "som") {
	print(th1L)
	## smoothing and segmentation function
	runSeg1 = function(l,ch,po,sm){
		CNA.object=CNA(l,ch,po, data.type="logratio",sampleid="T")
		if (sm) {
			cat("smoothing\n")
			smoothed.CNA.object=smooth.CNA(CNA.object)
		} else {
			smoothed.CNA.object=CNA.object
		}
		cat("segmentation\n")
		segment1=segment(smoothed.CNA.object, verbose=1)
		return(segment1$output)
	}
	## multithreding of S and S step
	if (mth) {
		cat("mutlithreading smoothing and segmentation\n")
		segment1=NULL
		for (i in 1:length(chrNtnS)) {
			for (j in 1:length(chrNtnS[[i]])) {
				chI=Chr[Chr == chrNtnS[[i]][j]]
				poI=Pos[Chr == chrNtnS[[i]][j]]
				lI=logR[Chr == chrNtnS[[i]][j],1]
				cmd=expression(runSeg1(lI,chI,poI,smoR))
				sink("/dev/null")
				mcparallel(cmd)
				sink()
			}
			tmpres=mccollect()
			cat(paste("Chr " ,paste(chrNtnS[[i]],collapse=" ")," : threads collected\n",sep=""))
			for (j in 1:length(chrNtnS[[i]])) {
				segment1=rbind(segment1,tmpres[[j]])
			}
		}
	} else {
		segment1=runSeg1(logR[,2],Chr,Pos,smoR)
	}
	###rawcalls
	covDifC1=data.frame(as.character(as.vector(segment1[segment1[,1] == "T",1])),as.vector(as.vector(segment1[segment1[,1] == "T",2])),as.numeric(as.vector(segment1[segment1[,1] == "T",3])),as.numeric(as.vector(segment1[segment1[,1] == "T",4])),as.numeric(as.vector(segment1[segment1[,1] == "T",5])),as.numeric(as.vector(segment1[segment1[,1] == "T",6])))
	colnames(covDifC1)=c("ID","chrom","start","end","probNum","seg.mean")
	covDifC1T2=covDifC1[(covDifC1[,5] >= binMin),]
	preCallL=covDifC1T2[ (covDifC1T2[,5] >= (3*binMin)) & (covDifC1T2[,6] < (th1L[1]) |  covDifC1T2[,6] > (th1L[2]) ),]
	covDifC1T2k=NULL
	covDifC1T2s=NULL
	for ( i in chrNtn ) {
		tmp=covDifC1T2[covDifC1T2[,2] == i,]
		if (dim(tmp)[1] >= 4) {
			covDifC1T2s=rbind(covDifC1T2s,tmp)
		} else {
			covDifC1T2k=rbind(covDifC1T2k,tmp)
		}
	}
	### calls refinement and extension
	C1.CNA.object=CNA(covDifC1T2s[,6],covDifC1T2s[,2],covDifC1T2s[,3], data.type="logratio",sampleid="NT")
	C1.segment1=segment(C1.CNA.object, verbose=1,alpha=0.2,nperm=100)
	colnames(C1.segment1$output)=c("ID","chrom","start","end","probNum","seg.mean")
	C1.segment1.all=rbind(C1.segment1$output,covDifC1T2k)
	C1.segment1.all=C1.segment1.all[order(C1.segment1.all[,2],C1.segment1.all[,3]),]
	TcallsL=C1.segment1.all[(C1.segment1.all[,6] < th1L[1] |  C1.segment1.all[,6] > th1L[2]),]
	### large raw call rescue
	for (i in 1:dim(preCallL)[1]) {
		if (length(preCallL[TcallsL[,2] == preCallL[i,2] & ((TcallsL[,3] <= preCallL[i,3] & TcallsL[,4] >= preCallL[i,3]) | (TcallsL[,3] >= preCallL[i,3] & TcallsL[,3] <= preCallL[i,4])),1]) < 1) {
			TcallsL=rbind(TcallsL,preCallL[i,])
		}
	}
	TcallsL=TcallsL[order(TcallsL[,2],TcallsL[,3]),]
	### calls annotation
	if (dim(na.omit(TcallsL))[1] != 0) {
		typ=NULL
		onco=NULL
		for ( i in 1:dim(TcallsL)[1]) {
			onco=c(onco,"T")
			if (TcallsL[i,6] > 0) {
				typ=c(typ,3)
			} else {
				typ=c(typ,1)
			}
			TcallsL[i,4]=covDifC1T2[as.vector(covDifC1T2[,2]) == as.vector(TcallsL[i,2]) & (as.vector(covDifC1T2[,3]) == as.vector(TcallsL[i,4]) | as.vector(covDifC1T2[,4]) == as.vector(TcallsL[i,4])),4]+ binSize
		}
		GLfinal=cbind(TcallsL[,c(2,3,4,6)],typ,onco)
	} else {
		GLfinal=NULL
	}
	Lcalls=GLfinal
	##output genomic ratio
	jpeg(paste(out,"GenomicRatios.jpeg",sep="_"),1200,400)
	plot(logR[,1],main="Somatic ratio",axes=F,xlab="",ylab="logR",ylim=c(min(logR[,1])-1,max(logR[,1])+1))
	axis(1,at=c(0,chrNcs),labels=F)
	axis(1,at=chrNmid,labels=names(chrNt),tick=F)
	axis(2)
	abline(h=mean(logR[,1]))
	dev.off()
} else if (metR == "somG") {
	print(th1L)
	print(th2L)
	print(th3L)
	## smoothing and segmentation function
	runSeg1 = function(l,ch,po,sm){
		CNA.object=CNA(l,ch,po, data.type="logratio",sampleid=c("TN","T","N"))
		if (sm) {
			#cat("smoothing\n")
			smoothed.CNA.object=smooth.CNA(CNA.object)
		} else {
			smoothed.CNA.object=CNA.object
		}
		#cat("segmentation\n")
		segment1=segment(smoothed.CNA.object, verbose=1)
		return(segment1$output)
	}
	## multithreding of S and S step
	if (mth) {
		cat("mutlithreading smoothing and segmentation\n")
		segment1=NULL
		for (i in 1:length(chrNtnS)) {
			for (j in 1:length(chrNtnS[[i]])) {
				chI=Chr[Chr == chrNtnS[[i]][j]]
				poI=Pos[Chr == chrNtnS[[i]][j]]
				lI=logR[Chr == chrNtnS[[i]][j],]
				cmd=expression(runSeg1(lI,chI,poI,smoR))
				sink("/dev/null")
				mcparallel(cmd)
				sink()
			}
			tmpres=mccollect()
			cat(paste("Chr " ,paste(chrNtnS[[i]],collapse=" ")," : threads collected\n",sep=""))
			for (j in 1:length(chrNtnS[[i]])) {
				segment1=rbind(segment1,tmpres[[j]])
			}
		}
	} else {
		segment1=runSeg1(logR,Chr,Pos,smoR)
	}
	###rawcalls
#	print(segment1[1:20,])
	covDifC1=data.frame(as.character(as.vector(segment1[segment1[,1] == "TN",1])),as.character(as.vector(segment1[segment1[,1] == "TN",2])),as.numeric(as.vector(segment1[segment1[,1] == "TN",3])),as.numeric(as.vector(segment1[segment1[,1] == "TN",4])),as.numeric(as.vector(segment1[segment1[,1] == "TN",5])),as.numeric(as.vector(segment1[segment1[,1] == "TN",6])))
	colnames(covDifC1)=c("ID","chrom","start","end","probNum","seg.mean")
	## tmp
	#write.table(covDifC1,file=paste(out,"CovDiffC",sep="_"),quote=F,sep="\t",col.names=T,row.names=F)
	covDifC2=data.frame(as.character(as.vector(segment1[segment1[,1] == "T",1])),as.vector(as.vector(segment1[segment1[,1] == "T",2])),as.numeric(as.vector(segment1[segment1[,1] == "T",3])),as.numeric(as.vector(segment1[segment1[,1] == "T",4])),as.numeric(as.vector(segment1[segment1[,1] == "T",5])),as.numeric(as.vector(segment1[segment1[,1] == "T",6])))
	colnames(covDifC2)=c("ID","chrom","start","end","probNum","seg.mean")
	covDifC3=data.frame(as.character(as.vector(segment1[segment1[,1] == "N",1])),as.vector(as.vector(segment1[segment1[,1] == "N",2])),as.numeric(as.vector(segment1[segment1[,1] == "N",3])),as.numeric(as.vector(segment1[segment1[,1] == "N",4])),as.numeric(as.vector(segment1[segment1[,1] == "N",5])),as.numeric(as.vector(segment1[segment1[,1] == "N",6])))
	colnames(covDifC3)=c("ID","chrom","start","end","probNum","seg.mean")
	covDifC1T2=covDifC1[(covDifC1[,5] >= binMin),]
	preCallL=covDifC1T2[ (covDifC1T2[,5] >= (3*binMin)) & (covDifC1T2[,6] < (th1L[1]) |  covDifC1T2[,6] > (th1L[2]) ),]
	covDifC1T2k=NULL
	covDifC1T2s=NULL
	for ( i in chrNtn ) {
		tmp=covDifC1T2[covDifC1T2[,2] == i,]
		if (dim(tmp)[1] >= 4) {
			covDifC1T2s=rbind(covDifC1T2s,tmp)
		} else {
			covDifC1T2k=rbind(covDifC1T2k,tmp)
		}
	}
	### calls refinement and extension
	C1.CNA.object=CNA(covDifC1T2s[,6],covDifC1T2s[,2],covDifC1T2s[,3], data.type="logratio",sampleid="NT")
	C1.segment1=segment(C1.CNA.object, verbose=1,alpha=0.2,nperm=100)
	colnames(C1.segment1$output)=c("ID","chrom","start","end","probNum","seg.mean")
	C1.segment1.all=rbind(C1.segment1$output,covDifC1T2k)
	C1.segment1.all=C1.segment1.all[order(C1.segment1.all[,2],C1.segment1.all[,3]),]
	mcor1=mad(C1.segment1.all[,6])/devM1
	covDifC2T2=covDifC2[(covDifC2[,5] >= binMin),]
	covDifC2T2k=NULL
	covDifC2T2s=NULL
	for ( i in chrNtn ) {
		tmp=covDifC2T2[covDifC2T2[,2] == i,]
		if (dim(tmp)[1] >= 4) {
			covDifC2T2s=rbind(covDifC2T2s,tmp)
		} else {
			covDifC2T2k=rbind(covDifC2T2k,tmp)
		}
	}
	C2.CNA.object=CNA(covDifC2T2s[,6],covDifC2T2s[,2],covDifC2T2s[,3], data.type="logratio",sampleid="T")
	C2.segment1=segment(C2.CNA.object, verbose=1,alpha=0.2,nperm=100)
	colnames(C2.segment1$output)=c("ID","chrom","start","end","probNum","seg.mean")
	C2.segment1.all=rbind(C2.segment1$output,covDifC2T2k)
	C2.segment1.all=C2.segment1.all[order(C2.segment1.all[,2],C2.segment1.all[,3]),]
	mcor2=mad(C2.segment1.all[,6])/devM2
	covDifC3T2=covDifC3[(covDifC3[,5] >= binMin),]
	covDifC3T2k=NULL
	covDifC3T2s=NULL
	for ( i in chrNtn ) {
		tmp=covDifC3T2[covDifC3T2[,2] == i,]
		if (dim(tmp)[1] >= 4) {
			covDifC3T2s=rbind(covDifC3T2s,tmp)
		} else {
			covDifC3T2k=rbind(covDifC3T2k,tmp)
		}
	}
	C3.CNA.object=CNA(covDifC3T2s[,6],covDifC3T2s[,2],covDifC3T2s[,3], data.type="logratio",sampleid="N")
	C3.segment1=segment(C3.CNA.object, verbose=1,alpha=0.2,nperm=100)
	colnames(C3.segment1$output)=c("ID","chrom","start","end","probNum","seg.mean")
	C3.segment1.all=rbind(C3.segment1$output,covDifC3T2k)
	C3.segment1.all=C3.segment1.all[order(C3.segment1.all[,2],C3.segment1.all[,3]),]
	mcor3=mad(C3.segment1.all[,6])/devM3
	NTcallsL=C1.segment1.all[ (C1.segment1.all[,6] < (th1L[1]) |  C1.segment1.all[,6] > (th1L[2]) ),]
	TcallsL=C2.segment1.all[ (C2.segment1.all[,6] < (th2L[1]) |  C2.segment1.all[,6] > (th2L[2])),]
	NcallsL=C3.segment1.all[ (C3.segment1.all[,6] < (th3L[1]) |  C3.segment1.all[,6] > (th3L[2])),]
	GcallsL=NULL
	###Germline call detection
	for (i in 1:dim(TcallsL)[1]) {
		if (length(NcallsL[NcallsL[,2] == TcallsL[i,2] & ((NcallsL[,3] <= TcallsL[i,3] & NcallsL[,4] >= TcallsL[i,3]) | (NcallsL[,3] >= TcallsL[i,3] & NcallsL[,3] <= TcallsL[i,4])),1]) >= 1) {
			GcallsL=rbind(GcallsL,TcallsL[i,])
		}
	}
	### large raw call rescue
	for (i in 1:dim(preCallL)[1]) {
		if (length(preCallL[NTcallsL[,2] == preCallL[i,2] & ((NTcallsL[,3] <= preCallL[i,3] & NTcallsL[,4] >= preCallL[i,3]) | (NTcallsL[,3] >= preCallL[i,3] & NTcallsL[,3] <= preCallL[i,4])),1]) < 1) {
			NTcallsL=rbind(NTcallsL,preCallL[i,])
		}
	}
	NTcallsLO=NTcallsL[order(NTcallsL[,2],NTcallsL[,3]),]
	### calls annotation
	if (dim(na.omit(NTcallsLO))[1] != 0) {
		typ=NULL
		onco=NULL
		for ( i in 1:dim(NTcallsLO)[1]) {
			onco=c(onco,"T")
			if (NTcallsLO[i,6] > 0) {
				typ=c(typ,3)
			} else {
				typ=c(typ,1)
			}
			if (NTcallsLO[i,1] == "NT" | NTcallsLO[i,1] == "TN") {
				NTcallsLO[i,4]=covDifC1T2[as.vector(NTcallsLO[i,2]) == as.vector(covDifC1T2[,2]) & ((as.vector(NTcallsLO[i,4]) == as.vector(covDifC1T2[,3])) | (as.vector(NTcallsLO[i,4]) == as.vector(covDifC1T2[,4]))),4]+ binSize
			} else {
				NTcallsLO[i,4]=covDifC2T2[as.vector(NTcallsLO[i,2]) == as.vector(covDifC2T2[,2]) & ((as.vector(NTcallsLO[i,4]) == as.vector(covDifC2T2[,3])) | (as.vector(NTcallsLO[i,4]) == as.vector(covDifC2T2[,4]))),4]+ binSize
			}
		}
		NTLfinal=cbind(NTcallsLO[,c(2,3,4,6)],typ,onco)
	} else {
		NTLfinal=NULL
	}
	if (dim(na.omit(GcallsL))[1] != 0) {
		typ=NULL
		onco=NULL
		for ( i in 1:dim(GcallsL)[1]) {
			onco=c(onco,"NT")
			if (GcallsL[i,6] > 0) {
				typ=c(typ,3)
			} else {
				typ=c(typ,1)
			}
			GcallsL[i,4]=covDifC2T2[as.vector(GcallsL[i,2]) == as.vector(covDifC2T2[,2]) & ((as.vector(GcallsL[i,4]) == as.vector(covDifC2T2[,3])) | (as.vector(GcallsL[i,4]) == as.vector(covDifC2T2[,4]))),4]+ binSize
		}
		GLfinal=cbind(GcallsL[,c(2,3,4,6)],typ,onco)
	} else {
		GLfinal=NULL
	}
	NTGLfinal=rbind(NTLfinal,GLfinal)
	if (!(is.null(NTGLfinal))) {
		Lcalls=NTGLfinal[order(NTGLfinal[,1],NTGLfinal[,2]),]
	} else {
		Lcalls=NULL
	}
	##output genomic ratio
	jpeg(paste(out,"GenomicRatios.jpeg",sep="_"),1200,1200)
	layout(matrix(1:3))
	plot(logR[,1],main="N vs T ratio",axes=F,xlab="",ylab="logR")
	axis(1,at=c(0,chrNcs),labels=F)
	axis(1,at=chrNmid,labels=names(chrNt),tick=F)
	axis(2)
	abline(h=th1L,col="pink")
	#abline(h=mean(logR[,1]),col="red")
	abline(h=median(logR[,1]),col="green")
	plot(logR[,2],main="T vs mean(T) ratio",axes=F,xlab="",ylab="logR")
	axis(1,at=c(0,chrNcs),labels=F)
	axis(1,at=chrNmid,labels=names(chrNt),tick=F)
	axis(2)
	abline(h=mean(logR[,2]),col="red")
	abline(h=median(logR[,2]),col="green")
	plot(logR[,3],main="N vs mean(N) ratio",axes=F,xlab="",ylab="logR")
	axis(1,at=c(0,chrNcs),labels=F)
	axis(1,at=chrNmid,labels=names(chrNt),tick=F)
	axis(2)
	abline(h=mean(logR[,3]),col="red")
	abline(h=median(logR[,3]),col="green")
	dev.off()
}
###output calls
write.table(Lcalls,file=paste(out,"CNVcalls.txt",sep="_"),quote=F,sep="\t",col.names=T,row.names=F)
