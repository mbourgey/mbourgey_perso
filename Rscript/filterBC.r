ARG=commandArgs(trailingOnly = T)
inputDir=ARG[1]
inuputBC=ARG[2]
outputName=ARG[3]
genFeat=ARG[4]

tumB=read.table(inuputBC)


if (genFeat == "genes") {
    mat=read.table(paste(inputDir,"matrix.mtx",sep="/"),skip=3)
    bc=read.table(paste(inputDir,"barcodes.tsv",sep="/"))
    ge=read.table(paste(inputDir,"genes.tsv",sep="/"))
} else {
    mat=read.table(gzfile(paste(inputDir,"matrix.mtx.gz",sep="/")),skip=3)
    bc=read.table(gzfile(paste(inputDir,"barcodes.tsv.gz",sep="/")))
    ge=read.table(gzfile(paste(inputDir,"features.tsv.gz",sep="/")))
}

bckeep =match(tumB$V1,bc$V1)
convertbc=1:length(bckeep)


###skip the first line to be recreated in the filtered output
#%%MatrixMarket matrix coordinate integer general
#%
#33694 896 2360359

matkeep=mat$V2 %in% bckeep

bc_flit=cbind(as.vector(bc[bckeep,1]))
mat_filt=mat[matkeep,]


for (i in convertbc) {
mat_filt$V2=replace(mat_filt$V2,mat_filt$V2==bckeep[i],i)
}




dir.create("filtered", showWarnings = FALSE)
dir.create(paste("filtered/",outputName,".filtered_gene_matrices",sep=""), showWarnings = FALSE)
write("%%MatrixMarket matrix coordinate integer general\n%",paste(paste("filtered/",outputName,".filtered_gene_matrices",sep=""),"matrix.mtx",sep="/"))
write(paste(as.character(c(dim(ge)[1],length(bckeep),dim(mat_filt)[1])),sep=" "),paste(paste("filtered/",outputName,".filtered_gene_matrices",sep=""),"matrix.mtx",sep="/"), append=TRUE,ncol=3)
write.table(mat_filt,paste(paste("filtered/",outputName,".filtered_gene_matrices",sep=""),"matrix.mtx",sep="/"), append=TRUE, col.names=F,row.names=F,quote=F,sep=" ")
write.table(bc_flit,paste(paste("filtered/",outputName,".filtered_gene_matrices",sep=""),"barcodes.tsv",sep="/"),  col.names=F,row.names=F,quote=F,sep="\t")
write.table(ge,paste(paste("filtered/",outputName,".filtered_gene_matrices",sep=""),"genes.tsv",sep="/"),  col.names=F,row.names=F,quote=F,sep="\t")


